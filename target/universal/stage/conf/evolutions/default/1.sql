# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table achievment (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  thumbnail                     varchar(255),
  constraint pk_achievment primary key (id)
);

create table achievment_details (
  id                            bigint auto_increment not null,
  achievment_id                 bigint,
  serialno                      varchar(255),
  name                          varchar(255),
  fathername                    varchar(255),
  aadharno                      varchar(255),
  mobile                        varchar(255),
  district                      varchar(255),
  state                         varchar(255),
  achievment_location_id        bigint,
  constraint pk_achievment_details primary key (id)
);

create table achievment_location (
  id                            bigint auto_increment not null,
  locationname                  varchar(255),
  thumbnail                     varchar(255),
  achievment_id                 bigint,
  constraint pk_achievment_location primary key (id)
);

create table adds (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  thumbnail                     varchar(255),
  status                        tinyint(1) default 0,
  constraint pk_adds primary key (id)
);

create table admit_card (
  id                            bigint auto_increment not null,
  city                          varchar(255),
  name                          varchar(255),
  mobile                        varchar(255),
  constraint pk_admit_card primary key (id)
);

create table announcement (
  id                            bigint auto_increment not null,
  heading                       varchar(255),
  date                          bigint,
  description                   varchar(255),
  constraint pk_announcement primary key (id)
);

create table chat (
  id                            bigint auto_increment not null,
  msg_type                      varchar(255),
  message                       varchar(255),
  media_thumbnail               varchar(255),
  media                         varchar(255),
  sender_name                   varchar(255),
  sender_id                     varchar(255),
  time                          varchar(255),
  constraint pk_chat primary key (id)
);

create table company (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_company primary key (id)
);

create table facebook (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  thumbnail                     varchar(255),
  constraint pk_facebook primary key (id)
);

create table facebook_details (
  id                            bigint auto_increment not null,
  url                           varchar(255),
  facebook_id                   bigint,
  constraint pk_facebook_details primary key (id)
);

create table form (
  id                            bigint auto_increment not null,
  thumbnail                     varchar(255),
  form_title_id                 bigint,
  constraint pk_form primary key (id)
);

create table form_title (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_form_title primary key (id)
);

create table gallery (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  date                          bigint,
  constraint pk_gallery primary key (id)
);

create table gallery_details (
  id                            bigint auto_increment not null,
  thumbnail                     varchar(255),
  gallery_id                    bigint,
  constraint pk_gallery_details primary key (id)
);

create table gold_sliver (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  price                         varchar(255),
  file                          varchar(255),
  constraint pk_gold_sliver primary key (id)
);

create table gotra (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_gotra primary key (id)
);

create table home (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  mobile                        varchar(255),
  email                         varchar(255),
  description                   varchar(255),
  thumbnail                     varchar(255),
  constraint pk_home primary key (id)
);

create table mail (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  thumbnail                     varchar(255),
  constraint pk_mail primary key (id)
);

create table mail_details (
  id                            bigint auto_increment not null,
  url                           varchar(255),
  mail_id                       bigint,
  constraint pk_mail_details primary key (id)
);

create table media (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  type                          varchar(255),
  constraint pk_media primary key (id)
);

create table organization (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  thumbnail                     varchar(255),
  constraint pk_organization primary key (id)
);

create table organization_details (
  id                            bigint auto_increment not null,
  membername                    varchar(255),
  designation                   varchar(255),
  phone                         varchar(255),
  email                         varchar(255),
  organization_id               bigint,
  constraint pk_organization_details primary key (id)
);

create table registration (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  email                         varchar(255),
  password                      varchar(255),
  constraint pk_registration primary key (id)
);

create table suggestion (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  email                         varchar(255),
  mobile                        varchar(255),
  description                   varchar(255),
  constraint pk_suggestion primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  yob                           varchar(255),
  gotra                         varchar(255),
  address                       varchar(255),
  city                          varchar(255),
  state                         varchar(255),
  country                       varchar(255),
  mobile                        varchar(255),
  profile_pic                   varchar(255),
  email                         varchar(255),
  password                      varchar(255),
  profession                    varchar(255),
  fathername                    varchar(255),
  mothername                    varchar(255),
  mothergotra                   varchar(255),
  wifename                      varchar(255),
  wifegotra                     varchar(255),
  sonname                       varchar(255),
  sonmaritalstatus              varchar(255),
  sonnameone                    varchar(255),
  sonmaritalstatusone           varchar(255),
  sonnametwo                    varchar(255),
  sonmaritalstatustwo           varchar(255),
  sonnamethree                  varchar(255),
  sonmaritalstatusthree         varchar(255),
  sonnamefour                   varchar(255),
  sonmaritalstatusfour          varchar(255),
  sonnamefive                   varchar(255),
  sonmaritalstatusfive          varchar(255),
  daughtername                  varchar(255),
  daughtermaritalstatus         varchar(255),
  daughternameone               varchar(255),
  daughtermaritalstatusone      varchar(255),
  daughternametwo               varchar(255),
  daughtermaritalstatustwo      varchar(255),
  daughternamethree             varchar(255),
  daughtermaritalstatusthree    varchar(255),
  daughternamefour              varchar(255),
  daughtermaritalstatusfour     varchar(255),
  daughternamefive              varchar(255),
  daughtermaritalstatusfive     varchar(255),
  brothername                   varchar(255),
  brothermaritalstatus          varchar(255),
  brothernameone                varchar(255),
  brothermaritalstatusone       varchar(255),
  brothernametwo                varchar(255),
  brothermaritalstatustwo       varchar(255),
  brothernamethree              varchar(255),
  brothermaritalstatusthree     varchar(255),
  brothernamefour               varchar(255),
  brothermaritalstatusfour      varchar(255),
  brothernamefive               varchar(255),
  brothermaritalstatusfive      varchar(255),
  sistername                    varchar(255),
  sistermaritalstatus           varchar(255),
  sisternameone                 varchar(255),
  sistermaritalstatusone        varchar(255),
  sisternametwo                 varchar(255),
  sistermaritalstatustwo        varchar(255),
  sisternamethree               varchar(255),
  sistermaritalstatusthree      varchar(255),
  sisternamefour                varchar(255),
  sistermaritalstatusfour       varchar(255),
  usertype                      integer not null,
  access_token                  varchar(255),
  constraint uq_user_email unique (email),
  constraint pk_user primary key (id)
);

alter table achievment_details add constraint fk_achievment_details_achievment_id foreign key (achievment_id) references achievment (id) on delete restrict on update restrict;
create index ix_achievment_details_achievment_id on achievment_details (achievment_id);

alter table achievment_details add constraint fk_achievment_details_achievment_location_id foreign key (achievment_location_id) references achievment_location (id) on delete restrict on update restrict;
create index ix_achievment_details_achievment_location_id on achievment_details (achievment_location_id);

alter table achievment_location add constraint fk_achievment_location_achievment_id foreign key (achievment_id) references achievment (id) on delete restrict on update restrict;
create index ix_achievment_location_achievment_id on achievment_location (achievment_id);

alter table facebook_details add constraint fk_facebook_details_facebook_id foreign key (facebook_id) references facebook (id) on delete restrict on update restrict;
create index ix_facebook_details_facebook_id on facebook_details (facebook_id);

alter table form add constraint fk_form_form_title_id foreign key (form_title_id) references form_title (id) on delete restrict on update restrict;
create index ix_form_form_title_id on form (form_title_id);

alter table gallery_details add constraint fk_gallery_details_gallery_id foreign key (gallery_id) references gallery (id) on delete restrict on update restrict;
create index ix_gallery_details_gallery_id on gallery_details (gallery_id);

alter table mail_details add constraint fk_mail_details_mail_id foreign key (mail_id) references mail (id) on delete restrict on update restrict;
create index ix_mail_details_mail_id on mail_details (mail_id);

alter table organization_details add constraint fk_organization_details_organization_id foreign key (organization_id) references organization (id) on delete restrict on update restrict;
create index ix_organization_details_organization_id on organization_details (organization_id);


# --- !Downs

alter table achievment_details drop foreign key fk_achievment_details_achievment_id;
drop index ix_achievment_details_achievment_id on achievment_details;

alter table achievment_details drop foreign key fk_achievment_details_achievment_location_id;
drop index ix_achievment_details_achievment_location_id on achievment_details;

alter table achievment_location drop foreign key fk_achievment_location_achievment_id;
drop index ix_achievment_location_achievment_id on achievment_location;

alter table facebook_details drop foreign key fk_facebook_details_facebook_id;
drop index ix_facebook_details_facebook_id on facebook_details;

alter table form drop foreign key fk_form_form_title_id;
drop index ix_form_form_title_id on form;

alter table gallery_details drop foreign key fk_gallery_details_gallery_id;
drop index ix_gallery_details_gallery_id on gallery_details;

alter table mail_details drop foreign key fk_mail_details_mail_id;
drop index ix_mail_details_mail_id on mail_details;

alter table organization_details drop foreign key fk_organization_details_organization_id;
drop index ix_organization_details_organization_id on organization_details;

drop table if exists achievment;

drop table if exists achievment_details;

drop table if exists achievment_location;

drop table if exists adds;

drop table if exists admit_card;

drop table if exists announcement;

drop table if exists chat;

drop table if exists company;

drop table if exists facebook;

drop table if exists facebook_details;

drop table if exists form;

drop table if exists form_title;

drop table if exists gallery;

drop table if exists gallery_details;

drop table if exists gold_sliver;

drop table if exists gotra;

drop table if exists home;

drop table if exists mail;

drop table if exists mail_details;

drop table if exists media;

drop table if exists organization;

drop table if exists organization_details;

drop table if exists registration;

drop table if exists suggestion;

drop table if exists user;

