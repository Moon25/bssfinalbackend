
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user: User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.14*/("""
"""),format.raw/*2.1*/("""<!doctype html>
<html>
<head>
    <title>BSS</title>
    <link rel="stylesheet" href=""""),_display_(/*6.35*/routes/*6.41*/.Assets.versioned("css/bootstrap.css")),format.raw/*6.79*/("""">
    <link rel="stylesheet" href=""""),_display_(/*7.35*/routes/*7.41*/.Assets.versioned("css/app.css")),format.raw/*7.73*/("""">
    <link rel="stylesheet" href=""""),_display_(/*8.35*/routes/*8.41*/.Assets.versioned("css/textangular.css")),format.raw/*8.81*/("""">
    <link rel="stylesheet" href=""""),_display_(/*9.35*/routes/*9.41*/.Assets.versioned("css/colorpicker.css")),format.raw/*9.81*/("""">
    <link rel="stylesheet" href=""""),_display_(/*10.35*/routes/*10.41*/.Assets.versioned("font-awesome-4.7.0/css/font-awesome.min.css")),format.raw/*10.105*/("""">
    <link rel="stylesheet" href=""""),_display_(/*11.35*/routes/*11.41*/.Assets.versioned("fonts/font.css")),format.raw/*11.76*/("""">
</head>
<body ng-app="myApp"  ng-controller="indexCtrl">
<header >
    <nav class="navbar navbar-inverse navbar-fixed-top app-navbar-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbarCollapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-left SourceSansProFontregularClass font15">
                   <a style="text-decoration: none;" href="#!/home"><li class="SourceSansProFontregularClass font22" style="color:#ffffff; margin-top: 8px;">BSS</li></a>
                </ul>
                <ul class="nav navbar-nav navbar-right SourceSansProFontregularClass font15">
                    <li class="dropdown">
                        <a style="color: #ffffff;" href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> """),_display_(/*31.156*/user/*31.160*/.getName),format.raw/*31.168*/("""</a>
                        <ul class="dropdown-menu SourceSansProFontregularClass font15" style="min-width:80px; background-color:#015592;">
                            <li><a href="/logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

 <div class="col-md-12 ">
            <ul class="" style="margin-top:65px;">


                <!--<a href="#!/registration" class="SourceSansProFontregularClass font19 app_top_menu text-uppercase"><span class="fa fa-registered" aria-hidden="true"></span>&nbsp;Registration</a>-->
                <a href="#!/home" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-home" aria-hidden="true"></span>&nbsp;Home Screen</a>
                <a href="#!/user" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-user" aria-hidden="true"></span>&nbsp;User</a>
                <!--<a href="#!/gotra" class="SourceSansProFontregularClass font17 app_top_menu text-uppercase"><span class="fa fa-google" aria-hidden="true"></span>&nbsp;Gotra</a>-->
                <a href="#!/announcement" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-bullhorn" aria-hidden="true"></span>&nbsp;Announcement</a>
                <a href="#!/organization" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-sitemap" aria-hidden="true"></span>&nbsp;Organization</a>
                <a href="#!/organizationDetails" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-sitemap" aria-hidden="true"></span>&nbsp;Organization Details </a>
                <a href="#!/formTitle" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-fab fa-wpforms" aria-hidden="true"></span>&nbsp;Form</a>
                <a href="#!/form" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-fab fa-wpforms" aria-hidden="true"></span>&nbsp;Form Details</a>
                <!--<a href="#!/goldSilver" class="SourceSansProFontregularClass font19 app_top_menu text-uppercase"><span class="fa fa-square" aria-hidden="true"></span>&nbsp;Gold Silver</a>-->
                <a href="#!/suggestion" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-thumbs-up" aria-hidden="true"></span>&nbsp;Suggestion</a>
                <a href="#!/achievment" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-trophy" aria-hidden="true"></span>&nbsp;Achievment</a><br><br>
                <a href="#!/achievmentLocation" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-trophy" aria-hidden="true"></span>&nbsp;Achievment Location</a>
                <a href="#!/achievmentDetails" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-trophy"></span>&nbsp;Achievment Details</a>
                <!--<a href="#!/admitCard" class="SourceSansProFontregularClass font19 app_top_menu text-uppercase"><span class="fa fa-id-card" aria-hidden="true"></span>&nbsp;Admit Card</a>-->
                <a href="#!/gallery" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-file-image-o" aria-hidden="true"></span>&nbsp;Gallery</a>
                <a href="#!/galleryDetails" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-file-image-o" aria-hidden="true"></span>&nbsp;Gallery Details</a>

                <a href="#!/facebook" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-facebook" aria-hidden="true"></span>&nbsp;Facebook</a>
                <a href="#!/facebookDetails" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-facebook" aria-hidden="true"></span>&nbsp;Facebook Details</a>
                <a href="#!/mail" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;Mail</a>
                <a href="#!/mailDetails" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;Mail Details</a>
                <a href="#!/adds" class="SourceSansProFontregularClass font15 app_top_menu text-uppercase"><span class="fa fa-caret-square-o-right" aria-hidden="true"></span>&nbsp;ADD</a>

                <!--<a href="#!/chat" class="SourceSansProFontregularClass font17 app_top_menu text-uppercase"><span class="fa fa-file-image-o" aria-hidden="true"></span>&nbsp;Chat</a>-->




            </ul>

</div>
<div class="col-md-12 app-form">
    <div class="col-md-12">
        <div ng-view ></div>
    </div>
</div>
</body>

<style>
         .navbar-fixed-left .navbar-nav > li """),format.raw/*86.46*/("""{"""),format.raw/*86.47*/("""
         """),format.raw/*87.10*/("""float: none;  /* Cancel default li float: left */
         width: 139px;
         """),format.raw/*89.10*/("""}"""),format.raw/*89.11*/("""
         """),format.raw/*90.10*/(""".navbar-fixed-left + .container """),format.raw/*90.42*/("""{"""),format.raw/*90.43*/("""
         """),format.raw/*91.10*/("""padding-left: 160px;
         """),format.raw/*92.10*/("""}"""),format.raw/*92.11*/("""
         """),format.raw/*93.10*/("""/* On using dropdown menu (To right shift popuped) */
         .navbar-fixed-left .navbar-nav > li > .dropdown-menu """),format.raw/*94.63*/("""{"""),format.raw/*94.64*/("""
         """),format.raw/*95.10*/("""margin-top: -50px;
         margin-left: 140px;
         """),format.raw/*97.10*/("""}"""),format.raw/*97.11*/("""
         """),format.raw/*98.10*/(""".css-form input.ng-dirty.ng-invalid """),format.raw/*98.46*/("""{"""),format.raw/*98.47*/("""
         """),format.raw/*99.10*/("""border-color: #e9322d;
         """),format.raw/*100.10*/("""}"""),format.raw/*100.11*/("""
         """),format.raw/*101.10*/(""".field-message """),format.raw/*101.25*/("""{"""),format.raw/*101.26*/("""
         """),format.raw/*102.10*/("""display: inline-block;
         color: #e9322d;
         font-sizes: 88%;
         margin-left: auto;
         """),format.raw/*106.10*/("""}"""),format.raw/*106.11*/("""
         """),format.raw/*107.10*/("""[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak """),format.raw/*107.89*/("""{"""),format.raw/*107.90*/("""
         """),format.raw/*108.10*/("""display: none !important;
         """),format.raw/*109.10*/("""}"""),format.raw/*109.11*/("""

         """),format.raw/*111.10*/("""body #left """),format.raw/*111.21*/("""{"""),format.raw/*111.22*/("""
                """),format.raw/*112.17*/("""height: 100%;
                position: fixed;
         """),format.raw/*114.10*/("""}"""),format.raw/*114.11*/("""
         """),format.raw/*115.10*/("""body #right """),format.raw/*115.22*/("""{"""),format.raw/*115.23*/("""
                """),format.raw/*116.17*/("""height: 100%;
                position: absolute;
                right: 0;
         """),format.raw/*119.10*/("""}"""),format.raw/*119.11*/("""

"""),format.raw/*121.1*/("""</style>
    <script src=""""),_display_(/*122.19*/routes/*122.25*/.Assets.versioned("js/javascripts/jquery.min.js")),format.raw/*122.74*/(""""></script>
    <script src=""""),_display_(/*123.19*/routes/*123.25*/.Assets.versioned("js/javascripts/bootstrap.js")),format.raw/*123.73*/(""""></script>
    <script src=""""),_display_(/*124.19*/routes/*124.25*/.Assets.versioned("js/javascripts/angular.js")),format.raw/*124.71*/(""""></script>
    <script src=""""),_display_(/*125.19*/routes/*125.25*/.Assets.versioned("js/javascripts/angular-route.js")),format.raw/*125.77*/(""""></script>
    <script src=""""),_display_(/*126.19*/routes/*126.25*/.Assets.versioned("js/javascripts/angular-resource.js")),format.raw/*126.80*/(""""></script>
    <script src=""""),_display_(/*127.19*/routes/*127.25*/.Assets.versioned("js/javascripts/angular-animate.js")),format.raw/*127.79*/(""""></script>
    <script src=""""),_display_(/*128.19*/routes/*128.25*/.Assets.versioned("js/javascripts/angular-messages.js")),format.raw/*128.80*/(""""></script>
    <script src=""""),_display_(/*129.19*/routes/*129.25*/.Assets.versioned("js/javascripts/angularjs-dropdown-multiselect.js")),format.raw/*129.94*/(""""></script>
    <script src=""""),_display_(/*130.19*/routes/*130.25*/.Assets.versioned("js/javascripts/ui-bootstrap.js")),format.raw/*130.76*/(""""></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.12.0/ui-bootstrap-tpls.min.js">
    <script src=""""),_display_(/*132.19*/routes/*132.25*/.Assets.versioned("js/javascripts/ui-bootstrap-tpls.js")),format.raw/*132.81*/(""""></script>
    <script src=""""),_display_(/*133.19*/routes/*133.25*/.Assets.versioned("js/javascripts/textAngular-rangy.min.js")),format.raw/*133.85*/(""""></script>
    <script src=""""),_display_(/*134.19*/routes/*134.25*/.Assets.versioned("js/javascripts/textAngular-sanitize.min.js")),format.raw/*134.88*/(""""></script>
    <script src=""""),_display_(/*135.19*/routes/*135.25*/.Assets.versioned("js/javascripts/textAngular.min.js")),format.raw/*135.79*/(""""></script>
    <script src=""""),_display_(/*136.19*/routes/*136.25*/.Assets.versioned("js/javascripts/angular-file-upload.min.js")),format.raw/*136.87*/(""""></script>
    <script src=""""),_display_(/*137.19*/routes/*137.25*/.Assets.versioned("js/javascripts/colorpicker.js")),format.raw/*137.75*/(""""></script>
    <script src=""""),_display_(/*138.19*/routes/*138.25*/.Assets.versioned("js/angularControllers/main.js")),format.raw/*138.75*/(""""></script>
    <script src=""""),_display_(/*139.19*/routes/*139.25*/.Assets.versioned("js/angularControllers/services.js")),format.raw/*139.79*/(""""></script>
   <!-- <script src=""""),_display_(/*140.23*/routes/*140.29*/.Assets.versioned("js/angularControllers/registration.js")),format.raw/*140.87*/(""""></script>-->
    <script src=""""),_display_(/*141.19*/routes/*141.25*/.Assets.versioned("js/angularControllers/announcement.js")),format.raw/*141.83*/(""""></script>
    <script src=""""),_display_(/*142.19*/routes/*142.25*/.Assets.versioned("js/angularControllers/organization.js")),format.raw/*142.83*/(""""></script>
    <script src=""""),_display_(/*143.19*/routes/*143.25*/.Assets.versioned("js/angularControllers/organizationDetails.js")),format.raw/*143.90*/(""""></script>
    <script src=""""),_display_(/*144.19*/routes/*144.25*/.Assets.versioned("js/angularControllers/form.js")),format.raw/*144.75*/(""""></script>
    <script src=""""),_display_(/*145.19*/routes/*145.25*/.Assets.versioned("js/angularControllers/goldSilver.js")),format.raw/*145.81*/(""""></script>
    <script src=""""),_display_(/*146.19*/routes/*146.25*/.Assets.versioned("js/angularControllers/suggestion.js")),format.raw/*146.81*/(""""></script>
    <script src=""""),_display_(/*147.19*/routes/*147.25*/.Assets.versioned("js/angularControllers/admitCard.js")),format.raw/*147.80*/(""""></script>
    <script src=""""),_display_(/*148.19*/routes/*148.25*/.Assets.versioned("js/angularControllers/chat.js")),format.raw/*148.75*/(""""></script>
    <script src=""""),_display_(/*149.19*/routes/*149.25*/.Assets.versioned("js/angularControllers/achievment.js")),format.raw/*149.81*/(""""></script>
    <script src=""""),_display_(/*150.19*/routes/*150.25*/.Assets.versioned("js/angularControllers/achievmentLocation.js")),format.raw/*150.89*/(""""></script>
    <script src=""""),_display_(/*151.19*/routes/*151.25*/.Assets.versioned("js/angularControllers/achievmentDetails.js")),format.raw/*151.88*/(""""></script>
    <script src=""""),_display_(/*152.19*/routes/*152.25*/.Assets.versioned("js/angularControllers/facebook.js")),format.raw/*152.79*/(""""></script>
    <script src=""""),_display_(/*153.19*/routes/*153.25*/.Assets.versioned("js/angularControllers/facebookDetails.js")),format.raw/*153.86*/(""""></script>
    <script src=""""),_display_(/*154.19*/routes/*154.25*/.Assets.versioned("js/angularControllers/mail.js")),format.raw/*154.75*/(""""></script>
    <script src=""""),_display_(/*155.19*/routes/*155.25*/.Assets.versioned("js/angularControllers/mailDetails.js")),format.raw/*155.82*/(""""></script>
    <script src=""""),_display_(/*156.19*/routes/*156.25*/.Assets.versioned("js/angularControllers/gallery.js")),format.raw/*156.78*/(""""></script>
    <script src=""""),_display_(/*157.19*/routes/*157.25*/.Assets.versioned("js/angularControllers/galleryDetails.js")),format.raw/*157.85*/(""""></script>
    <script src=""""),_display_(/*158.19*/routes/*158.25*/.Assets.versioned("js/angularControllers/gotra.js")),format.raw/*158.76*/(""""></script>
    <script src=""""),_display_(/*159.19*/routes/*159.25*/.Assets.versioned("js/angularControllers/user.js")),format.raw/*159.75*/(""""></script>
    <script src=""""),_display_(/*160.19*/routes/*160.25*/.Assets.versioned("js/angularControllers/home.js")),format.raw/*160.75*/(""""></script>
    <script src=""""),_display_(/*161.19*/routes/*161.25*/.Assets.versioned("js/angularControllers/formTitle.js")),format.raw/*161.80*/(""""></script>
    <script src=""""),_display_(/*162.19*/routes/*162.25*/.Assets.versioned("js/angularControllers/adds.js")),format.raw/*162.75*/(""""></script>

</html>"""))
      }
    }
  }

  def render(user:User): play.twirl.api.HtmlFormat.Appendable = apply(user)

  def f:((User) => play.twirl.api.HtmlFormat.Appendable) = (user) => apply(user)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Jul 23 17:31:01 IST 2018
                  SOURCE: D:/BssBackendUpdatedCode/BSS Project Backend/bss/app/views/index.scala.html
                  HASH: 5d5179f56b145b5808fd94cc950cc3f1ff15a552
                  MATRIX: 946->1|1053->13|1081->15|1198->106|1212->112|1270->150|1334->188|1348->194|1400->226|1464->264|1478->270|1538->310|1602->348|1616->354|1676->394|1741->432|1756->438|1842->502|1907->540|1922->546|1978->581|3272->1847|3286->1851|3316->1859|8414->6929|8443->6930|8482->6941|8594->7025|8623->7026|8662->7037|8722->7069|8751->7070|8790->7081|8849->7112|8878->7113|8917->7124|9062->7241|9091->7242|9130->7253|9217->7312|9246->7313|9285->7324|9349->7360|9378->7361|9417->7372|9479->7405|9509->7406|9549->7417|9593->7432|9623->7433|9663->7444|9807->7559|9837->7560|9877->7571|9985->7650|10015->7651|10055->7662|10120->7698|10150->7699|10192->7712|10232->7723|10262->7724|10309->7742|10396->7800|10426->7801|10466->7812|10507->7824|10537->7825|10584->7843|10701->7931|10731->7932|10763->7936|10819->7964|10835->7970|10906->8019|10965->8050|10981->8056|11051->8104|11110->8135|11126->8141|11194->8187|11253->8218|11269->8224|11343->8276|11402->8307|11418->8313|11495->8368|11554->8399|11570->8405|11646->8459|11705->8490|11721->8496|11798->8551|11857->8582|11873->8588|11964->8657|12023->8688|12039->8694|12112->8745|12283->8888|12299->8894|12377->8950|12436->8981|12452->8987|12534->9047|12593->9078|12609->9084|12694->9147|12753->9178|12769->9184|12845->9238|12904->9269|12920->9275|13004->9337|13063->9368|13079->9374|13151->9424|13210->9455|13226->9461|13298->9511|13357->9542|13373->9548|13449->9602|13512->9637|13528->9643|13608->9701|13670->9735|13686->9741|13766->9799|13825->9830|13841->9836|13921->9894|13980->9925|13996->9931|14083->9996|14142->10027|14158->10033|14230->10083|14289->10114|14305->10120|14383->10176|14442->10207|14458->10213|14536->10269|14595->10300|14611->10306|14688->10361|14747->10392|14763->10398|14835->10448|14894->10479|14910->10485|14988->10541|15047->10572|15063->10578|15149->10642|15208->10673|15224->10679|15309->10742|15368->10773|15384->10779|15460->10833|15519->10864|15535->10870|15618->10931|15677->10962|15693->10968|15765->11018|15824->11049|15840->11055|15919->11112|15978->11143|15994->11149|16069->11202|16128->11233|16144->11239|16226->11299|16285->11330|16301->11336|16374->11387|16433->11418|16449->11424|16521->11474|16580->11505|16596->11511|16668->11561|16727->11592|16743->11598|16820->11653|16879->11684|16895->11690|16967->11740
                  LINES: 28->1|33->1|34->2|38->6|38->6|38->6|39->7|39->7|39->7|40->8|40->8|40->8|41->9|41->9|41->9|42->10|42->10|42->10|43->11|43->11|43->11|63->31|63->31|63->31|118->86|118->86|119->87|121->89|121->89|122->90|122->90|122->90|123->91|124->92|124->92|125->93|126->94|126->94|127->95|129->97|129->97|130->98|130->98|130->98|131->99|132->100|132->100|133->101|133->101|133->101|134->102|138->106|138->106|139->107|139->107|139->107|140->108|141->109|141->109|143->111|143->111|143->111|144->112|146->114|146->114|147->115|147->115|147->115|148->116|151->119|151->119|153->121|154->122|154->122|154->122|155->123|155->123|155->123|156->124|156->124|156->124|157->125|157->125|157->125|158->126|158->126|158->126|159->127|159->127|159->127|160->128|160->128|160->128|161->129|161->129|161->129|162->130|162->130|162->130|164->132|164->132|164->132|165->133|165->133|165->133|166->134|166->134|166->134|167->135|167->135|167->135|168->136|168->136|168->136|169->137|169->137|169->137|170->138|170->138|170->138|171->139|171->139|171->139|172->140|172->140|172->140|173->141|173->141|173->141|174->142|174->142|174->142|175->143|175->143|175->143|176->144|176->144|176->144|177->145|177->145|177->145|178->146|178->146|178->146|179->147|179->147|179->147|180->148|180->148|180->148|181->149|181->149|181->149|182->150|182->150|182->150|183->151|183->151|183->151|184->152|184->152|184->152|185->153|185->153|185->153|186->154|186->154|186->154|187->155|187->155|187->155|188->156|188->156|188->156|189->157|189->157|189->157|190->158|190->158|190->158|191->159|191->159|191->159|192->160|192->160|192->160|193->161|193->161|193->161|194->162|194->162|194->162
                  -- GENERATED --
              */
          