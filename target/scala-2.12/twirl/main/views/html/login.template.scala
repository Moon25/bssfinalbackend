
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object login extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[play.data.Form[User],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(loginForm: play.data.Form[User]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.2*/import helper._


Seq[Any](format.raw/*1.35*/("""
"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BSS</title>
    <link rel="stylesheet" href=""""),_display_(/*8.35*/routes/*8.41*/.Assets.versioned("css/bootstrap.css")),format.raw/*8.79*/("""">
    <link rel="stylesheet" href=""""),_display_(/*9.35*/routes/*9.41*/.Assets.versioned("font-awesome-4.7.0/css/font-awesome.min.css")),format.raw/*9.105*/("""">
    <link rel="stylesheet" href=""""),_display_(/*10.35*/routes/*10.41*/.Assets.versioned("css/app.css")),format.raw/*10.73*/("""">
    <link rel="stylesheet" href=""""),_display_(/*11.35*/routes/*11.41*/.Assets.versioned("fonts/font.css")),format.raw/*11.76*/("""">
</head>
<body class="app-body-login" style="background-color:#03A9F4;"> <!--style="background-color:#B0BEC5;"-->
<div class="col-md-12">
    <div class="col-md-4"></div>
    <div class="col-md-4" style="height: 300px; box-shadow:0 5px 10px rgba(0,0,0,0.16); background:#ffffff; margin-top: 160px;">
        <div class="col-md-12" style="margin-top: 15px;">
            <div class="col-md-3"></div>
            <div class="col-md-8">
                <p class="SourceSansProFontregularClass font25" style="color: #03A9F4;">Login to BSS</p>
            </div>
            <div class="col-md-1"></div>
        </div>
        <br/>
        <div class="col-md-12">
            """),_display_(/*26.14*/if(flash.containsKey("failure"))/*26.46*/ {_display_(Seq[Any](format.raw/*26.48*/("""
            """),format.raw/*27.13*/("""<div class="alert-message warning SourceSansProFontregularClass font18" style="color: #03A9F4;">
                <strong>Done!</strong> """),_display_(/*28.41*/flash/*28.46*/.get("failure")),format.raw/*28.61*/("""
            """),format.raw/*29.13*/("""</div>
            """)))}),format.raw/*30.14*/("""
        """),format.raw/*31.9*/("""</div>
        <div class="col-md-12">
            <div class="col-md-2"></div>
            <div class="col-md-8" style="margin-left:30px;">
                """),_display_(/*35.18*/form(controllers.routes.UserControllers.login())/*35.66*/ {_display_(Seq[Any](format.raw/*35.68*/("""
                """),format.raw/*36.17*/("""<fieldset class="SourceSansProFontLightClass font16">
                    """),_display_(/*37.22*/inputText(loginForm("email"), '_label -> "Email-ID",'_help -> "")),format.raw/*37.87*/("""
                    """),_display_(/*38.22*/inputPassword(loginForm("password"), '_label -> "Password", '_help -> "")),format.raw/*38.95*/("""
                """),format.raw/*39.17*/("""</fieldset>
                <div class="actions">
                    <div class="col-md-12" style="margin-bottom: 15px;">
                        <div class="col-md-12">
                            <input type="submit" value="Login" style="width: 250px; margin-left: -78px; background-color: #015592; border-color: #015592;" class="btn btn-primary btn-md btn-block SourceSansProFontregularClass font16">
                        </div>
                    </div>
                </div><br>
                """)))}),format.raw/*47.18*/("""
            """),format.raw/*48.13*/("""</div>
            <div class="col-md-2"></div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>
</body>
</html>"""))
      }
    }
  }

  def render(loginForm:play.data.Form[User]): play.twirl.api.HtmlFormat.Appendable = apply(loginForm)

  def f:((play.data.Form[User]) => play.twirl.api.HtmlFormat.Appendable) = (loginForm) => apply(loginForm)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Jul 23 17:31:01 IST 2018
                  SOURCE: D:/BssBackendUpdatedCode/BSS Project Backend/bss/app/views/login.scala.html
                  HASH: 4608e6b10d22c5762f9a70ccee9eb6a4d9d49f3f
                  MATRIX: 962->1|1068->37|1113->34|1141->54|1296->183|1310->189|1368->227|1432->265|1446->271|1531->335|1596->373|1611->379|1664->411|1729->449|1744->455|1800->490|2517->1180|2558->1212|2598->1214|2640->1228|2805->1366|2819->1371|2855->1386|2897->1400|2949->1421|2986->1431|3175->1593|3232->1641|3272->1643|3318->1661|3421->1737|3507->1802|3557->1825|3651->1898|3697->1916|4243->2431|4285->2445
                  LINES: 28->1|31->2|34->1|35->3|40->8|40->8|40->8|41->9|41->9|41->9|42->10|42->10|42->10|43->11|43->11|43->11|58->26|58->26|58->26|59->27|60->28|60->28|60->28|61->29|62->30|63->31|67->35|67->35|67->35|68->36|69->37|69->37|70->38|70->38|71->39|79->47|80->48
                  -- GENERATED --
              */
          