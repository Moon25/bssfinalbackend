
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/BssBackendUpdatedCode/BSS Project Backend/bss/conf/routes
// @DATE:Tue Jul 24 17:31:18 IST 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  Application_19: controllers.Application,
  // @LINE:16
  Assets_17: controllers.Assets,
  // @LINE:28
  UserControllers_4: controllers.UserControllers,
  // @LINE:58
  MediaController_0: controllers.MediaController,
  // @LINE:75
  OrganizationController_12: controllers.OrganizationController,
  // @LINE:91
  OrganizationDetailsController_21: controllers.OrganizationDetailsController,
  // @LINE:113
  RegistrationController_24: controllers.RegistrationController,
  // @LINE:121
  AnnouncementController_25: controllers.AnnouncementController,
  // @LINE:141
  FormTitleController_13: controllers.FormTitleController,
  // @LINE:156
  FormController_14: controllers.FormController,
  // @LINE:175
  GoldSilverController_7: controllers.GoldSilverController,
  // @LINE:190
  SuggestionController_3: controllers.SuggestionController,
  // @LINE:205
  AdmitCardController_2: controllers.AdmitCardController,
  // @LINE:222
  ChatController_23: controllers.ChatController,
  // @LINE:251
  AchievmentController_5: controllers.AchievmentController,
  // @LINE:267
  AchievmentLocationController_22: controllers.AchievmentLocationController,
  // @LINE:288
  AchievmentDetailsController_18: controllers.AchievmentDetailsController,
  // @LINE:308
  FacebookController_16: controllers.FacebookController,
  // @LINE:323
  FacebookDetailsController_9: controllers.FacebookDetailsController,
  // @LINE:342
  MailController_20: controllers.MailController,
  // @LINE:358
  MailDetailsController_8: controllers.MailDetailsController,
  // @LINE:377
  GalleryController_1: controllers.GalleryController,
  // @LINE:392
  GalleryDetailsController_15: controllers.GalleryDetailsController,
  // @LINE:411
  GotraController_11: controllers.GotraController,
  // @LINE:431
  HomeController_6: controllers.HomeController,
  // @LINE:451
  AddsController_10: controllers.AddsController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    Application_19: controllers.Application,
    // @LINE:16
    Assets_17: controllers.Assets,
    // @LINE:28
    UserControllers_4: controllers.UserControllers,
    // @LINE:58
    MediaController_0: controllers.MediaController,
    // @LINE:75
    OrganizationController_12: controllers.OrganizationController,
    // @LINE:91
    OrganizationDetailsController_21: controllers.OrganizationDetailsController,
    // @LINE:113
    RegistrationController_24: controllers.RegistrationController,
    // @LINE:121
    AnnouncementController_25: controllers.AnnouncementController,
    // @LINE:141
    FormTitleController_13: controllers.FormTitleController,
    // @LINE:156
    FormController_14: controllers.FormController,
    // @LINE:175
    GoldSilverController_7: controllers.GoldSilverController,
    // @LINE:190
    SuggestionController_3: controllers.SuggestionController,
    // @LINE:205
    AdmitCardController_2: controllers.AdmitCardController,
    // @LINE:222
    ChatController_23: controllers.ChatController,
    // @LINE:251
    AchievmentController_5: controllers.AchievmentController,
    // @LINE:267
    AchievmentLocationController_22: controllers.AchievmentLocationController,
    // @LINE:288
    AchievmentDetailsController_18: controllers.AchievmentDetailsController,
    // @LINE:308
    FacebookController_16: controllers.FacebookController,
    // @LINE:323
    FacebookDetailsController_9: controllers.FacebookDetailsController,
    // @LINE:342
    MailController_20: controllers.MailController,
    // @LINE:358
    MailDetailsController_8: controllers.MailDetailsController,
    // @LINE:377
    GalleryController_1: controllers.GalleryController,
    // @LINE:392
    GalleryDetailsController_15: controllers.GalleryDetailsController,
    // @LINE:411
    GotraController_11: controllers.GotraController,
    // @LINE:431
    HomeController_6: controllers.HomeController,
    // @LINE:451
    AddsController_10: controllers.AddsController
  ) = this(errorHandler, Application_19, Assets_17, UserControllers_4, MediaController_0, OrganizationController_12, OrganizationDetailsController_21, RegistrationController_24, AnnouncementController_25, FormTitleController_13, FormController_14, GoldSilverController_7, SuggestionController_3, AdmitCardController_2, ChatController_23, AchievmentController_5, AchievmentLocationController_22, AchievmentDetailsController_18, FacebookController_16, FacebookDetailsController_9, MailController_20, MailDetailsController_8, GalleryController_1, GalleryDetailsController_15, GotraController_11, HomeController_6, AddsController_10, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, Application_19, Assets_17, UserControllers_4, MediaController_0, OrganizationController_12, OrganizationDetailsController_21, RegistrationController_24, AnnouncementController_25, FormTitleController_13, FormController_14, GoldSilverController_7, SuggestionController_3, AdmitCardController_2, ChatController_23, AchievmentController_5, AchievmentLocationController_22, AchievmentDetailsController_18, FacebookController_16, FacebookDetailsController_9, MailController_20, MailDetailsController_8, GalleryController_1, GalleryDetailsController_15, GotraController_11, HomeController_6, AddsController_10, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.Application.login"""),
    ("""OPTIONS""", this.prefix, """controllers.Application.options(path:String ?= "")"""),
    ("""OPTIONS""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """""" + "$" + """path<.+>""", """controllers.Application.options(path:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """home""", """controllers.Application.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logout""", """controllers.Application.logOut"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """media/""" + "$" + """mediaName<[^/]+>""", """controllers.Application.getFile(mediaName:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """media/Thumbnail/""" + "$" + """mediaName<[^/]+>""", """controllers.Application.getFileThumb(mediaName:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/login""", """controllers.UserControllers.login"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/user/validate""", """controllers.UserControllers.loginAPI"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/users""", """controllers.UserControllers.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getuser""", """controllers.UserControllers.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/user/""" + "$" + """userId<[^/]+>""", """controllers.UserControllers.get(userId:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateuser/""" + "$" + """id<[^/]+>""", """controllers.UserControllers.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteuser/""" + "$" + """id<[^/]+>""", """controllers.UserControllers.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """upload""", """controllers.MediaController.upload"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploadmedia""", """controllers.MediaController.uploadmedia"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletemedia/""" + "$" + """id<[^/]+>""", """controllers.MediaController.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/displaymedia""", """controllers.MediaController.getmedia"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/organization""", """controllers.OrganizationController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getorganization""", """controllers.OrganizationController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateorganization/""" + "$" + """id<[^/]+>""", """controllers.OrganizationController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteorganization/""" + "$" + """id<[^/]+>""", """controllers.OrganizationController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/organizationdetails""", """controllers.OrganizationDetailsController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getorganizationdetails""", """controllers.OrganizationDetailsController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/organization-details/""" + "$" + """orgId<[^/]+>""", """controllers.OrganizationDetailsController.getorganizationDetails(orgId:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateorganizationdetails/""" + "$" + """id<[^/]+>""", """controllers.OrganizationDetailsController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteorganizationdetails/""" + "$" + """id<[^/]+>""", """controllers.OrganizationDetailsController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/registration""", """controllers.UserControllers.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getregistration""", """controllers.RegistrationController.getall"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/announcement""", """controllers.AnnouncementController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getannouncement""", """controllers.AnnouncementController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/announcement""", """controllers.AnnouncementController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateannouncement/""" + "$" + """id<[^/]+>""", """controllers.AnnouncementController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteannouncement/""" + "$" + """id<[^/]+>""", """controllers.AnnouncementController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/formtitle""", """controllers.FormTitleController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getformtitle""", """controllers.FormTitleController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateformtitle/""" + "$" + """id<[^/]+>""", """controllers.FormTitleController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteformtitle/""" + "$" + """id<[^/]+>""", """controllers.FormTitleController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/form""", """controllers.FormController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getform""", """controllers.FormController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getform-details/""" + "$" + """formId<[^/]+>""", """controllers.FormController.getform(formId:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateform/""" + "$" + """id<[^/]+>""", """controllers.FormController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteform/""" + "$" + """id<[^/]+>""", """controllers.FormController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/goldSilver""", """controllers.GoldSilverController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getgoldSilver""", """controllers.GoldSilverController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updategoldSilver/""" + "$" + """id<[^/]+>""", """controllers.GoldSilverController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletegoldSilver/""" + "$" + """id<[^/]+>""", """controllers.GoldSilverController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/suggestion""", """controllers.SuggestionController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getsuggestion""", """controllers.SuggestionController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updatesuggestion/""" + "$" + """id<[^/]+>""", """controllers.SuggestionController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletesuggestion/""" + "$" + """id<[^/]+>""", """controllers.SuggestionController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/admitCard""", """controllers.AdmitCardController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getadmitCard""", """controllers.AdmitCardController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateadmitCard/""" + "$" + """id<[^/]+>""", """controllers.AdmitCardController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteadmitCard/""" + "$" + """id<[^/]+>""", """controllers.AdmitCardController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/send-message""", """controllers.ChatController.add"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/send-media""", """controllers.ChatController.chatUpload"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/list/messages""", """controllers.ChatController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/list/message""", """controllers.ChatController.getMessages(page:Int ?= 0)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/list/latest-message""", """controllers.ChatController.getLatestMessages(id:Long ?= 0)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updatechat/""" + "$" + """id<[^/]+>""", """controllers.ChatController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletechat/""" + "$" + """id<[^/]+>""", """controllers.ChatController.delete(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/delete/message/""" + "$" + """id<[^/]+>""", """controllers.ChatController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/achievment""", """controllers.AchievmentController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getachievment""", """controllers.AchievmentController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateachievment/""" + "$" + """id<[^/]+>""", """controllers.AchievmentController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteachievment/""" + "$" + """id<[^/]+>""", """controllers.AchievmentController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/achievmentlocation""", """controllers.AchievmentLocationController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getachievmentlocation""", """controllers.AchievmentLocationController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getachievment-location/""" + "$" + """locid<[^/]+>""", """controllers.AchievmentLocationController.getdata(locid:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateachievmentlocation/""" + "$" + """id<[^/]+>""", """controllers.AchievmentLocationController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteachievmentlocation/""" + "$" + """id<[^/]+>""", """controllers.AchievmentLocationController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/ahievmentdetails""", """controllers.AchievmentDetailsController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getahievmentdetails""", """controllers.AchievmentDetailsController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getachievment-details/""" + "$" + """achievmentdetailsid<[^/]+>""", """controllers.AchievmentDetailsController.getdata(achievmentdetailsid:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateahievmentdetails/""" + "$" + """id<[^/]+>""", """controllers.AchievmentDetailsController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteahievmentdetails/""" + "$" + """id<[^/]+>""", """controllers.AchievmentDetailsController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/facebook""", """controllers.FacebookController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getfacebook""", """controllers.FacebookController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updatefacebook/""" + "$" + """id<[^/]+>""", """controllers.FacebookController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletefacebook/""" + "$" + """id<[^/]+>""", """controllers.FacebookController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/facebookDetails""", """controllers.FacebookDetailsController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getfacebookDetails""", """controllers.FacebookDetailsController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getfacebook-details/""" + "$" + """locid<[^/]+>""", """controllers.FacebookDetailsController.getfacebookdetails(locid:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updatefacebookDetails/""" + "$" + """id<[^/]+>""", """controllers.FacebookDetailsController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletefacebookDetails/""" + "$" + """id<[^/]+>""", """controllers.FacebookDetailsController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/mail""", """controllers.MailController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getmail""", """controllers.MailController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updatemail/""" + "$" + """id<[^/]+>""", """controllers.MailController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletemail/""" + "$" + """id<[^/]+>""", """controllers.MailController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/maildetails""", """controllers.MailDetailsController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getmaildetails""", """controllers.MailDetailsController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getmail-details/""" + "$" + """mailid<[^/]+>""", """controllers.MailDetailsController.getmaildetails(mailid:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updatemaildetails/""" + "$" + """id<[^/]+>""", """controllers.MailDetailsController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletemaildetails/""" + "$" + """id<[^/]+>""", """controllers.MailDetailsController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/gallery""", """controllers.GalleryController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getgallery""", """controllers.GalleryController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updategallery/""" + "$" + """id<[^/]+>""", """controllers.GalleryController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletegallery/""" + "$" + """id<[^/]+>""", """controllers.GalleryController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/gallerydetails""", """controllers.GalleryDetailsController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getgallerydetails""", """controllers.GalleryDetailsController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/gallery-details/""" + "$" + """galleryId<[^/]+>""", """controllers.GalleryDetailsController.getgallerydetails(galleryId:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updategallerydetails/""" + "$" + """id<[^/]+>""", """controllers.GalleryDetailsController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletegallerydetails/""" + "$" + """id<[^/]+>""", """controllers.GalleryDetailsController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/gotra""", """controllers.GotraController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getgotra""", """controllers.GotraController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updategotra/""" + "$" + """id<[^/]+>""", """controllers.GotraController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletegotra/""" + "$" + """id<[^/]+>""", """controllers.GotraController.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/search/members""", """controllers.UserControllers.findMembers(name:String ?= "", gotra:String ?= "", city:String ?= "", state:String ?= "", country:String ?= "", profession:String ?= "", sonmaritalstatus:String ?= "", daughtermaritalstatus:String ?= "", brothermaritalstatus:String ?= "" )"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/home""", """controllers.HomeController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/gethome""", """controllers.HomeController.getall"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updatehome/""" + "$" + """id<[^/]+>""", """controllers.HomeController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deletehome/""" + "$" + """id<[^/]+>""", """controllers.HomeController.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """save/profileImage""", """controllers.MediaController.saveImage"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """what""", """controllers.MediaController.what"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/addadd""", """controllers.AddsController.add"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/displayadd""", """controllers.AddsController.getall"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """active/ads""", """controllers.AddsController.getAds"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/updateadd/""" + "$" + """id<[^/]+>""", """controllers.AddsController.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/deleteadd/""" + "$" + """id<[^/]+>""", """controllers.AddsController.delete(id:Long)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_Application_login0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_Application_login0_invoker = createInvoker(
    Application_19.login,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "login",
      Nil,
      "GET",
      this.prefix + """""",
      """ Home page""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_Application_options1_route = Route("OPTIONS",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_Application_options1_invoker = createInvoker(
    Application_19.options(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "options",
      Seq(classOf[String]),
      "OPTIONS",
      this.prefix + """""",
      """""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_Application_options2_route = Route("OPTIONS",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), DynamicPart("path", """.+""",false)))
  )
  private[this] lazy val controllers_Application_options2_invoker = createInvoker(
    Application_19.options(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "options",
      Seq(classOf[String]),
      "OPTIONS",
      this.prefix + """""" + "$" + """path<.+>""",
      """""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_Application_index3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("home")))
  )
  private[this] lazy val controllers_Application_index3_invoker = createInvoker(
    Application_19.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "index",
      Nil,
      "GET",
      this.prefix + """home""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_Application_logOut4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logout")))
  )
  private[this] lazy val controllers_Application_logOut4_invoker = createInvoker(
    Application_19.logOut,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "logOut",
      Nil,
      "GET",
      this.prefix + """logout""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_Assets_versioned5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned5_invoker = createInvoker(
    Assets_17.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_Application_getFile6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("media/"), DynamicPart("mediaName", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_getFile6_invoker = createInvoker(
    Application_19.getFile(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getFile",
      Seq(classOf[String]),
      "GET",
      this.prefix + """media/""" + "$" + """mediaName<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:20
  private[this] lazy val controllers_Application_getFileThumb7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("media/Thumbnail/"), DynamicPart("mediaName", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Application_getFileThumb7_invoker = createInvoker(
    Application_19.getFileThumb(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "getFileThumb",
      Seq(classOf[String]),
      "GET",
      this.prefix + """media/Thumbnail/""" + "$" + """mediaName<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_UserControllers_login8_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/login")))
  )
  private[this] lazy val controllers_UserControllers_login8_invoker = createInvoker(
    UserControllers_4.login,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "login",
      Nil,
      "POST",
      this.prefix + """api/login""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:31
  private[this] lazy val controllers_UserControllers_loginAPI9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/user/validate")))
  )
  private[this] lazy val controllers_UserControllers_loginAPI9_invoker = createInvoker(
    UserControllers_4.loginAPI,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "loginAPI",
      Nil,
      "POST",
      this.prefix + """api/user/validate""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:35
  private[this] lazy val controllers_UserControllers_add10_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/users")))
  )
  private[this] lazy val controllers_UserControllers_add10_invoker = createInvoker(
    UserControllers_4.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "add",
      Nil,
      "POST",
      this.prefix + """api/users""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:38
  private[this] lazy val controllers_UserControllers_getall11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getuser")))
  )
  private[this] lazy val controllers_UserControllers_getall11_invoker = createInvoker(
    UserControllers_4.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getuser""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:41
  private[this] lazy val controllers_UserControllers_get12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/user/"), DynamicPart("userId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserControllers_get12_invoker = createInvoker(
    UserControllers_4.get(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "get",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/user/""" + "$" + """userId<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:50
  private[this] lazy val controllers_UserControllers_update13_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateuser/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserControllers_update13_invoker = createInvoker(
    UserControllers_4.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateuser/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:53
  private[this] lazy val controllers_UserControllers_delete14_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteuser/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_UserControllers_delete14_invoker = createInvoker(
    UserControllers_4.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteuser/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:58
  private[this] lazy val controllers_MediaController_upload15_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("upload")))
  )
  private[this] lazy val controllers_MediaController_upload15_invoker = createInvoker(
    MediaController_0.upload,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MediaController",
      "upload",
      Nil,
      "POST",
      this.prefix + """upload""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:61
  private[this] lazy val controllers_MediaController_uploadmedia16_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploadmedia")))
  )
  private[this] lazy val controllers_MediaController_uploadmedia16_invoker = createInvoker(
    MediaController_0.uploadmedia,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MediaController",
      "uploadmedia",
      Nil,
      "POST",
      this.prefix + """uploadmedia""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:64
  private[this] lazy val controllers_MediaController_delete17_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletemedia/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MediaController_delete17_invoker = createInvoker(
    MediaController_0.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MediaController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletemedia/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:67
  private[this] lazy val controllers_MediaController_getmedia18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/displaymedia")))
  )
  private[this] lazy val controllers_MediaController_getmedia18_invoker = createInvoker(
    MediaController_0.getmedia,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MediaController",
      "getmedia",
      Nil,
      "GET",
      this.prefix + """api/displaymedia""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:75
  private[this] lazy val controllers_OrganizationController_add19_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/organization")))
  )
  private[this] lazy val controllers_OrganizationController_add19_invoker = createInvoker(
    OrganizationController_12.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/organization""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:78
  private[this] lazy val controllers_OrganizationController_getall20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getorganization")))
  )
  private[this] lazy val controllers_OrganizationController_getall20_invoker = createInvoker(
    OrganizationController_12.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getorganization""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:81
  private[this] lazy val controllers_OrganizationController_update21_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateorganization/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrganizationController_update21_invoker = createInvoker(
    OrganizationController_12.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateorganization/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:84
  private[this] lazy val controllers_OrganizationController_delete22_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteorganization/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrganizationController_delete22_invoker = createInvoker(
    OrganizationController_12.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteorganization/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:91
  private[this] lazy val controllers_OrganizationDetailsController_add23_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/organizationdetails")))
  )
  private[this] lazy val controllers_OrganizationDetailsController_add23_invoker = createInvoker(
    OrganizationDetailsController_21.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationDetailsController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/organizationdetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:94
  private[this] lazy val controllers_OrganizationDetailsController_getall24_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getorganizationdetails")))
  )
  private[this] lazy val controllers_OrganizationDetailsController_getall24_invoker = createInvoker(
    OrganizationDetailsController_21.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationDetailsController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getorganizationdetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:97
  private[this] lazy val controllers_OrganizationDetailsController_getorganizationDetails25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/organization-details/"), DynamicPart("orgId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrganizationDetailsController_getorganizationDetails25_invoker = createInvoker(
    OrganizationDetailsController_21.getorganizationDetails(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationDetailsController",
      "getorganizationDetails",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/organization-details/""" + "$" + """orgId<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:100
  private[this] lazy val controllers_OrganizationDetailsController_update26_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateorganizationdetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrganizationDetailsController_update26_invoker = createInvoker(
    OrganizationDetailsController_21.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationDetailsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateorganizationdetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:103
  private[this] lazy val controllers_OrganizationDetailsController_delete27_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteorganizationdetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrganizationDetailsController_delete27_invoker = createInvoker(
    OrganizationDetailsController_21.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrganizationDetailsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteorganizationdetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:110
  private[this] lazy val controllers_UserControllers_add28_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/registration")))
  )
  private[this] lazy val controllers_UserControllers_add28_invoker = createInvoker(
    UserControllers_4.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "add",
      Nil,
      "POST",
      this.prefix + """api/registration""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:113
  private[this] lazy val controllers_RegistrationController_getall29_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getregistration")))
  )
  private[this] lazy val controllers_RegistrationController_getall29_invoker = createInvoker(
    RegistrationController_24.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RegistrationController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getregistration""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:121
  private[this] lazy val controllers_AnnouncementController_add30_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/announcement")))
  )
  private[this] lazy val controllers_AnnouncementController_add30_invoker = createInvoker(
    AnnouncementController_25.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AnnouncementController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/announcement""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:124
  private[this] lazy val controllers_AnnouncementController_getall31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getannouncement")))
  )
  private[this] lazy val controllers_AnnouncementController_getall31_invoker = createInvoker(
    AnnouncementController_25.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AnnouncementController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getannouncement""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:127
  private[this] lazy val controllers_AnnouncementController_getall32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/announcement")))
  )
  private[this] lazy val controllers_AnnouncementController_getall32_invoker = createInvoker(
    AnnouncementController_25.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AnnouncementController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/announcement""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:130
  private[this] lazy val controllers_AnnouncementController_update33_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateannouncement/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AnnouncementController_update33_invoker = createInvoker(
    AnnouncementController_25.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AnnouncementController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateannouncement/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:133
  private[this] lazy val controllers_AnnouncementController_delete34_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteannouncement/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AnnouncementController_delete34_invoker = createInvoker(
    AnnouncementController_25.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AnnouncementController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteannouncement/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:141
  private[this] lazy val controllers_FormTitleController_add35_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/formtitle")))
  )
  private[this] lazy val controllers_FormTitleController_add35_invoker = createInvoker(
    FormTitleController_13.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormTitleController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/formtitle""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:144
  private[this] lazy val controllers_FormTitleController_getall36_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getformtitle")))
  )
  private[this] lazy val controllers_FormTitleController_getall36_invoker = createInvoker(
    FormTitleController_13.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormTitleController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getformtitle""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:148
  private[this] lazy val controllers_FormTitleController_update37_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateformtitle/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FormTitleController_update37_invoker = createInvoker(
    FormTitleController_13.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormTitleController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateformtitle/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:151
  private[this] lazy val controllers_FormTitleController_delete38_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteformtitle/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FormTitleController_delete38_invoker = createInvoker(
    FormTitleController_13.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormTitleController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteformtitle/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:156
  private[this] lazy val controllers_FormController_add39_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/form")))
  )
  private[this] lazy val controllers_FormController_add39_invoker = createInvoker(
    FormController_14.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/form""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:159
  private[this] lazy val controllers_FormController_getall40_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getform")))
  )
  private[this] lazy val controllers_FormController_getall40_invoker = createInvoker(
    FormController_14.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getform""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:162
  private[this] lazy val controllers_FormController_getform41_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getform-details/"), DynamicPart("formId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FormController_getform41_invoker = createInvoker(
    FormController_14.getform(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormController",
      "getform",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/getform-details/""" + "$" + """formId<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:165
  private[this] lazy val controllers_FormController_update42_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateform/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FormController_update42_invoker = createInvoker(
    FormController_14.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateform/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:168
  private[this] lazy val controllers_FormController_delete43_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteform/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FormController_delete43_invoker = createInvoker(
    FormController_14.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FormController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteform/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:175
  private[this] lazy val controllers_GoldSilverController_add44_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/goldSilver")))
  )
  private[this] lazy val controllers_GoldSilverController_add44_invoker = createInvoker(
    GoldSilverController_7.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GoldSilverController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/goldSilver""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:178
  private[this] lazy val controllers_GoldSilverController_getall45_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getgoldSilver")))
  )
  private[this] lazy val controllers_GoldSilverController_getall45_invoker = createInvoker(
    GoldSilverController_7.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GoldSilverController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getgoldSilver""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:181
  private[this] lazy val controllers_GoldSilverController_update46_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updategoldSilver/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GoldSilverController_update46_invoker = createInvoker(
    GoldSilverController_7.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GoldSilverController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updategoldSilver/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:184
  private[this] lazy val controllers_GoldSilverController_delete47_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletegoldSilver/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GoldSilverController_delete47_invoker = createInvoker(
    GoldSilverController_7.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GoldSilverController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletegoldSilver/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:190
  private[this] lazy val controllers_SuggestionController_add48_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/suggestion")))
  )
  private[this] lazy val controllers_SuggestionController_add48_invoker = createInvoker(
    SuggestionController_3.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuggestionController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/suggestion""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:193
  private[this] lazy val controllers_SuggestionController_getall49_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getsuggestion")))
  )
  private[this] lazy val controllers_SuggestionController_getall49_invoker = createInvoker(
    SuggestionController_3.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuggestionController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getsuggestion""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:196
  private[this] lazy val controllers_SuggestionController_update50_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updatesuggestion/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SuggestionController_update50_invoker = createInvoker(
    SuggestionController_3.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuggestionController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updatesuggestion/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:199
  private[this] lazy val controllers_SuggestionController_delete51_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletesuggestion/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SuggestionController_delete51_invoker = createInvoker(
    SuggestionController_3.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SuggestionController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletesuggestion/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:205
  private[this] lazy val controllers_AdmitCardController_add52_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/admitCard")))
  )
  private[this] lazy val controllers_AdmitCardController_add52_invoker = createInvoker(
    AdmitCardController_2.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdmitCardController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/admitCard""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:208
  private[this] lazy val controllers_AdmitCardController_getall53_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getadmitCard")))
  )
  private[this] lazy val controllers_AdmitCardController_getall53_invoker = createInvoker(
    AdmitCardController_2.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdmitCardController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getadmitCard""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:211
  private[this] lazy val controllers_AdmitCardController_update54_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateadmitCard/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdmitCardController_update54_invoker = createInvoker(
    AdmitCardController_2.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdmitCardController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateadmitCard/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:214
  private[this] lazy val controllers_AdmitCardController_delete55_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteadmitCard/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdmitCardController_delete55_invoker = createInvoker(
    AdmitCardController_2.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdmitCardController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteadmitCard/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:222
  private[this] lazy val controllers_ChatController_add56_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/send-message")))
  )
  private[this] lazy val controllers_ChatController_add56_invoker = createInvoker(
    ChatController_23.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChatController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/send-message""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:225
  private[this] lazy val controllers_ChatController_chatUpload57_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/send-media")))
  )
  private[this] lazy val controllers_ChatController_chatUpload57_invoker = createInvoker(
    ChatController_23.chatUpload,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChatController",
      "chatUpload",
      Nil,
      "POST",
      this.prefix + """api/send-media""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:229
  private[this] lazy val controllers_ChatController_getall58_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/list/messages")))
  )
  private[this] lazy val controllers_ChatController_getall58_invoker = createInvoker(
    ChatController_23.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChatController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/list/messages""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:232
  private[this] lazy val controllers_ChatController_getMessages59_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/list/message")))
  )
  private[this] lazy val controllers_ChatController_getMessages59_invoker = createInvoker(
    ChatController_23.getMessages(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChatController",
      "getMessages",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/list/message""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:235
  private[this] lazy val controllers_ChatController_getLatestMessages60_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/list/latest-message")))
  )
  private[this] lazy val controllers_ChatController_getLatestMessages60_invoker = createInvoker(
    ChatController_23.getLatestMessages(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChatController",
      "getLatestMessages",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/list/latest-message""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:238
  private[this] lazy val controllers_ChatController_update61_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updatechat/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ChatController_update61_invoker = createInvoker(
    ChatController_23.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChatController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updatechat/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:241
  private[this] lazy val controllers_ChatController_delete62_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletechat/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ChatController_delete62_invoker = createInvoker(
    ChatController_23.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChatController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletechat/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:244
  private[this] lazy val controllers_ChatController_delete63_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/delete/message/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ChatController_delete63_invoker = createInvoker(
    ChatController_23.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ChatController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/delete/message/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:251
  private[this] lazy val controllers_AchievmentController_add64_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/achievment")))
  )
  private[this] lazy val controllers_AchievmentController_add64_invoker = createInvoker(
    AchievmentController_5.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/achievment""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:254
  private[this] lazy val controllers_AchievmentController_getall65_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getachievment")))
  )
  private[this] lazy val controllers_AchievmentController_getall65_invoker = createInvoker(
    AchievmentController_5.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getachievment""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:257
  private[this] lazy val controllers_AchievmentController_update66_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateachievment/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AchievmentController_update66_invoker = createInvoker(
    AchievmentController_5.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateachievment/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:260
  private[this] lazy val controllers_AchievmentController_delete67_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteachievment/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AchievmentController_delete67_invoker = createInvoker(
    AchievmentController_5.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteachievment/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:267
  private[this] lazy val controllers_AchievmentLocationController_add68_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/achievmentlocation")))
  )
  private[this] lazy val controllers_AchievmentLocationController_add68_invoker = createInvoker(
    AchievmentLocationController_22.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentLocationController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/achievmentlocation""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:270
  private[this] lazy val controllers_AchievmentLocationController_getall69_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getachievmentlocation")))
  )
  private[this] lazy val controllers_AchievmentLocationController_getall69_invoker = createInvoker(
    AchievmentLocationController_22.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentLocationController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getachievmentlocation""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:273
  private[this] lazy val controllers_AchievmentLocationController_getdata70_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getachievment-location/"), DynamicPart("locid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AchievmentLocationController_getdata70_invoker = createInvoker(
    AchievmentLocationController_22.getdata(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentLocationController",
      "getdata",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/getachievment-location/""" + "$" + """locid<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:277
  private[this] lazy val controllers_AchievmentLocationController_update71_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateachievmentlocation/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AchievmentLocationController_update71_invoker = createInvoker(
    AchievmentLocationController_22.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentLocationController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateachievmentlocation/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:280
  private[this] lazy val controllers_AchievmentLocationController_delete72_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteachievmentlocation/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AchievmentLocationController_delete72_invoker = createInvoker(
    AchievmentLocationController_22.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentLocationController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteachievmentlocation/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:288
  private[this] lazy val controllers_AchievmentDetailsController_add73_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/ahievmentdetails")))
  )
  private[this] lazy val controllers_AchievmentDetailsController_add73_invoker = createInvoker(
    AchievmentDetailsController_18.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentDetailsController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/ahievmentdetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:291
  private[this] lazy val controllers_AchievmentDetailsController_getall74_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getahievmentdetails")))
  )
  private[this] lazy val controllers_AchievmentDetailsController_getall74_invoker = createInvoker(
    AchievmentDetailsController_18.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentDetailsController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getahievmentdetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:294
  private[this] lazy val controllers_AchievmentDetailsController_getdata75_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getachievment-details/"), DynamicPart("achievmentdetailsid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AchievmentDetailsController_getdata75_invoker = createInvoker(
    AchievmentDetailsController_18.getdata(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentDetailsController",
      "getdata",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/getachievment-details/""" + "$" + """achievmentdetailsid<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:299
  private[this] lazy val controllers_AchievmentDetailsController_update76_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateahievmentdetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AchievmentDetailsController_update76_invoker = createInvoker(
    AchievmentDetailsController_18.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentDetailsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateahievmentdetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:302
  private[this] lazy val controllers_AchievmentDetailsController_delete77_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteahievmentdetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AchievmentDetailsController_delete77_invoker = createInvoker(
    AchievmentDetailsController_18.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AchievmentDetailsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteahievmentdetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:308
  private[this] lazy val controllers_FacebookController_add78_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/facebook")))
  )
  private[this] lazy val controllers_FacebookController_add78_invoker = createInvoker(
    FacebookController_16.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/facebook""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:311
  private[this] lazy val controllers_FacebookController_getall79_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getfacebook")))
  )
  private[this] lazy val controllers_FacebookController_getall79_invoker = createInvoker(
    FacebookController_16.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getfacebook""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:314
  private[this] lazy val controllers_FacebookController_update80_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updatefacebook/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FacebookController_update80_invoker = createInvoker(
    FacebookController_16.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updatefacebook/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:317
  private[this] lazy val controllers_FacebookController_delete81_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletefacebook/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FacebookController_delete81_invoker = createInvoker(
    FacebookController_16.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletefacebook/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:323
  private[this] lazy val controllers_FacebookDetailsController_add82_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/facebookDetails")))
  )
  private[this] lazy val controllers_FacebookDetailsController_add82_invoker = createInvoker(
    FacebookDetailsController_9.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookDetailsController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/facebookDetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:326
  private[this] lazy val controllers_FacebookDetailsController_getall83_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getfacebookDetails")))
  )
  private[this] lazy val controllers_FacebookDetailsController_getall83_invoker = createInvoker(
    FacebookDetailsController_9.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookDetailsController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getfacebookDetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:329
  private[this] lazy val controllers_FacebookDetailsController_getfacebookdetails84_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getfacebook-details/"), DynamicPart("locid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FacebookDetailsController_getfacebookdetails84_invoker = createInvoker(
    FacebookDetailsController_9.getfacebookdetails(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookDetailsController",
      "getfacebookdetails",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/getfacebook-details/""" + "$" + """locid<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:333
  private[this] lazy val controllers_FacebookDetailsController_update85_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updatefacebookDetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FacebookDetailsController_update85_invoker = createInvoker(
    FacebookDetailsController_9.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookDetailsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updatefacebookDetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:336
  private[this] lazy val controllers_FacebookDetailsController_delete86_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletefacebookDetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_FacebookDetailsController_delete86_invoker = createInvoker(
    FacebookDetailsController_9.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.FacebookDetailsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletefacebookDetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:342
  private[this] lazy val controllers_MailController_add87_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/mail")))
  )
  private[this] lazy val controllers_MailController_add87_invoker = createInvoker(
    MailController_20.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/mail""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:345
  private[this] lazy val controllers_MailController_getall88_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getmail")))
  )
  private[this] lazy val controllers_MailController_getall88_invoker = createInvoker(
    MailController_20.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getmail""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:348
  private[this] lazy val controllers_MailController_update89_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updatemail/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MailController_update89_invoker = createInvoker(
    MailController_20.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updatemail/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:351
  private[this] lazy val controllers_MailController_delete90_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletemail/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MailController_delete90_invoker = createInvoker(
    MailController_20.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletemail/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:358
  private[this] lazy val controllers_MailDetailsController_add91_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/maildetails")))
  )
  private[this] lazy val controllers_MailDetailsController_add91_invoker = createInvoker(
    MailDetailsController_8.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailDetailsController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/maildetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:361
  private[this] lazy val controllers_MailDetailsController_getall92_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getmaildetails")))
  )
  private[this] lazy val controllers_MailDetailsController_getall92_invoker = createInvoker(
    MailDetailsController_8.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailDetailsController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getmaildetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:364
  private[this] lazy val controllers_MailDetailsController_getmaildetails93_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getmail-details/"), DynamicPart("mailid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MailDetailsController_getmaildetails93_invoker = createInvoker(
    MailDetailsController_8.getmaildetails(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailDetailsController",
      "getmaildetails",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/getmail-details/""" + "$" + """mailid<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:368
  private[this] lazy val controllers_MailDetailsController_update94_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updatemaildetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MailDetailsController_update94_invoker = createInvoker(
    MailDetailsController_8.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailDetailsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updatemaildetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:371
  private[this] lazy val controllers_MailDetailsController_delete95_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletemaildetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MailDetailsController_delete95_invoker = createInvoker(
    MailDetailsController_8.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MailDetailsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletemaildetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:377
  private[this] lazy val controllers_GalleryController_add96_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/gallery")))
  )
  private[this] lazy val controllers_GalleryController_add96_invoker = createInvoker(
    GalleryController_1.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/gallery""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:380
  private[this] lazy val controllers_GalleryController_getall97_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getgallery")))
  )
  private[this] lazy val controllers_GalleryController_getall97_invoker = createInvoker(
    GalleryController_1.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getgallery""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:383
  private[this] lazy val controllers_GalleryController_update98_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updategallery/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GalleryController_update98_invoker = createInvoker(
    GalleryController_1.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updategallery/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:386
  private[this] lazy val controllers_GalleryController_delete99_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletegallery/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GalleryController_delete99_invoker = createInvoker(
    GalleryController_1.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletegallery/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:392
  private[this] lazy val controllers_GalleryDetailsController_add100_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/gallerydetails")))
  )
  private[this] lazy val controllers_GalleryDetailsController_add100_invoker = createInvoker(
    GalleryDetailsController_15.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryDetailsController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/gallerydetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:395
  private[this] lazy val controllers_GalleryDetailsController_getall101_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getgallerydetails")))
  )
  private[this] lazy val controllers_GalleryDetailsController_getall101_invoker = createInvoker(
    GalleryDetailsController_15.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryDetailsController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getgallerydetails""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:398
  private[this] lazy val controllers_GalleryDetailsController_getgallerydetails102_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/gallery-details/"), DynamicPart("galleryId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GalleryDetailsController_getgallerydetails102_invoker = createInvoker(
    GalleryDetailsController_15.getgallerydetails(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryDetailsController",
      "getgallerydetails",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """api/gallery-details/""" + "$" + """galleryId<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:401
  private[this] lazy val controllers_GalleryDetailsController_update103_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updategallerydetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GalleryDetailsController_update103_invoker = createInvoker(
    GalleryDetailsController_15.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryDetailsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updategallerydetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:404
  private[this] lazy val controllers_GalleryDetailsController_delete104_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletegallerydetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GalleryDetailsController_delete104_invoker = createInvoker(
    GalleryDetailsController_15.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GalleryDetailsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletegallerydetails/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:411
  private[this] lazy val controllers_GotraController_add105_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/gotra")))
  )
  private[this] lazy val controllers_GotraController_add105_invoker = createInvoker(
    GotraController_11.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GotraController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/gotra""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:414
  private[this] lazy val controllers_GotraController_getall106_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getgotra")))
  )
  private[this] lazy val controllers_GotraController_getall106_invoker = createInvoker(
    GotraController_11.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GotraController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/getgotra""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:417
  private[this] lazy val controllers_GotraController_update107_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updategotra/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GotraController_update107_invoker = createInvoker(
    GotraController_11.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GotraController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updategotra/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:420
  private[this] lazy val controllers_GotraController_delete108_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletegotra/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GotraController_delete108_invoker = createInvoker(
    GotraController_11.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GotraController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletegotra/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:424
  private[this] lazy val controllers_UserControllers_findMembers109_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/search/members")))
  )
  private[this] lazy val controllers_UserControllers_findMembers109_invoker = createInvoker(
    UserControllers_4.findMembers(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserControllers",
      "findMembers",
      Seq(classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String], classOf[String]),
      "GET",
      this.prefix + """api/search/members""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:431
  private[this] lazy val controllers_HomeController_add110_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/home")))
  )
  private[this] lazy val controllers_HomeController_add110_invoker = createInvoker(
    HomeController_6.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/home""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:434
  private[this] lazy val controllers_HomeController_getall111_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/gethome")))
  )
  private[this] lazy val controllers_HomeController_getall111_invoker = createInvoker(
    HomeController_6.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/gethome""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:437
  private[this] lazy val controllers_HomeController_update112_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updatehome/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_update112_invoker = createInvoker(
    HomeController_6.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updatehome/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:440
  private[this] lazy val controllers_HomeController_delete113_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deletehome/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_delete113_invoker = createInvoker(
    HomeController_6.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deletehome/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:443
  private[this] lazy val controllers_MediaController_saveImage114_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("save/profileImage")))
  )
  private[this] lazy val controllers_MediaController_saveImage114_invoker = createInvoker(
    MediaController_0.saveImage,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MediaController",
      "saveImage",
      Nil,
      "POST",
      this.prefix + """save/profileImage""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:446
  private[this] lazy val controllers_MediaController_what115_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("what")))
  )
  private[this] lazy val controllers_MediaController_what115_invoker = createInvoker(
    MediaController_0.what,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MediaController",
      "what",
      Nil,
      "POST",
      this.prefix + """what""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:451
  private[this] lazy val controllers_AddsController_add116_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/addadd")))
  )
  private[this] lazy val controllers_AddsController_add116_invoker = createInvoker(
    AddsController_10.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AddsController",
      "add",
      Nil,
      "POST",
      this.prefix + """api/addadd""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:454
  private[this] lazy val controllers_AddsController_getall117_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/displayadd")))
  )
  private[this] lazy val controllers_AddsController_getall117_invoker = createInvoker(
    AddsController_10.getall,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AddsController",
      "getall",
      Nil,
      "GET",
      this.prefix + """api/displayadd""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:457
  private[this] lazy val controllers_AddsController_getAds118_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("active/ads")))
  )
  private[this] lazy val controllers_AddsController_getAds118_invoker = createInvoker(
    AddsController_10.getAds,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AddsController",
      "getAds",
      Nil,
      "GET",
      this.prefix + """active/ads""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:461
  private[this] lazy val controllers_AddsController_update119_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/updateadd/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AddsController_update119_invoker = createInvoker(
    AddsController_10.update(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AddsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      this.prefix + """api/updateadd/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )

  // @LINE:464
  private[this] lazy val controllers_AddsController_delete120_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/deleteadd/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AddsController_delete120_invoker = createInvoker(
    AddsController_10.delete(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AddsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      this.prefix + """api/deleteadd/""" + "$" + """id<[^/]+>""",
      """""",
      Seq("""nocsrf""")
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_Application_login0_route(params@_) =>
      call { 
        controllers_Application_login0_invoker.call(Application_19.login)
      }
  
    // @LINE:8
    case controllers_Application_options1_route(params@_) =>
      call(params.fromQuery[String]("path", Some(""))) { (path) =>
        controllers_Application_options1_invoker.call(Application_19.options(path))
      }
  
    // @LINE:9
    case controllers_Application_options2_route(params@_) =>
      call(params.fromPath[String]("path", None)) { (path) =>
        controllers_Application_options2_invoker.call(Application_19.options(path))
      }
  
    // @LINE:11
    case controllers_Application_index3_route(params@_) =>
      call { 
        controllers_Application_index3_invoker.call(Application_19.index)
      }
  
    // @LINE:13
    case controllers_Application_logOut4_route(params@_) =>
      call { 
        controllers_Application_logOut4_invoker.call(Application_19.logOut)
      }
  
    // @LINE:16
    case controllers_Assets_versioned5_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned5_invoker.call(Assets_17.versioned(path, file))
      }
  
    // @LINE:19
    case controllers_Application_getFile6_route(params@_) =>
      call(params.fromPath[String]("mediaName", None)) { (mediaName) =>
        controllers_Application_getFile6_invoker.call(Application_19.getFile(mediaName))
      }
  
    // @LINE:20
    case controllers_Application_getFileThumb7_route(params@_) =>
      call(params.fromPath[String]("mediaName", None)) { (mediaName) =>
        controllers_Application_getFileThumb7_invoker.call(Application_19.getFileThumb(mediaName))
      }
  
    // @LINE:28
    case controllers_UserControllers_login8_route(params@_) =>
      call { 
        controllers_UserControllers_login8_invoker.call(UserControllers_4.login)
      }
  
    // @LINE:31
    case controllers_UserControllers_loginAPI9_route(params@_) =>
      call { 
        controllers_UserControllers_loginAPI9_invoker.call(UserControllers_4.loginAPI)
      }
  
    // @LINE:35
    case controllers_UserControllers_add10_route(params@_) =>
      call { 
        controllers_UserControllers_add10_invoker.call(UserControllers_4.add)
      }
  
    // @LINE:38
    case controllers_UserControllers_getall11_route(params@_) =>
      call { 
        controllers_UserControllers_getall11_invoker.call(UserControllers_4.getall)
      }
  
    // @LINE:41
    case controllers_UserControllers_get12_route(params@_) =>
      call(params.fromPath[Long]("userId", None)) { (userId) =>
        controllers_UserControllers_get12_invoker.call(UserControllers_4.get(userId))
      }
  
    // @LINE:50
    case controllers_UserControllers_update13_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_UserControllers_update13_invoker.call(UserControllers_4.update(id))
      }
  
    // @LINE:53
    case controllers_UserControllers_delete14_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_UserControllers_delete14_invoker.call(UserControllers_4.delete(id))
      }
  
    // @LINE:58
    case controllers_MediaController_upload15_route(params@_) =>
      call { 
        controllers_MediaController_upload15_invoker.call(MediaController_0.upload)
      }
  
    // @LINE:61
    case controllers_MediaController_uploadmedia16_route(params@_) =>
      call { 
        controllers_MediaController_uploadmedia16_invoker.call(MediaController_0.uploadmedia)
      }
  
    // @LINE:64
    case controllers_MediaController_delete17_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_MediaController_delete17_invoker.call(MediaController_0.delete(id))
      }
  
    // @LINE:67
    case controllers_MediaController_getmedia18_route(params@_) =>
      call { 
        controllers_MediaController_getmedia18_invoker.call(MediaController_0.getmedia)
      }
  
    // @LINE:75
    case controllers_OrganizationController_add19_route(params@_) =>
      call { 
        controllers_OrganizationController_add19_invoker.call(OrganizationController_12.add)
      }
  
    // @LINE:78
    case controllers_OrganizationController_getall20_route(params@_) =>
      call { 
        controllers_OrganizationController_getall20_invoker.call(OrganizationController_12.getall)
      }
  
    // @LINE:81
    case controllers_OrganizationController_update21_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_OrganizationController_update21_invoker.call(OrganizationController_12.update(id))
      }
  
    // @LINE:84
    case controllers_OrganizationController_delete22_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_OrganizationController_delete22_invoker.call(OrganizationController_12.delete(id))
      }
  
    // @LINE:91
    case controllers_OrganizationDetailsController_add23_route(params@_) =>
      call { 
        controllers_OrganizationDetailsController_add23_invoker.call(OrganizationDetailsController_21.add)
      }
  
    // @LINE:94
    case controllers_OrganizationDetailsController_getall24_route(params@_) =>
      call { 
        controllers_OrganizationDetailsController_getall24_invoker.call(OrganizationDetailsController_21.getall)
      }
  
    // @LINE:97
    case controllers_OrganizationDetailsController_getorganizationDetails25_route(params@_) =>
      call(params.fromPath[Long]("orgId", None)) { (orgId) =>
        controllers_OrganizationDetailsController_getorganizationDetails25_invoker.call(OrganizationDetailsController_21.getorganizationDetails(orgId))
      }
  
    // @LINE:100
    case controllers_OrganizationDetailsController_update26_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_OrganizationDetailsController_update26_invoker.call(OrganizationDetailsController_21.update(id))
      }
  
    // @LINE:103
    case controllers_OrganizationDetailsController_delete27_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_OrganizationDetailsController_delete27_invoker.call(OrganizationDetailsController_21.delete(id))
      }
  
    // @LINE:110
    case controllers_UserControllers_add28_route(params@_) =>
      call { 
        controllers_UserControllers_add28_invoker.call(UserControllers_4.add)
      }
  
    // @LINE:113
    case controllers_RegistrationController_getall29_route(params@_) =>
      call { 
        controllers_RegistrationController_getall29_invoker.call(RegistrationController_24.getall)
      }
  
    // @LINE:121
    case controllers_AnnouncementController_add30_route(params@_) =>
      call { 
        controllers_AnnouncementController_add30_invoker.call(AnnouncementController_25.add)
      }
  
    // @LINE:124
    case controllers_AnnouncementController_getall31_route(params@_) =>
      call { 
        controllers_AnnouncementController_getall31_invoker.call(AnnouncementController_25.getall)
      }
  
    // @LINE:127
    case controllers_AnnouncementController_getall32_route(params@_) =>
      call { 
        controllers_AnnouncementController_getall32_invoker.call(AnnouncementController_25.getall)
      }
  
    // @LINE:130
    case controllers_AnnouncementController_update33_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AnnouncementController_update33_invoker.call(AnnouncementController_25.update(id))
      }
  
    // @LINE:133
    case controllers_AnnouncementController_delete34_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AnnouncementController_delete34_invoker.call(AnnouncementController_25.delete(id))
      }
  
    // @LINE:141
    case controllers_FormTitleController_add35_route(params@_) =>
      call { 
        controllers_FormTitleController_add35_invoker.call(FormTitleController_13.add)
      }
  
    // @LINE:144
    case controllers_FormTitleController_getall36_route(params@_) =>
      call { 
        controllers_FormTitleController_getall36_invoker.call(FormTitleController_13.getall)
      }
  
    // @LINE:148
    case controllers_FormTitleController_update37_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_FormTitleController_update37_invoker.call(FormTitleController_13.update(id))
      }
  
    // @LINE:151
    case controllers_FormTitleController_delete38_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_FormTitleController_delete38_invoker.call(FormTitleController_13.delete(id))
      }
  
    // @LINE:156
    case controllers_FormController_add39_route(params@_) =>
      call { 
        controllers_FormController_add39_invoker.call(FormController_14.add)
      }
  
    // @LINE:159
    case controllers_FormController_getall40_route(params@_) =>
      call { 
        controllers_FormController_getall40_invoker.call(FormController_14.getall)
      }
  
    // @LINE:162
    case controllers_FormController_getform41_route(params@_) =>
      call(params.fromPath[Long]("formId", None)) { (formId) =>
        controllers_FormController_getform41_invoker.call(FormController_14.getform(formId))
      }
  
    // @LINE:165
    case controllers_FormController_update42_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_FormController_update42_invoker.call(FormController_14.update(id))
      }
  
    // @LINE:168
    case controllers_FormController_delete43_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_FormController_delete43_invoker.call(FormController_14.delete(id))
      }
  
    // @LINE:175
    case controllers_GoldSilverController_add44_route(params@_) =>
      call { 
        controllers_GoldSilverController_add44_invoker.call(GoldSilverController_7.add)
      }
  
    // @LINE:178
    case controllers_GoldSilverController_getall45_route(params@_) =>
      call { 
        controllers_GoldSilverController_getall45_invoker.call(GoldSilverController_7.getall)
      }
  
    // @LINE:181
    case controllers_GoldSilverController_update46_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GoldSilverController_update46_invoker.call(GoldSilverController_7.update(id))
      }
  
    // @LINE:184
    case controllers_GoldSilverController_delete47_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GoldSilverController_delete47_invoker.call(GoldSilverController_7.delete(id))
      }
  
    // @LINE:190
    case controllers_SuggestionController_add48_route(params@_) =>
      call { 
        controllers_SuggestionController_add48_invoker.call(SuggestionController_3.add)
      }
  
    // @LINE:193
    case controllers_SuggestionController_getall49_route(params@_) =>
      call { 
        controllers_SuggestionController_getall49_invoker.call(SuggestionController_3.getall)
      }
  
    // @LINE:196
    case controllers_SuggestionController_update50_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_SuggestionController_update50_invoker.call(SuggestionController_3.update(id))
      }
  
    // @LINE:199
    case controllers_SuggestionController_delete51_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_SuggestionController_delete51_invoker.call(SuggestionController_3.delete(id))
      }
  
    // @LINE:205
    case controllers_AdmitCardController_add52_route(params@_) =>
      call { 
        controllers_AdmitCardController_add52_invoker.call(AdmitCardController_2.add)
      }
  
    // @LINE:208
    case controllers_AdmitCardController_getall53_route(params@_) =>
      call { 
        controllers_AdmitCardController_getall53_invoker.call(AdmitCardController_2.getall)
      }
  
    // @LINE:211
    case controllers_AdmitCardController_update54_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AdmitCardController_update54_invoker.call(AdmitCardController_2.update(id))
      }
  
    // @LINE:214
    case controllers_AdmitCardController_delete55_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AdmitCardController_delete55_invoker.call(AdmitCardController_2.delete(id))
      }
  
    // @LINE:222
    case controllers_ChatController_add56_route(params@_) =>
      call { 
        controllers_ChatController_add56_invoker.call(ChatController_23.add)
      }
  
    // @LINE:225
    case controllers_ChatController_chatUpload57_route(params@_) =>
      call { 
        controllers_ChatController_chatUpload57_invoker.call(ChatController_23.chatUpload)
      }
  
    // @LINE:229
    case controllers_ChatController_getall58_route(params@_) =>
      call { 
        controllers_ChatController_getall58_invoker.call(ChatController_23.getall)
      }
  
    // @LINE:232
    case controllers_ChatController_getMessages59_route(params@_) =>
      call(params.fromQuery[Int]("page", Some(0))) { (page) =>
        controllers_ChatController_getMessages59_invoker.call(ChatController_23.getMessages(page))
      }
  
    // @LINE:235
    case controllers_ChatController_getLatestMessages60_route(params@_) =>
      call(params.fromQuery[Long]("id", Some(0))) { (id) =>
        controllers_ChatController_getLatestMessages60_invoker.call(ChatController_23.getLatestMessages(id))
      }
  
    // @LINE:238
    case controllers_ChatController_update61_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_ChatController_update61_invoker.call(ChatController_23.update(id))
      }
  
    // @LINE:241
    case controllers_ChatController_delete62_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_ChatController_delete62_invoker.call(ChatController_23.delete(id))
      }
  
    // @LINE:244
    case controllers_ChatController_delete63_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_ChatController_delete63_invoker.call(ChatController_23.delete(id))
      }
  
    // @LINE:251
    case controllers_AchievmentController_add64_route(params@_) =>
      call { 
        controllers_AchievmentController_add64_invoker.call(AchievmentController_5.add)
      }
  
    // @LINE:254
    case controllers_AchievmentController_getall65_route(params@_) =>
      call { 
        controllers_AchievmentController_getall65_invoker.call(AchievmentController_5.getall)
      }
  
    // @LINE:257
    case controllers_AchievmentController_update66_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AchievmentController_update66_invoker.call(AchievmentController_5.update(id))
      }
  
    // @LINE:260
    case controllers_AchievmentController_delete67_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AchievmentController_delete67_invoker.call(AchievmentController_5.delete(id))
      }
  
    // @LINE:267
    case controllers_AchievmentLocationController_add68_route(params@_) =>
      call { 
        controllers_AchievmentLocationController_add68_invoker.call(AchievmentLocationController_22.add)
      }
  
    // @LINE:270
    case controllers_AchievmentLocationController_getall69_route(params@_) =>
      call { 
        controllers_AchievmentLocationController_getall69_invoker.call(AchievmentLocationController_22.getall)
      }
  
    // @LINE:273
    case controllers_AchievmentLocationController_getdata70_route(params@_) =>
      call(params.fromPath[Long]("locid", None)) { (locid) =>
        controllers_AchievmentLocationController_getdata70_invoker.call(AchievmentLocationController_22.getdata(locid))
      }
  
    // @LINE:277
    case controllers_AchievmentLocationController_update71_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AchievmentLocationController_update71_invoker.call(AchievmentLocationController_22.update(id))
      }
  
    // @LINE:280
    case controllers_AchievmentLocationController_delete72_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AchievmentLocationController_delete72_invoker.call(AchievmentLocationController_22.delete(id))
      }
  
    // @LINE:288
    case controllers_AchievmentDetailsController_add73_route(params@_) =>
      call { 
        controllers_AchievmentDetailsController_add73_invoker.call(AchievmentDetailsController_18.add)
      }
  
    // @LINE:291
    case controllers_AchievmentDetailsController_getall74_route(params@_) =>
      call { 
        controllers_AchievmentDetailsController_getall74_invoker.call(AchievmentDetailsController_18.getall)
      }
  
    // @LINE:294
    case controllers_AchievmentDetailsController_getdata75_route(params@_) =>
      call(params.fromPath[Long]("achievmentdetailsid", None)) { (achievmentdetailsid) =>
        controllers_AchievmentDetailsController_getdata75_invoker.call(AchievmentDetailsController_18.getdata(achievmentdetailsid))
      }
  
    // @LINE:299
    case controllers_AchievmentDetailsController_update76_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AchievmentDetailsController_update76_invoker.call(AchievmentDetailsController_18.update(id))
      }
  
    // @LINE:302
    case controllers_AchievmentDetailsController_delete77_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AchievmentDetailsController_delete77_invoker.call(AchievmentDetailsController_18.delete(id))
      }
  
    // @LINE:308
    case controllers_FacebookController_add78_route(params@_) =>
      call { 
        controllers_FacebookController_add78_invoker.call(FacebookController_16.add)
      }
  
    // @LINE:311
    case controllers_FacebookController_getall79_route(params@_) =>
      call { 
        controllers_FacebookController_getall79_invoker.call(FacebookController_16.getall)
      }
  
    // @LINE:314
    case controllers_FacebookController_update80_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_FacebookController_update80_invoker.call(FacebookController_16.update(id))
      }
  
    // @LINE:317
    case controllers_FacebookController_delete81_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_FacebookController_delete81_invoker.call(FacebookController_16.delete(id))
      }
  
    // @LINE:323
    case controllers_FacebookDetailsController_add82_route(params@_) =>
      call { 
        controllers_FacebookDetailsController_add82_invoker.call(FacebookDetailsController_9.add)
      }
  
    // @LINE:326
    case controllers_FacebookDetailsController_getall83_route(params@_) =>
      call { 
        controllers_FacebookDetailsController_getall83_invoker.call(FacebookDetailsController_9.getall)
      }
  
    // @LINE:329
    case controllers_FacebookDetailsController_getfacebookdetails84_route(params@_) =>
      call(params.fromPath[Long]("locid", None)) { (locid) =>
        controllers_FacebookDetailsController_getfacebookdetails84_invoker.call(FacebookDetailsController_9.getfacebookdetails(locid))
      }
  
    // @LINE:333
    case controllers_FacebookDetailsController_update85_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_FacebookDetailsController_update85_invoker.call(FacebookDetailsController_9.update(id))
      }
  
    // @LINE:336
    case controllers_FacebookDetailsController_delete86_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_FacebookDetailsController_delete86_invoker.call(FacebookDetailsController_9.delete(id))
      }
  
    // @LINE:342
    case controllers_MailController_add87_route(params@_) =>
      call { 
        controllers_MailController_add87_invoker.call(MailController_20.add)
      }
  
    // @LINE:345
    case controllers_MailController_getall88_route(params@_) =>
      call { 
        controllers_MailController_getall88_invoker.call(MailController_20.getall)
      }
  
    // @LINE:348
    case controllers_MailController_update89_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_MailController_update89_invoker.call(MailController_20.update(id))
      }
  
    // @LINE:351
    case controllers_MailController_delete90_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_MailController_delete90_invoker.call(MailController_20.delete(id))
      }
  
    // @LINE:358
    case controllers_MailDetailsController_add91_route(params@_) =>
      call { 
        controllers_MailDetailsController_add91_invoker.call(MailDetailsController_8.add)
      }
  
    // @LINE:361
    case controllers_MailDetailsController_getall92_route(params@_) =>
      call { 
        controllers_MailDetailsController_getall92_invoker.call(MailDetailsController_8.getall)
      }
  
    // @LINE:364
    case controllers_MailDetailsController_getmaildetails93_route(params@_) =>
      call(params.fromPath[Long]("mailid", None)) { (mailid) =>
        controllers_MailDetailsController_getmaildetails93_invoker.call(MailDetailsController_8.getmaildetails(mailid))
      }
  
    // @LINE:368
    case controllers_MailDetailsController_update94_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_MailDetailsController_update94_invoker.call(MailDetailsController_8.update(id))
      }
  
    // @LINE:371
    case controllers_MailDetailsController_delete95_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_MailDetailsController_delete95_invoker.call(MailDetailsController_8.delete(id))
      }
  
    // @LINE:377
    case controllers_GalleryController_add96_route(params@_) =>
      call { 
        controllers_GalleryController_add96_invoker.call(GalleryController_1.add)
      }
  
    // @LINE:380
    case controllers_GalleryController_getall97_route(params@_) =>
      call { 
        controllers_GalleryController_getall97_invoker.call(GalleryController_1.getall)
      }
  
    // @LINE:383
    case controllers_GalleryController_update98_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GalleryController_update98_invoker.call(GalleryController_1.update(id))
      }
  
    // @LINE:386
    case controllers_GalleryController_delete99_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GalleryController_delete99_invoker.call(GalleryController_1.delete(id))
      }
  
    // @LINE:392
    case controllers_GalleryDetailsController_add100_route(params@_) =>
      call { 
        controllers_GalleryDetailsController_add100_invoker.call(GalleryDetailsController_15.add)
      }
  
    // @LINE:395
    case controllers_GalleryDetailsController_getall101_route(params@_) =>
      call { 
        controllers_GalleryDetailsController_getall101_invoker.call(GalleryDetailsController_15.getall)
      }
  
    // @LINE:398
    case controllers_GalleryDetailsController_getgallerydetails102_route(params@_) =>
      call(params.fromPath[Long]("galleryId", None)) { (galleryId) =>
        controllers_GalleryDetailsController_getgallerydetails102_invoker.call(GalleryDetailsController_15.getgallerydetails(galleryId))
      }
  
    // @LINE:401
    case controllers_GalleryDetailsController_update103_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GalleryDetailsController_update103_invoker.call(GalleryDetailsController_15.update(id))
      }
  
    // @LINE:404
    case controllers_GalleryDetailsController_delete104_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GalleryDetailsController_delete104_invoker.call(GalleryDetailsController_15.delete(id))
      }
  
    // @LINE:411
    case controllers_GotraController_add105_route(params@_) =>
      call { 
        controllers_GotraController_add105_invoker.call(GotraController_11.add)
      }
  
    // @LINE:414
    case controllers_GotraController_getall106_route(params@_) =>
      call { 
        controllers_GotraController_getall106_invoker.call(GotraController_11.getall)
      }
  
    // @LINE:417
    case controllers_GotraController_update107_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GotraController_update107_invoker.call(GotraController_11.update(id))
      }
  
    // @LINE:420
    case controllers_GotraController_delete108_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_GotraController_delete108_invoker.call(GotraController_11.delete(id))
      }
  
    // @LINE:424
    case controllers_UserControllers_findMembers109_route(params@_) =>
      call(params.fromQuery[String]("name", Some("")), params.fromQuery[String]("gotra", Some("")), params.fromQuery[String]("city", Some("")), params.fromQuery[String]("state", Some("")), params.fromQuery[String]("country", Some("")), params.fromQuery[String]("profession", Some("")), params.fromQuery[String]("sonmaritalstatus", Some("")), params.fromQuery[String]("daughtermaritalstatus", Some("")), params.fromQuery[String]("brothermaritalstatus", Some("" ))) { (name, gotra, city, state, country, profession, sonmaritalstatus, daughtermaritalstatus, brothermaritalstatus) =>
        controllers_UserControllers_findMembers109_invoker.call(UserControllers_4.findMembers(name, gotra, city, state, country, profession, sonmaritalstatus, daughtermaritalstatus, brothermaritalstatus))
      }
  
    // @LINE:431
    case controllers_HomeController_add110_route(params@_) =>
      call { 
        controllers_HomeController_add110_invoker.call(HomeController_6.add)
      }
  
    // @LINE:434
    case controllers_HomeController_getall111_route(params@_) =>
      call { 
        controllers_HomeController_getall111_invoker.call(HomeController_6.getall)
      }
  
    // @LINE:437
    case controllers_HomeController_update112_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_HomeController_update112_invoker.call(HomeController_6.update(id))
      }
  
    // @LINE:440
    case controllers_HomeController_delete113_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_HomeController_delete113_invoker.call(HomeController_6.delete(id))
      }
  
    // @LINE:443
    case controllers_MediaController_saveImage114_route(params@_) =>
      call { 
        controllers_MediaController_saveImage114_invoker.call(MediaController_0.saveImage)
      }
  
    // @LINE:446
    case controllers_MediaController_what115_route(params@_) =>
      call { 
        controllers_MediaController_what115_invoker.call(MediaController_0.what)
      }
  
    // @LINE:451
    case controllers_AddsController_add116_route(params@_) =>
      call { 
        controllers_AddsController_add116_invoker.call(AddsController_10.add)
      }
  
    // @LINE:454
    case controllers_AddsController_getall117_route(params@_) =>
      call { 
        controllers_AddsController_getall117_invoker.call(AddsController_10.getall)
      }
  
    // @LINE:457
    case controllers_AddsController_getAds118_route(params@_) =>
      call { 
        controllers_AddsController_getAds118_invoker.call(AddsController_10.getAds)
      }
  
    // @LINE:461
    case controllers_AddsController_update119_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AddsController_update119_invoker.call(AddsController_10.update(id))
      }
  
    // @LINE:464
    case controllers_AddsController_delete120_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_AddsController_delete120_invoker.call(AddsController_10.delete(id))
      }
  }
}
