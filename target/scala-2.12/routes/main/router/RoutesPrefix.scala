
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/BssBackendUpdatedCode/BSS Project Backend/bss/conf/routes
// @DATE:Tue Jul 24 17:31:18 IST 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
