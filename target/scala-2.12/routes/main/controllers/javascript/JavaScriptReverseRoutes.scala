
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/BssBackendUpdatedCode/BSS Project Backend/bss/conf/routes
// @DATE:Tue Jul 24 17:31:18 IST 2018

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:16
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:358
  class ReverseMailDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:371
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailDetailsController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletemaildetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:364
    def getmaildetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailDetailsController.getmaildetails",
      """
        function(mailid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getmail-details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("mailid", mailid0))})
        }
      """
    )
  
    // @LINE:368
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailDetailsController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updatemaildetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:358
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailDetailsController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/maildetails"})
        }
      """
    )
  
    // @LINE:361
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailDetailsController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getmaildetails"})
        }
      """
    )
  
  }

  // @LINE:141
  class ReverseFormTitleController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:148
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormTitleController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateformtitle/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:141
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormTitleController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/formtitle"})
        }
      """
    )
  
    // @LINE:151
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormTitleController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteformtitle/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:144
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormTitleController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getformtitle"})
        }
      """
    )
  
  }

  // @LINE:175
  class ReverseGoldSilverController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:181
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GoldSilverController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updategoldSilver/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:175
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GoldSilverController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/goldSilver"})
        }
      """
    )
  
    // @LINE:184
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GoldSilverController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletegoldSilver/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:178
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GoldSilverController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getgoldSilver"})
        }
      """
    )
  
  }

  // @LINE:342
  class ReverseMailController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:348
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updatemail/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:342
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/mail"})
        }
      """
    )
  
    // @LINE:351
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletemail/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:345
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MailController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getmail"})
        }
      """
    )
  
  }

  // @LINE:205
  class ReverseAdmitCardController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:211
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdmitCardController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateadmitCard/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:205
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdmitCardController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/admitCard"})
        }
      """
    )
  
    // @LINE:214
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdmitCardController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteadmitCard/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:208
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdmitCardController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getadmitCard"})
        }
      """
    )
  
  }

  // @LINE:267
  class ReverseAchievmentLocationController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:280
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentLocationController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteachievmentlocation/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:277
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentLocationController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateachievmentlocation/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:267
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentLocationController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/achievmentlocation"})
        }
      """
    )
  
    // @LINE:273
    def getdata: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentLocationController.getdata",
      """
        function(locid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getachievment-location/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("locid", locid0))})
        }
      """
    )
  
    // @LINE:270
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentLocationController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getachievmentlocation"})
        }
      """
    )
  
  }

  // @LINE:308
  class ReverseFacebookController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:314
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updatefacebook/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:308
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/facebook"})
        }
      """
    )
  
    // @LINE:317
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletefacebook/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:311
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getfacebook"})
        }
      """
    )
  
  }

  // @LINE:222
  class ReverseChatController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:241
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChatController.delete",
      """
        function(id0) {
        
          if (true) {
            return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletechat/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
          }
        
        }
      """
    )
  
    // @LINE:238
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChatController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updatechat/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:222
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChatController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/send-message"})
        }
      """
    )
  
    // @LINE:225
    def chatUpload: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChatController.chatUpload",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/send-media"})
        }
      """
    )
  
    // @LINE:235
    def getLatestMessages: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChatController.getLatestMessages",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/list/latest-message" + _qS([(id0 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[Long]].javascriptUnbind + """)("id", id0))])})
        }
      """
    )
  
    // @LINE:232
    def getMessages: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChatController.getMessages",
      """
        function(page0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/list/message" + _qS([(page0 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[Int]].javascriptUnbind + """)("page", page0))])})
        }
      """
    )
  
    // @LINE:229
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ChatController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/list/messages"})
        }
      """
    )
  
  }

  // @LINE:323
  class ReverseFacebookDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:336
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookDetailsController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletefacebookDetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:333
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookDetailsController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updatefacebookDetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:323
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookDetailsController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/facebookDetails"})
        }
      """
    )
  
    // @LINE:329
    def getfacebookdetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookDetailsController.getfacebookdetails",
      """
        function(locid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getfacebook-details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("locid", locid0))})
        }
      """
    )
  
    // @LINE:326
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FacebookDetailsController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getfacebookDetails"})
        }
      """
    )
  
  }

  // @LINE:28
  class ReverseUserControllers(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:53
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserControllers.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteuser/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:50
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserControllers.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateuser/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:31
    def loginAPI: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserControllers.loginAPI",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/user/validate"})
        }
      """
    )
  
    // @LINE:41
    def get: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserControllers.get",
      """
        function(userId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/user/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("userId", userId0))})
        }
      """
    )
  
    // @LINE:35
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserControllers.add",
      """
        function() {
        
          if (true) {
            return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users"})
          }
        
        }
      """
    )
  
    // @LINE:424
    def findMembers: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserControllers.findMembers",
      """
        function(name0,gotra1,city2,state3,country4,profession5,sonmaritalstatus6,daughtermaritalstatus7,brothermaritalstatus8) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/search/members" + _qS([(name0 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("name", name0)), (gotra1 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("gotra", gotra1)), (city2 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("city", city2)), (state3 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("state", state3)), (country4 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("country", country4)), (profession5 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("profession", profession5)), (sonmaritalstatus6 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("sonmaritalstatus", sonmaritalstatus6)), (daughtermaritalstatus7 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("daughtermaritalstatus", daughtermaritalstatus7)), (brothermaritalstatus8 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("brothermaritalstatus", brothermaritalstatus8))])})
        }
      """
    )
  
    // @LINE:28
    def login: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserControllers.login",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/login"})
        }
      """
    )
  
    // @LINE:38
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UserControllers.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getuser"})
        }
      """
    )
  
  }

  // @LINE:377
  class ReverseGalleryController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:383
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updategallery/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:377
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/gallery"})
        }
      """
    )
  
    // @LINE:386
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletegallery/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:380
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getgallery"})
        }
      """
    )
  
  }

  // @LINE:75
  class ReverseOrganizationController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:81
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateorganization/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:75
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/organization"})
        }
      """
    )
  
    // @LINE:84
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteorganization/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:78
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getorganization"})
        }
      """
    )
  
  }

  // @LINE:288
  class ReverseAchievmentDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:302
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentDetailsController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteahievmentdetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:299
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentDetailsController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateahievmentdetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:288
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentDetailsController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/ahievmentdetails"})
        }
      """
    )
  
    // @LINE:294
    def getdata: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentDetailsController.getdata",
      """
        function(achievmentdetailsid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getachievment-details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("achievmentdetailsid", achievmentdetailsid0))})
        }
      """
    )
  
    // @LINE:291
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentDetailsController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getahievmentdetails"})
        }
      """
    )
  
  }

  // @LINE:121
  class ReverseAnnouncementController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:130
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AnnouncementController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateannouncement/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:121
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AnnouncementController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/announcement"})
        }
      """
    )
  
    // @LINE:133
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AnnouncementController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteannouncement/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:124
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AnnouncementController.getall",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getannouncement"})
          }
        
        }
      """
    )
  
  }

  // @LINE:58
  class ReverseMediaController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:446
    def what: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MediaController.what",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "what"})
        }
      """
    )
  
    // @LINE:64
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MediaController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletemedia/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:58
    def upload: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MediaController.upload",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "upload"})
        }
      """
    )
  
    // @LINE:61
    def uploadmedia: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MediaController.uploadmedia",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadmedia"})
        }
      """
    )
  
    // @LINE:443
    def saveImage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MediaController.saveImage",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "save/profileImage"})
        }
      """
    )
  
    // @LINE:67
    def getmedia: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.MediaController.getmedia",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/displaymedia"})
        }
      """
    )
  
  }

  // @LINE:431
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:437
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updatehome/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:431
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/home"})
        }
      """
    )
  
    // @LINE:440
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletehome/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:434
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/gethome"})
        }
      """
    )
  
  }

  // @LINE:156
  class ReverseFormController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:168
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteform/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:165
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateform/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:156
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/form"})
        }
      """
    )
  
    // @LINE:162
    def getform: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormController.getform",
      """
        function(formId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getform-details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("formId", formId0))})
        }
      """
    )
  
    // @LINE:159
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.FormController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getform"})
        }
      """
    )
  
  }

  // @LINE:190
  class ReverseSuggestionController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:196
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuggestionController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updatesuggestion/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:190
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuggestionController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/suggestion"})
        }
      """
    )
  
    // @LINE:199
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuggestionController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletesuggestion/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:193
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SuggestionController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getsuggestion"})
        }
      """
    )
  
  }

  // @LINE:411
  class ReverseGotraController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:417
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GotraController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updategotra/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:411
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GotraController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/gotra"})
        }
      """
    )
  
    // @LINE:420
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GotraController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletegotra/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:414
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GotraController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getgotra"})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseApplication(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:19
    def getFile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getFile",
      """
        function(mediaName0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "media/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("mediaName", mediaName0))})
        }
      """
    )
  
    // @LINE:8
    def options: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.options",
      """
        function(path0) {
        
          if (true) {
            return _wA({method:"OPTIONS", url:"""" + _prefix + """" + _qS([(path0 == null ? null : (""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("path", path0))])})
          }
        
        }
      """
    )
  
    // @LINE:20
    def getFileThumb: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.getFileThumb",
      """
        function(mediaName0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "media/Thumbnail/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("mediaName", mediaName0))})
        }
      """
    )
  
    // @LINE:13
    def logOut: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.logOut",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
        }
      """
    )
  
    // @LINE:11
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "home"})
        }
      """
    )
  
    // @LINE:6
    def login: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Application.login",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:113
  class ReverseRegistrationController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:113
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.RegistrationController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getregistration"})
        }
      """
    )
  
  }

  // @LINE:451
  class ReverseAddsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:464
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AddsController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteadd/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:457
    def getAds: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AddsController.getAds",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "active/ads"})
        }
      """
    )
  
    // @LINE:461
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AddsController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateadd/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:451
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AddsController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/addadd"})
        }
      """
    )
  
    // @LINE:454
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AddsController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/displayadd"})
        }
      """
    )
  
  }

  // @LINE:392
  class ReverseGalleryDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:404
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryDetailsController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deletegallerydetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:401
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryDetailsController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updategallerydetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:398
    def getgallerydetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryDetailsController.getgallerydetails",
      """
        function(galleryId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/gallery-details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("galleryId", galleryId0))})
        }
      """
    )
  
    // @LINE:392
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryDetailsController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/gallerydetails"})
        }
      """
    )
  
    // @LINE:395
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GalleryDetailsController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getgallerydetails"})
        }
      """
    )
  
  }

  // @LINE:251
  class ReverseAchievmentController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:257
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateachievment/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:251
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/achievment"})
        }
      """
    )
  
    // @LINE:260
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteachievment/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:254
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AchievmentController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getachievment"})
        }
      """
    )
  
  }

  // @LINE:91
  class ReverseOrganizationDetailsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:103
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationDetailsController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/deleteorganizationdetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:100
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationDetailsController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/updateorganizationdetails/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:91
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationDetailsController.add",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/organizationdetails"})
        }
      """
    )
  
    // @LINE:97
    def getorganizationDetails: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationDetailsController.getorganizationDetails",
      """
        function(orgId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/organization-details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("orgId", orgId0))})
        }
      """
    )
  
    // @LINE:94
    def getall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrganizationDetailsController.getall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getorganizationdetails"})
        }
      """
    )
  
  }


}
