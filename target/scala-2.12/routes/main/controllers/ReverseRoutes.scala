
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/BssBackendUpdatedCode/BSS Project Backend/bss/conf/routes
// @DATE:Tue Jul 24 17:31:18 IST 2018

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:16
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:358
  class ReverseMailDetailsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:371
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletemaildetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:364
    def getmaildetails(mailid:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getmail-details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("mailid", mailid)))
    }
  
    // @LINE:368
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updatemaildetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:358
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/maildetails")
    }
  
    // @LINE:361
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getmaildetails")
    }
  
  }

  // @LINE:141
  class ReverseFormTitleController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:148
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateformtitle/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:141
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/formtitle")
    }
  
    // @LINE:151
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteformtitle/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:144
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getformtitle")
    }
  
  }

  // @LINE:175
  class ReverseGoldSilverController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:181
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updategoldSilver/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:175
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/goldSilver")
    }
  
    // @LINE:184
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletegoldSilver/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:178
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getgoldSilver")
    }
  
  }

  // @LINE:342
  class ReverseMailController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:348
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updatemail/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:342
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/mail")
    }
  
    // @LINE:351
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletemail/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:345
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getmail")
    }
  
  }

  // @LINE:205
  class ReverseAdmitCardController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:211
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateadmitCard/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:205
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/admitCard")
    }
  
    // @LINE:214
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteadmitCard/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:208
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getadmitCard")
    }
  
  }

  // @LINE:267
  class ReverseAchievmentLocationController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:280
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteachievmentlocation/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:277
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateachievmentlocation/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:267
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/achievmentlocation")
    }
  
    // @LINE:273
    def getdata(locid:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getachievment-location/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("locid", locid)))
    }
  
    // @LINE:270
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getachievmentlocation")
    }
  
  }

  // @LINE:308
  class ReverseFacebookController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:314
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updatefacebook/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:308
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/facebook")
    }
  
    // @LINE:317
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletefacebook/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:311
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getfacebook")
    }
  
  }

  // @LINE:222
  class ReverseChatController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:241
    def delete(id:Long): Call = {
    
      (id: @unchecked) match {
      
        // @LINE:241
        case (id)  =>
          
          Call("DELETE", _prefix + { _defaultPrefix } + "api/deletechat/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
      
      }
    
    }
  
    // @LINE:238
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updatechat/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:222
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/send-message")
    }
  
    // @LINE:225
    def chatUpload(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/send-media")
    }
  
    // @LINE:235
    def getLatestMessages(id:Long = 0): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/list/latest-message" + play.core.routing.queryString(List(if(id == 0) None else Some(implicitly[play.api.mvc.QueryStringBindable[Long]].unbind("id", id)))))
    }
  
    // @LINE:232
    def getMessages(page:Int = 0): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/list/message" + play.core.routing.queryString(List(if(page == 0) None else Some(implicitly[play.api.mvc.QueryStringBindable[Int]].unbind("page", page)))))
    }
  
    // @LINE:229
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/list/messages")
    }
  
  }

  // @LINE:323
  class ReverseFacebookDetailsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:336
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletefacebookDetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:333
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updatefacebookDetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:323
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/facebookDetails")
    }
  
    // @LINE:329
    def getfacebookdetails(locid:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getfacebook-details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("locid", locid)))
    }
  
    // @LINE:326
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getfacebookDetails")
    }
  
  }

  // @LINE:28
  class ReverseUserControllers(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:53
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteuser/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:50
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateuser/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:31
    def loginAPI(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/user/validate")
    }
  
    // @LINE:41
    def get(userId:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/user/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("userId", userId)))
    }
  
    // @LINE:35
    def add(): Call = {
    
      () match {
      
        // @LINE:35
        case ()  =>
          
          Call("POST", _prefix + { _defaultPrefix } + "api/users")
      
      }
    
    }
  
    // @LINE:424
    def findMembers(name:String = "", gotra:String = "", city:String = "", state:String = "", country:String = "", profession:String = "", sonmaritalstatus:String = "", daughtermaritalstatus:String = "", brothermaritalstatus:String = "" ): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/search/members" + play.core.routing.queryString(List(if(name == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("name", name)), if(gotra == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("gotra", gotra)), if(city == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("city", city)), if(state == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("state", state)), if(country == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("country", country)), if(profession == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("profession", profession)), if(sonmaritalstatus == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("sonmaritalstatus", sonmaritalstatus)), if(daughtermaritalstatus == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("daughtermaritalstatus", daughtermaritalstatus)), if(brothermaritalstatus == "" ) None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("brothermaritalstatus", brothermaritalstatus)))))
    }
  
    // @LINE:28
    def login(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/login")
    }
  
    // @LINE:38
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getuser")
    }
  
  }

  // @LINE:377
  class ReverseGalleryController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:383
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updategallery/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:377
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/gallery")
    }
  
    // @LINE:386
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletegallery/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:380
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getgallery")
    }
  
  }

  // @LINE:75
  class ReverseOrganizationController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:81
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateorganization/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:75
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/organization")
    }
  
    // @LINE:84
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteorganization/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:78
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getorganization")
    }
  
  }

  // @LINE:288
  class ReverseAchievmentDetailsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:302
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteahievmentdetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:299
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateahievmentdetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:288
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/ahievmentdetails")
    }
  
    // @LINE:294
    def getdata(achievmentdetailsid:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getachievment-details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("achievmentdetailsid", achievmentdetailsid)))
    }
  
    // @LINE:291
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getahievmentdetails")
    }
  
  }

  // @LINE:121
  class ReverseAnnouncementController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:130
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateannouncement/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:121
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/announcement")
    }
  
    // @LINE:133
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteannouncement/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:124
    def getall(): Call = {
    
      () match {
      
        // @LINE:124
        case ()  =>
          
          Call("GET", _prefix + { _defaultPrefix } + "api/getannouncement")
      
      }
    
    }
  
  }

  // @LINE:58
  class ReverseMediaController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:446
    def what(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "what")
    }
  
    // @LINE:64
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletemedia/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:58
    def upload(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "upload")
    }
  
    // @LINE:61
    def uploadmedia(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "uploadmedia")
    }
  
    // @LINE:443
    def saveImage(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "save/profileImage")
    }
  
    // @LINE:67
    def getmedia(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/displaymedia")
    }
  
  }

  // @LINE:431
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:437
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updatehome/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:431
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/home")
    }
  
    // @LINE:440
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletehome/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:434
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/gethome")
    }
  
  }

  // @LINE:156
  class ReverseFormController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:168
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteform/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:165
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateform/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:156
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/form")
    }
  
    // @LINE:162
    def getform(formId:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getform-details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("formId", formId)))
    }
  
    // @LINE:159
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getform")
    }
  
  }

  // @LINE:190
  class ReverseSuggestionController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:196
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updatesuggestion/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:190
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/suggestion")
    }
  
    // @LINE:199
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletesuggestion/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:193
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getsuggestion")
    }
  
  }

  // @LINE:411
  class ReverseGotraController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:417
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updategotra/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:411
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/gotra")
    }
  
    // @LINE:420
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletegotra/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:414
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getgotra")
    }
  
  }

  // @LINE:6
  class ReverseApplication(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:19
    def getFile(mediaName:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "media/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("mediaName", mediaName)))
    }
  
    // @LINE:8
    def options(path:String): Call = {
    
      (path: @unchecked) match {
      
        // @LINE:8
        case (path)  =>
          
          Call("OPTIONS", _prefix + play.core.routing.queryString(List(if(path == "") None else Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("path", path)))))
      
      }
    
    }
  
    // @LINE:20
    def getFileThumb(mediaName:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "media/Thumbnail/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("mediaName", mediaName)))
    }
  
    // @LINE:13
    def logOut(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "logout")
    }
  
    // @LINE:11
    def index(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "home")
    }
  
    // @LINE:6
    def login(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:113
  class ReverseRegistrationController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:113
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getregistration")
    }
  
  }

  // @LINE:451
  class ReverseAddsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:464
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteadd/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:457
    def getAds(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "active/ads")
    }
  
    // @LINE:461
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateadd/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:451
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/addadd")
    }
  
    // @LINE:454
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/displayadd")
    }
  
  }

  // @LINE:392
  class ReverseGalleryDetailsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:404
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deletegallerydetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:401
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updategallerydetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:398
    def getgallerydetails(galleryId:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/gallery-details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("galleryId", galleryId)))
    }
  
    // @LINE:392
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/gallerydetails")
    }
  
    // @LINE:395
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getgallerydetails")
    }
  
  }

  // @LINE:251
  class ReverseAchievmentController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:257
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateachievment/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:251
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/achievment")
    }
  
    // @LINE:260
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteachievment/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:254
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getachievment")
    }
  
  }

  // @LINE:91
  class ReverseOrganizationDetailsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:103
    def delete(id:Long): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/deleteorganizationdetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:100
    def update(id:Long): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/updateorganizationdetails/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:91
    def add(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/organizationdetails")
    }
  
    // @LINE:97
    def getorganizationDetails(orgId:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/organization-details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("orgId", orgId)))
    }
  
    // @LINE:94
    def getall(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getorganizationdetails")
    }
  
  }


}
