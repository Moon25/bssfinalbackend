
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/BssBackendUpdatedCode/BSS Project Backend/bss/conf/routes
// @DATE:Tue Jul 24 17:31:18 IST 2018

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseMailDetailsController MailDetailsController = new controllers.ReverseMailDetailsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseFormTitleController FormTitleController = new controllers.ReverseFormTitleController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGoldSilverController GoldSilverController = new controllers.ReverseGoldSilverController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseMailController MailController = new controllers.ReverseMailController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAdmitCardController AdmitCardController = new controllers.ReverseAdmitCardController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAchievmentLocationController AchievmentLocationController = new controllers.ReverseAchievmentLocationController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseFacebookController FacebookController = new controllers.ReverseFacebookController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseChatController ChatController = new controllers.ReverseChatController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseFacebookDetailsController FacebookDetailsController = new controllers.ReverseFacebookDetailsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseUserControllers UserControllers = new controllers.ReverseUserControllers(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGalleryController GalleryController = new controllers.ReverseGalleryController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseOrganizationController OrganizationController = new controllers.ReverseOrganizationController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAchievmentDetailsController AchievmentDetailsController = new controllers.ReverseAchievmentDetailsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAnnouncementController AnnouncementController = new controllers.ReverseAnnouncementController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseMediaController MediaController = new controllers.ReverseMediaController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseFormController FormController = new controllers.ReverseFormController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSuggestionController SuggestionController = new controllers.ReverseSuggestionController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGotraController GotraController = new controllers.ReverseGotraController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseApplication Application = new controllers.ReverseApplication(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseRegistrationController RegistrationController = new controllers.ReverseRegistrationController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAddsController AddsController = new controllers.ReverseAddsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGalleryDetailsController GalleryDetailsController = new controllers.ReverseGalleryDetailsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAchievmentController AchievmentController = new controllers.ReverseAchievmentController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseOrganizationDetailsController OrganizationDetailsController = new controllers.ReverseOrganizationDetailsController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseMailDetailsController MailDetailsController = new controllers.javascript.ReverseMailDetailsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseFormTitleController FormTitleController = new controllers.javascript.ReverseFormTitleController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGoldSilverController GoldSilverController = new controllers.javascript.ReverseGoldSilverController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseMailController MailController = new controllers.javascript.ReverseMailController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAdmitCardController AdmitCardController = new controllers.javascript.ReverseAdmitCardController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAchievmentLocationController AchievmentLocationController = new controllers.javascript.ReverseAchievmentLocationController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseFacebookController FacebookController = new controllers.javascript.ReverseFacebookController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseChatController ChatController = new controllers.javascript.ReverseChatController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseFacebookDetailsController FacebookDetailsController = new controllers.javascript.ReverseFacebookDetailsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseUserControllers UserControllers = new controllers.javascript.ReverseUserControllers(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGalleryController GalleryController = new controllers.javascript.ReverseGalleryController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseOrganizationController OrganizationController = new controllers.javascript.ReverseOrganizationController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAchievmentDetailsController AchievmentDetailsController = new controllers.javascript.ReverseAchievmentDetailsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAnnouncementController AnnouncementController = new controllers.javascript.ReverseAnnouncementController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseMediaController MediaController = new controllers.javascript.ReverseMediaController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseFormController FormController = new controllers.javascript.ReverseFormController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSuggestionController SuggestionController = new controllers.javascript.ReverseSuggestionController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGotraController GotraController = new controllers.javascript.ReverseGotraController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseRegistrationController RegistrationController = new controllers.javascript.ReverseRegistrationController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAddsController AddsController = new controllers.javascript.ReverseAddsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGalleryDetailsController GalleryDetailsController = new controllers.javascript.ReverseGalleryDetailsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAchievmentController AchievmentController = new controllers.javascript.ReverseAchievmentController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseOrganizationDetailsController OrganizationDetailsController = new controllers.javascript.ReverseOrganizationDetailsController(RoutesPrefix.byNamePrefix());
  }

}
