# This is the main configuration file for the application.
# https://www.playframework.com/documentation/latest/ConfigFile
# ~~~~~
# Play uses HOCON as its configuration file format.  HOCON has a number
# of advantages over other config formats, but there are two things that
# can be used when modifying settings.
#
# You can include other configuration files in this main application.conf file:
#include "extra-config.conf"
#
# You can declare variables and substitute for them:
#mykey = ${some.value}
#
# And if an environment variable exists when there is no other subsitution, then
# HOCON will fall back to substituting environment variable:
#mykey = ${JAVA_HOME}



## Akka
# https://www.playframework.com/documentation/latest/ScalaAkka#Configuration
# https://www.playframework.com/documentation/latest/JavaAkka#Configuration
# ~~~~~
# Play uses Akka internally and exposes Akka Streams and actors in Websockets and
# other streaming HTTP responses.
akka {
  # "akka.log-config-on-start" is extraordinarly useful because it log the complete
  # configuration at INFO level, including defaults and overrides, so it s worth
  # putting at the very top.
  #
  # Put the following in your conf/logback.xml file:
  #
  # <logger name="akka.actor" level="INFO" />
  #
  # And then uncomment this line to debug the configuration.
  #
  #log-config-on-start = true
}

## Secret key
# http://www.playframework.com/documentation/latest/ApplicationSecret
# ~~~~~
# The secret key is used to sign Play's session cookie.
# This must be changed for production, but we don't recommend you change it in this file.
play.http.secret.key = "@uo<afx=HFwwUpAXfWa<hm/kTR34i<AO2ZXd=wL^b[IV/GWHE3FGrJQ@xd10UmXa"

## Modules
# https://www.playframework.com/documentation/latest/Modules
# ~~~~~
# Control which modules are loaded when Play starts.  Note that modules are
# the replacement for "GlobalSettings", which are deprecated in 2.5.x.
# Please see https://www.playframework.com/documentation/latest/GlobalSettings
# for more information.
#
# You can also extend Play functionality by using one of the publically available
# Play modules: https://playframework.com/documentation/latest/ModuleDirectory
play.modules {
  # By default, Play will load any class called Module that is defined
  # in the root package (the "app" directory), or you can define them
  # explicitly below.
  # If there are any built-in modules that you want to disable, you can list them here.
  #enabled += my.application.Module

  # If there are any built-in modules that you want to disable, you can list them here.
  #disabled += ""
}
ebean.default="models.*"
## IDE
# https://www.playframework.com/documentation/latest/IDE
# ~~~~~
# Depending on your IDE, you can add a hyperlink for errors that will jump you
# directly to the code location in the IDE in dev mode. The following line makes
# use of the IntelliJ IDEA REST interface:
#play.editor="http://localhost:63342/api/file/?file=%s&line=%s"

## Internationalisation
# https://www.playframework.com/documentation/latest/JavaI18N
# https://www.playframework.com/documentation/latest/ScalaI18N
# ~~~~~
# Play comes with its own i18n settings, which allow the user's preferred language
# to map through to internal messages, or allow the language to be stored in a cookie.
play.i18n {
  # The application languages
  langs = [ "en" ]

  # Whether the language cookie should be secure or not
  #langCookieSecure = true

  # Whether the HTTP only attribute of the cookie should be set to true
  #langCookieHttpOnly = true
}

play.filters.headers.contentSecurityPolicy = null

#play.filters {

  # Enabled filters are run automatically against Play.
  # CSRFFilter, AllowedHostFilters, and SecurityHeadersFilters are enabled by default.
  #enabled += filters.ExampleFilter

  # Disabled filters remove elements from the enabled list.
  #disabled += filters.ExampleFilter

  ## CORS filter configuration
  # https://www.playframework.com/documentation/latest/CorsFilter
  # ~~~~~
  # CORS is a protocol that allows web applications to make requests from the browser
  # across different domains.
  # NOTE: You MUST apply the CORS configuration before the CSRF filter, as CSRF has
  # dependencies on CORS settings.
 # cors {
    # Filter paths by a whitelist of path prefixes
    #pathPrefixes = ["/some/path", ...]

    # The allowed origins. If null, all origins are allowed.
    //allowedOrigins = ["http://localhost:9001"]

    # The allowed HTTP methods. If null, all methods are allowed
    #allowedHttpMethods = ["GET", "POST","PUT","DELET"]
  #}

  ## CSRF Filter
  # https://www.playframework.com/documentation/latest/ScalaCsrf#Applying-a-global-CSRF-filter
  # https://www.playframework.com/documentation/latest/JavaCsrf#Applying-a-global-CSRF-filter
  # ~~~~~
  # Play supports multiple methods for verifying that a request is not a CSRF request.
  # The primary mechanism is a CSRF token. This token gets placed either in the query string
  # or body of every form submitted, and also gets placed in the users session.
  # Play then verifies that both tokens are present and match.
 # csrf {
    # Sets the cookie to be sent only over HTTPS
    #cookie.secure = true

    # Defaults to CSRFErrorHandler in the root package.
    #errorHandler = MyCSRFErrorHandler
  #}

  ## Security headers filter configuration
  # https://www.playframework.com/documentation/latest/SecurityHeaders
  # ~~~~~
  # Defines security headers that prevent XSS attacks.
  # If enabled, then all options are set to the below configuration by default:
  #headers {
    # The X-Frame-Options header. If null, the header is not set.
    #frameOptions = "DENY"

    # The X-XSS-Protection header. If null, the header is not set.
    #xssProtection = "1; mode=block"

    # The X-Content-Type-Options header. If null, the header is not set.
    #contentTypeOptions = "nosniff"

    # The X-Permitted-Cross-Domain-Policies header. If null, the header is not set.
    #permittedCrossDomainPolicies = "master-only"

    # The Content-Security-Policy header. If null, the header is not set.
    #contentSecurityPolicy = "default-src 'self'"
 # }

  ## Allowed hosts filter configuration
  # https://www.playframework.com/documentation/latest/AllowedHostsFilter
  # ~~~~~
  # Play provides a filter that lets you configure which hosts can access your application.
  # This is useful to prevent cache poisoning attacks.
 # hosts {
    # Allow requests to example.com, its subdomains, and localhost:9000.
    #allowed = [".example.com", "localhost:9000"]
  #}
#}

## Evolutions
# https://www.playframework.com/documentation/latest/Evolutions
# ~~~~~
# Evolutions allows database scripts to be automatically run on startup in dev mode
# for database migrations. You must enable this by adding to build.sbt:
#
#libraryDependencies += evolutions
#
play.evolutions {
  # You can disable evolutions for a specific datasource if necessary
  db.default.enabled = false
}

## Database Connection Pool
# https://www.playframework.com/documentation/latest/SettingsJDBC
# ~~~~~
# Play doesn't require a JDBC database to run, but you can easily enable one.
#
# libraryDependencies += jdbc
#
fixedConnectionPool = 9
play.db {
  # The combination of these two settings results in "db.default" as the
  # default JDBC pool:
  #config = "db"
  #default = "default"

  # Play uses HikariCP as the default connection pool.  You can override
  # settings by changing the prototype:
  prototype {
    # Sets a fixed JDBC connection pool sizes of 50
    hikaricp.minimumIdle = ${fixedConnectionPool}
    hikaricp.maximumPoolSize = ${fixedConnectionPool}
  }
}

## JDBC Datasource
# https://www.playframework.com/documentation/latest/JavaDatabase
# https://www.playframework.com/documentation/latest/ScalaDatabase
# ~~~~~
# Once JDBC datasource is set up, you can work with several different
# database options:
#
# Slick (Scala preferred option): https://www.playframework.com/documentation/latest/PlaySlick
# JPA (Java preferred option): https://playframework.com/documentation/latest/JavaJPA
# EBean: https://playframework.com/documentation/latest/JavaEbean
# Anorm: https://www.playframework.com/documentation/latest/ScalaAnorm
#
db {
  # You can declare as many datasources as you want.
  # By convention, the default datasource is named `default`

  # https://www.playframework.com/documentation/latest/Developing-with-the-H2-Database
  #default.driver = org.h2.Driver
  #default.url = "jdbc:h2:mem:play"
  #default.username = sa
  #default.password = ""


 # default.driver=com.mysql.jdbc.Driver
  #default.url="jdbc:mysql://root:root@localhost/meetingroom"

  default.driver=com.mysql.jdbc.Driver
   default.url="jdbc:mysql://localhost:3306/bssdb?useUnicode=yes&characterEncoding=UTF-8"
  default.username=root
  default.password=root


  # You can turn on SQL logging for any datasource
  # https://www.playframework.com/documentation/latest/Highlights25#Logging-SQL-statements
  #default.logSql=true


}
database.dispatcher {
  executor = "thread-pool-executor"
  throughput = 1
  thread-pool-executor {
    fixed-pool-sizes = ${fixedConnectionPool}
  }
}
play.filters.csrf.header.protectHeaders = null

play.filters.csrf.header.bypassHeaders {
  X-Requested-With = "*"
  Csrf-Token = "nocheck"
}
play.filters.csrf.bypassCorsTrustedOrigins = false

play.http.actionCreator = "security.SecurityCheck"

play.filters.enabled += play.filters.hosts.AllowedHostsFilter

play.filters.hosts {
  # Allow requests to example.com, its subdomains, and localhost:9000.
  allowed = ["localhost:9000","192.168.1.47:9000","192.168.1.170:9000","localhost:8000","45.55.239.12:9000","139.59.34.210:9000"]
}


play.filters.enabled += "play.filters.headers.SecurityHeadersFilter"
#allowedOrigins = ["http://www.example.com", ...]

play.filters.headers {
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" img-src 'self' *.fbcdn.net *.twimg.com *.googleusercontent.com *.xingassets.com vk.com *.yimg.com secure.gravatar.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" style-src 'self' 'unsafe-inline' cdnjs.cloudflare.com maxcdn.bootstrapcdn.com cdn.jsdelivr.net fonts.googleapis.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" font-src 'self' fonts.gstatic.com fonts.googleapis.com cdnjs.cloudflare.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" script-src 'self' cdnjs.cloudflare.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" connect-src 'self' twitter.com *.xing.com;"

}



play.filters.enabled += "play.filters.cors.CORSFilter"

play.filters.cors {
  pathPrefixes = ["/some/path", ...]
  allowedOrigins = ["http://localhost:9000","http://localhost:8000","http://plustvinc.com","http://45.55.239.12:9000","http://45.55.239.12:9001"]
  allowedHttpMethods = ["GET", "POST","DELETE","PUT","OPTIONS"]
  allowedHttpHeaders = ["Accept","Origin","Content-Type","X-Auth-Token"]
  preflightMaxAge = 3 days
}

play.http.parser.maxDiskBuffer=100MB

parsers.anyContent.maxLength=100MB
