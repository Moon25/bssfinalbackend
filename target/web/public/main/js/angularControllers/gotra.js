'use strict';


 app.controller('gotra', ['$rootScope', '$scope', '$location', 'webservice', 'FileUploader', '$window', function($rootScope, $scope, $location, webservice, FileUploader, $window)
    {
      $scope.newItem={};
      $scope.hide=true;
      $scope.show=true;

           getall();

            $rootScope.main =function(){
               getall();
            }


            $scope.getTemplate = function (contact) {
                                     if (contact.id=== $scope.newItem.id)
                                     return 'edit';
                                     else
                                     return 'display';
                            };

            $scope.editContact = function (contact) {
                                      $scope.newItem = angular.copy(contact);
                                      $scope.inputItem = false;
                                      $scope.addItemLabel = "Add";
                                      $scope.buttonIcon = "glyphicon glyphicon-plus";
                            };

            $scope.reset = function () {
                     $scope.newItem = {};
            };

            $scope.hideuploder = function(url){

            if(url != null)
            {
            $scope.hide =false;
            }
            else
            {
            $scope.hide=true;
            }
            }

            $scope.hideInputbox =function(){
               $scope.show=false;
               if( $scope.newItem.src != null)
               {console.log("true");
               $scope.show =false;}
               else{
               console.log("f");
               $scope.show =true;
               }
            }

            $scope.save = function(newItem,form) {

                        webservice.addgotra.add(newItem).$promise.then(function(reply) {
                                $scope.showIcon = false;
                                $scope.newItem = {};
                                getall();
                                $scope.hide=true;
                                $scope.show=true;
                                form.$setPristine();
                                form.$setUntouched();
                                $scope.inputItem=false;
                            },
                            function(err, headers) {
                                console.log("reply", err);
                            });
            }

            $scope.addItem = function() {
                        $scope.inputItem = !$scope.inputItem;
                        $scope.hide=true;
                        $scope.show=true;
                        if ($scope.addItemLabel == "Add") {
                        $scope.addItemLabel = "Hide";
                        $scope.buttonIcon = "glyphicon glyphicon-minus";
                        } else {
                        $scope.addItemLabel = "Add";
                        $scope.buttonIcon = "glyphicon glyphicon-plus";
                        }
                       }

           /* $scope.disableSaveButton = function(){
               if ($scope.newItem) {
                 if (($scope.newItem.name && $scope.newItem.name.length > 0) && ($scope.newItem.thumbnail && $scope.newItem.thumbnail.length > 0))

                     return false;
                     }
                     return true;
            }*/

            function getall(){
                      webservice.displaygotra.query().$promise.then(function(reply) {
                                              $scope.items =reply;
                                           },
                                           function(err, headers) {
                                               console.log("getall", err);
                                           });

                      $scope.addItemLabel = "Add";
                      $scope.buttonIcon = "glyphicon glyphicon-plus";

            }

            $scope.edit =function(newItem){
                              /* if(newItem.url != null){
                               newItem.src =newItem.url;
                               }*/
                               webservice.updategotra.put(newItem).$promise.then(function(reply) {
                                      $scope.newItem = {};
                                       console.log("reply" , reply);
                                      getall();
                                    },
                                function(err, headers) {
                                      console.log("err",err);
                                });
            }

            $scope.deleteitem=function(id){
                      if($window.confirm("Are you sure, you want to delete ?")) {
                         webservice.deletegotra.delete({"id": id}).$promise.then(function(reply) {
                                         $scope.reset();
                                         getall();
                           },
                           function(err, headers) {
                                console.log("err",err);
                                });
                         }
            }

            var uploader = $scope.uploader = new FileUploader({
                    url: '/uploadmedia',
                    autoUpload: true,
                    });

               uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
                    console.info('onWhenAddingFileFailed', item, filter, options);
                    };
                    uploader.onAfterAddingFile = function(fileItem) {
                    console.info('onAfterAddingFile', fileItem);
                    };
                    uploader.onAfterAddingAll = function(addedFileItems) {
                    console.info('onAfterAddingAll', addedFileItems);
                    $scope.uploadInProgress = true;
                    };
                    uploader.onBeforeUploadItem = function(item) {
                    console.log("tagname" + $scope.tagname);
                    item.formData.push({
                    tagname: $scope.tagname
                    });
                    console.info('onBeforeUploadItem exit', item);
                    };
                    uploader.onProgressItem = function(fileItem, progress) {
                    console.info('onProgressItem', fileItem, progress);
                    };
                    uploader.onProgressAll = function(progress) {
                    $scope.dynamic = progress;
                    $scope.show =false;
                    console.info('onProgressAll', progress);
                    };
                    uploader.onSuccessItem = function(fileItem, response, status, headers) {

                    console.info('onSuccessItem', fileItem, response, status, headers);
                    };
                    uploader.onErrorItem = function(fileItem, response, status, headers) {
                    console.info('onErrorItem', fileItem, response, status, headers);
                    };
                    uploader.onCancelItem = function(fileItem, response, status, headers) {
                    console.info('onCancelItem', fileItem, response, status, headers);
                    };
                    uploader.onCompleteItem = function(fileItem, response, status, headers) {
                    console.info('onCompleteItem', fileItem, response, status, headers);

                    if (status == 200) {
                    $scope.newItem.thumbnail = {};
                    $scope.newItem.thumbnail =response;

                    console.log("Updated Item is ", $scope.newItem)
                    }
                    };
                    uploader.onCompleteAll = function() {
                    console.info('onCompleteAll');
                    $scope.uploadInProgress = false;
                    };
                    console.info('uploader', uploader);
                    uploader.filters.push({
                    name: 'imageFilter',
                    fn: function(item /*{File|FileLikeObject}*/ , options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|gif|pdf|mp4|avi'.indexOf(type) !== -1;
                    }
                    });
                    $scope.fileNameChanged = function() {
                    console.log("fileNameChanged : select file");
                    //$scope.uploader.uploadAll();
                    console.info('uploader', uploader);
                    console.log("called function");
                    }

            $scope.onFileSelect = function($files) {
                //$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {
                  var $file = $files[i];
                  $upload.upload({
                    url: 'my/upload/url',
                    file: $file,
                    progress: function(e){}
                  }).then(function(data, status, headers, config) {
                    // file is uploaded successfully
                    console.log(data);
                  });
                }
              }
    }]);