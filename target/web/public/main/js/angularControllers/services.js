(function() {

    'use strict';

    var webservice = ['$resource', function($resource) {

        return {




                  addorganization: $resource('/api/organization/:id', {
                                                   headers: {
                                                         'Content-Type': 'application/json'
                                                        }
                                                   }, {
                                                   add: {
                                                        method: 'POST',
                                                             params: {}
                                                         }
                                                  }),
                   displayorganization: $resource('/api/getorganization/:id', {
                                                    headers: {
                                                          'Content-Type': 'application/json'
                                                         }
                                                    }, {
                                                    query: {
                                                        method: 'GET',
                                                        params: {},
                                                        isArray: true
                                                       }
                                                    }),
                   updateorganization: $resource('/api/updateorganization/:id', {
                                                      headers: {
                                                           'Content-Type': 'application/json'
                                                         }
                                                      }, {
                                                      put: {
                                                        method: 'PUT',
                                                        params: {
                                                             id: '@id'
                                                          }
                                                        }
                                                      }),
                   deleteorganization: $resource('/api/deleteorganization/:id', {}, {
                                                       delete: {
                                                            method: 'DELETE',
                                                            params: {}
                                                          },
                                                       }),

           //Adds Page
            addadd: $resource('/api/addadd/:id', {
                                                                     headers: {
                                                                           'Content-Type': 'application/json'
                                                                          }
                                                                     }, {
                                                                     add: {
                                                                          method: 'POST',
                                                                               params: {}
                                                                           }
                                                                    }),
            displayadd: $resource('/api/displayadd/:id', {
                                                                      headers: {
                                                                            'Content-Type': 'application/json'
                                                                           }
                                                                      }, {
                                                                      query: {
                                                                          method: 'GET',
                                                                          params: {},
                                                                          isArray: true
                                                                         }
                                                                      }),
            updateadd: $resource('/api/updateadd/:id', {
                                                                        headers: {
                                                                             'Content-Type': 'application/json'
                                                                           }
                                                                        }, {
                                                                        put: {
                                                                          method: 'PUT',
                                                                          params: {
                                                                               id: '@id'
                                                                            }
                                                                          }
                                                                        }),
            deleteadd: $resource('/api/deleteadd/:id', {}, {
                                                                         delete: {
                                                                              method: 'DELETE',
                                                                              params: {}
                                                                            },
                                                                         }),
            ads: $resource('/active/ads/:id', {
                                                                                  headers: {
                                                                                        'Content-Type': 'application/json'
                                                                                       }
                                                                                  }, {
                                                                                  query: {
                                                                                      method: 'GET',
                                                                                      params: {},
                                                                                      isArray: true
                                                                                     }
                                                                                  }),

       //Organization Details Page start here


         addorganizationdetails: $resource('/api/organizationdetails/:id', {
                                                               headers: {
                                                                     'Content-Type': 'application/json'
                                                                    }
                                                               }, {
                                                               add: {
                                                                    method: 'POST',
                                                                         params: {}
                                                                     }
                                                              }),
         displayorganizationdetails: $resource('/api/getorganizationdetails/:id', {
                                                                headers: {
                                                                      'Content-Type': 'application/json'
                                                                     }
                                                                }, {
                                                                query: {
                                                                    method: 'GET',
                                                                    params: {},
                                                                    isArray: true
                                                                   }
                                                                }),
         updateorganizationdetails: $resource('/api/updateorganizationdetails/:id', {
                                                                  headers: {
                                                                       'Content-Type': 'application/json'
                                                                     }
                                                                  }, {
                                                                  put: {
                                                                    method: 'PUT',
                                                                    params: {
                                                                         id: '@id'
                                                                      }
                                                                    }
                                                                  }),
         deleteorganizationdetails: $resource('/api/deleteorganizationdetails/:id', {}, {
                                                                   delete: {
                                                                        method: 'DELETE',
                                                                        params: {}
                                                                      },
                                                                   }),


         //Organization Details page end here




                deletemedia: $resource('/api/deletemedia/:id', {}, {
                                                               delete: {
                                                               method: 'DELETE',
                                                               params: {}
                                                               },
                                                             }),

                displaymedia: $resource('/api/displaymedia/:id', {
                                                                headers: {
                                                                'Content-Type': 'application/json'
                                                                }
                                                               }, {
                                                                 query: {
                                                                  method: 'GET',
                                                                  params: {},
                                                                  isArray: true
                                                                  }
                                                             }),









               addregistration: $resource('/api/registration/:id', {
                                                              headers: {
                                                                    'Content-Type': 'application/json'
                                                                   }
                                                              }, {
                                                              add: {
                                                                   method: 'POST',
                                                                        params: {}
                                                                    }
                                                             }),


               displayregistration: $resource('/api/getregistration/:id', {
                                                               headers: {
                                                                     'Content-Type': 'application/json'
                                                                    }
                                                               }, {
                                                               query: {
                                                                   method: 'GET',
                                                                   params: {},
                                                                   isArray: true
                                                                  }
                                                               }),


             addannouncement: $resource('/api/announcement/:id', {
                                                             headers: {
                                                                   'Content-Type': 'application/json'
                                                                  }
                                                             }, {
                                                             add: {
                                                                  method: 'POST',
                                                                       params: {}
                                                                   }
                                                            }),

             displayannouncement: $resource('/api/getannouncement/:id', {
                                                                  headers: {
                                                                     'Content-Type': 'application/json'
                                                                         }
                                                                     }, {
                                                                      query: {
                                                                       method: 'GET',
                                                                       params: {},
                                                                       isArray: true
                                                                         }
                                                                   }),

             updateannouncement: $resource('/api/updateannouncement/:id', {
                                                              headers: {
                                                                   'Content-Type': 'application/json'
                                                                 }
                                                              }, {
                                                              put: {
                                                                method: 'PUT',
                                                                params: {
                                                                     id: '@id'
                                                                  }
                                                                }
                                                              }),

             deleteannouncement: $resource('/api/deleteannouncement/:id', {}, {
                                                               delete: {
                                                                    method: 'DELETE',
                                                                    params: {}
                                                                  },
                                                               }),






            addformtitle: $resource('/api/formtitle/:id', {
                                                                             headers: {
                                                                                  'Content-Type': 'application/json'
                                                                                           }
                                                                                       }, {
                                                                                        add: {
                                                                                             method: 'POST',
                                                                                             params: {}
                                                                                           }
                                                                                       }),
           displayformtitle: $resource('/api/getformtitle/:id', {
                                                                                      headers: {
                                                                                          'Content-Type': 'application/json'
                                                                                      }
                                                                                  }, {
                                                                                      query: {
                                                                                          method: 'GET',
                                                                                          params: {},
                                                                                          isArray: true
                                                                                      }
                                                                                  }),
             updateformtitle: $resource('/api/updateformtitle/:id', {
                                                                       headers: {
                                                                              'Content-Type': 'application/json'
                                                                        }
                                                                         }, {
                                                                             put: {
                                                                                method: 'PUT',
                                                                                 params: {
                                                                                   id: '@id'
                                                                                          }
                                                                                      }
                                                                                  }),
             deleteformtitle: $resource('/api/deleteformtitle/:id', {}, {
                                                                               delete: {
                                                                                   method: 'DELETE',
                                                                                   params: {}
                                                                                },
                                                                                  }),



       addform: $resource('/api/form/:id', {
                                                                       headers: {
                                                                            'Content-Type': 'application/json'
                                                                                     }
                                                                                 }, {
                                                                                  add: {
                                                                                       method: 'POST',
                                                                                       params: {}
                                                                                     }
                                                                                 }),
        displayform: $resource('/api/getform/:id', {
                                                                                headers: {
                                                                                    'Content-Type': 'application/json'
                                                                                }
                                                                            }, {
                                                                                query: {
                                                                                    method: 'GET',
                                                                                    params: {},
                                                                                    isArray: true
                                                                                }
                                                                            }),
       updateform: $resource('/api/updateform/:id', {
                                                                 headers: {
                                                                        'Content-Type': 'application/json'
                                                                  }
                                                                   }, {
                                                                       put: {
                                                                          method: 'PUT',
                                                                           params: {
                                                                             id: '@id'
                                                                                    }
                                                                                }
                                                                            }),
       deleteform: $resource('/api/deleteform/:id', {}, {
                                                                         delete: {
                                                                             method: 'DELETE',
                                                                             params: {}
                                                                          },
                                                                            }),


      addgoldSilver: $resource('/api/goldSilver/:id', {
                                                                           headers: {
                                                                                'Content-Type': 'application/json'
                                                                                         }
                                                                                     }, {
                                                                                      add: {
                                                                                           method: 'POST',
                                                                                           params: {}
                                                                                         }
                                                                                     }),
      displaygoldSilver: $resource('/api/getgoldSilver/:id', {
                                                                                     headers: {
                                                                                         'Content-Type': 'application/json'
                                                                                     }
                                                                                 }, {
                                                                                     query: {
                                                                                         method: 'GET',
                                                                                         params: {},
                                                                                         isArray: true
                                                                                     }
                                                                                 }),

      updategoldSilver: $resource('/api/updategoldSilver/:id', {
                                                                      headers: {
                                                                             'Content-Type': 'application/json'
                                                                       }
                                                                        }, {
                                                                            put: {
                                                                               method: 'PUT',
                                                                                params: {
                                                                                  id: '@id'
                                                                                         }
                                                                                     }
                                                                                 }),
      deletegoldSilver: $resource('/api/deletegoldSilver/:id', {}, {
                                                                              delete: {
                                                                                  method: 'DELETE',
                                                                                  params: {}
                                                                               },
                                                                                 }),

     addsuggestion: $resource('/api/suggestion/:id', {
                                                                               headers: {
                                                                                    'Content-Type': 'application/json'
                                                                                             }
                                                                                         }, {
                                                                                          add: {
                                                                                               method: 'POST',
                                                                                               params: {}
                                                                                             }
                                                                                         }),

     displaysuggestion: $resource('/api/getsuggestion/:id', {
                                                                                         headers: {
                                                                                             'Content-Type': 'application/json'
                                                                                         }
                                                                                     }, {
                                                                                         query: {
                                                                                             method: 'GET',
                                                                                             params: {},
                                                                                             isArray: true
                                                                                         }
                                                                                     }),

     updatesuggestion: $resource('/api/updatesuggestion/:id', {
                                                                          headers: {
                                                                                 'Content-Type': 'application/json'
                                                                           }
                                                                            }, {
                                                                                put: {
                                                                                   method: 'PUT',
                                                                                    params: {
                                                                                      id: '@id'
                                                                                             }
                                                                                         }
                                                                                     }),

   deletesuggestion: $resource('/api/deletesuggestion/:id', {}, {
                                                                                 delete: {
                                                                                     method: 'DELETE',
                                                                                     params: {}
                                                                                  },
                                                                                    }),

   addadmitCard: $resource('/api/admitCard/:id', {
                                                                                  headers: {
                                                                                       'Content-Type': 'application/json'
                                                                                                }
                                                                                            }, {
                                                                                             add: {
                                                                                                  method: 'POST',
                                                                                                  params: {}
                                                                                                }
                                                                                            }),

   displayadmitCard: $resource('/api/getadmitCard/:id', {
                                                                                            headers: {
                                                                                                'Content-Type': 'application/json'
                                                                                            }
                                                                                        }, {
                                                                                            query: {
                                                                                                method: 'GET',
                                                                                                params: {},
                                                                                                isArray: true
                                                                                            }
                                                                                        }),

  updateadmitCard: $resource('/api/updateadmitCard/:id', {
                                                                           headers: {
                                                                                  'Content-Type': 'application/json'
                                                                            }
                                                                             }, {
                                                                                 put: {
                                                                                    method: 'PUT',
                                                                                     params: {
                                                                                       id: '@id'
                                                                                              }
                                                                                          }
                                                                                      }),

  deleteadmitCard: $resource('/api/deleteadmitCard/:id', {}, {
                                                                                   delete: {
                                                                                       method: 'DELETE',
                                                                                       params: {}
                                                                                    },
                                                                                      }),




     addchat: $resource('/api/send-message/:id', {
                                                                                        headers: {
                                                                                             'Content-Type': 'application/json'
                                                                                                      }
                                                                                                  }, {
                                                                                                   add: {
                                                                                                        method: 'POST',
                                                                                                        params: {}
                                                                                                      }
                                                                                                  }),

    displaychat: $resource('/api/list/messages/:id', {
                                                                                                headers: {
                                                                                                    'Content-Type': 'application/json'
                                                                                                }
                                                                                            }, {
                                                                                                query: {
                                                                                                    method: 'GET',
                                                                                                    params: {},
                                                                                                    isArray: true
                                                                                                }
                                                                                            }),

   updatechat: $resource('/api/updatechat/:id', {
                                                                                 headers: {
                                                                                        'Content-Type': 'application/json'
                                                                                  }
                                                                                   }, {
                                                                                       put: {
                                                                                          method: 'PUT',
                                                                                           params: {
                                                                                             id: '@id'
                                                                                                    }
                                                                                                }
                                                                                            }),

  deletechat: $resource('/api/deletechat/:id', {}, {
                                                                                        delete: {
                                                                                            method: 'DELETE',
                                                                                            params: {}
                                                                                         },
                                                                                           }),









//Achievment page start here

 addachievment: $resource('/api/achievment/:id', {
                                                 headers: {
                                                       'Content-Type': 'application/json'
                                                      }
                                                 }, {
                                                 add: {
                                                      method: 'POST',
                                                           params: {}
                                                       }
                                                }),
 displayachievment: $resource('/api/getachievment/:id', {
                                                  headers: {
                                                        'Content-Type': 'application/json'
                                                       }
                                                  }, {
                                                  query: {
                                                      method: 'GET',
                                                      params: {},
                                                      isArray: true
                                                     }
                                                  }),
 updateachievment: $resource('/api/updateachievment/:id', {
                                                    headers: {
                                                         'Content-Type': 'application/json'
                                                       }
                                                    }, {
                                                    put: {
                                                      method: 'PUT',
                                                      params: {
                                                           id: '@id'
                                                        }
                                                      }
                                                    }),
 deleteachievment: $resource('/api/deleteachievment/:id', {}, {
                                                     delete: {
                                                          method: 'DELETE',
                                                          params: {}
                                                        },
                                                     }),


     //Achievment page end here

     //AchievmentLocation page start here
     addachievmentLocation: $resource('/api/achievmentlocation/:id', {
                                                      headers: {
                                                            'Content-Type': 'application/json'
                                                           }
                                                      }, {
                                                      add: {
                                                           method: 'POST',
                                                                params: {}
                                                            }
                                                     }),
      displayachievmentLocation: $resource('/api/getachievmentlocation/:id', {
                                                       headers: {
                                                             'Content-Type': 'application/json'
                                                            }
                                                       }, {
                                                       query: {
                                                           method: 'GET',
                                                           params: {},
                                                           isArray: true
                                                          }
                                                       }),
      updateachievmentLocation: $resource('/api/updateachievmentlocation/:id', {
                                                         headers: {
                                                              'Content-Type': 'application/json'
                                                            }
                                                         }, {
                                                         put: {
                                                           method: 'PUT',
                                                           params: {
                                                                id: '@id'
                                                             }
                                                           }
                                                         }),
      deleteachievmentLocation: $resource('/api/deleteachievmentlocation/:id', {}, {
                                                          delete: {
                                                               method: 'DELETE',
                                                               params: {}
                                                             },
                                                          }),



     //AchievmentLocation page end here

     //Achievment Details page start here


     addahievmentdetails: $resource('/api/ahievmentdetails/:id', {
                                                                         headers: {
                                                                           'Content-Type': 'application/json'
                                                                           }
                                                                             }, {
                                                                               add: {
                                                                                 method: 'POST',
                                                                                  params: {}
                                                                                 }
                                                                              }),
     displayahievmentdetails: $resource('/api/getahievmentdetails/:id', {
                                                                             headers: {
                                                                            'Content-Type': 'application/json'
                                                                               }
                                                                               }, {
                                                                                query: {
                                                                                 method: 'GET',
                                                                                  params: {},
                                                                                    isArray: true
                                                                                     }
                                                                                   }),
    updateahievmentdetails: $resource('/api/updateahievmentdetails/:id', {
                                                                                 headers: {
                                                                                   'Content-Type': 'application/json'
                                                                                  }
                                                                                   }, {
                                                                                     put: {
                                                                                        method: 'PUT',
                                                                                        params: {
                                                                                         id: '@id'
                                                                                          }
                                                                                         }
                                                                                    }),
   deleteahievmentdetails: $resource('/api/deleteahievmentdetails/:id', {}, {
                                                                                     delete: {
                                                                                       method: 'DELETE',
                                                                                         params: {}
                                                                                         },
                                                                                   }),


     //Achievment Details page end here

   achievmentLocationnew: $resource('/api/getachievment-location/:id', {
                                                          headers: {
                                                                'Content-Type': 'application/json'
                                                               }
                                                          }, {
                                                          query: {
                                                              method: 'GET',
                                                              params: {},
                                                              isArray: true
                                                             }
                                                          }),




   //Facebook page start here

   addfacebook: $resource('/api/facebook/:id', {
                                                    headers: {
                                                          'Content-Type': 'application/json'
                                                         }
                                                    }, {
                                                    add: {
                                                         method: 'POST',
                                                              params: {}
                                                          }
                                                   }),
    displayfacebook: $resource('/api/getfacebook/:id', {
                                                     headers: {
                                                           'Content-Type': 'application/json'
                                                          }
                                                     }, {
                                                     query: {
                                                         method: 'GET',
                                                         params: {},
                                                         isArray: true
                                                        }
                                                     }),
    updatefacebook: $resource('/api/updatefacebook/:id', {
                                                       headers: {
                                                            'Content-Type': 'application/json'
                                                          }
                                                       }, {
                                                       put: {
                                                         method: 'PUT',
                                                         params: {
                                                              id: '@id'
                                                           }
                                                         }
                                                       }),
    deletefacebook: $resource('/api/deletefacebook/:id', {}, {
                                                        delete: {
                                                             method: 'DELETE',
                                                             params: {}
                                                           },
                                                        }),



   //Facebook page end here

   //Facebook details page start here

       addfacebookdetails: $resource('/api/facebookDetails/:id', {
                                                         headers: {
                                                               'Content-Type': 'application/json'
                                                              }
                                                         }, {
                                                         add: {
                                                              method: 'POST',
                                                                   params: {}
                                                               }
                                                        }),
         displayfacebookdetails: $resource('/api/getfacebookDetails/:id', {
                                                          headers: {
                                                                'Content-Type': 'application/json'
                                                               }
                                                          }, {
                                                          query: {
                                                              method: 'GET',
                                                              params: {},
                                                              isArray: true
                                                             }
                                                          }),
         updatefacebookdetails: $resource('/api/updatefacebookDetails/:id', {
                                                            headers: {
                                                                 'Content-Type': 'application/json'
                                                               }
                                                            }, {
                                                            put: {
                                                              method: 'PUT',
                                                              params: {
                                                                   id: '@id'
                                                                }
                                                              }
                                                            }),
         deletefacebookdetails: $resource('/api/deletefacebookDetails/:id', {}, {
                                                             delete: {
                                                                  method: 'DELETE',
                                                                  params: {}
                                                                },
                                                             }),



   //Facebook Details page end here

   //Mail page start here

   addmail: $resource('/api/mail/:id', {
                                                       headers: {
                                                             'Content-Type': 'application/json'
                                                            }
                                                       }, {
                                                       add: {
                                                            method: 'POST',
                                                                 params: {}
                                                             }
                                                      }),
    displaymail: $resource('/api/getmail/:id', {
                                                        headers: {
                                                              'Content-Type': 'application/json'
                                                             }
                                                        }, {
                                                        query: {
                                                            method: 'GET',
                                                            params: {},
                                                            isArray: true
                                                           }
                                                        }),
     updatemail: $resource('/api/updatemail/:id', {
                                                          headers: {
                                                               'Content-Type': 'application/json'
                                                             }
                                                          }, {
                                                          put: {
                                                            method: 'PUT',
                                                            params: {
                                                                 id: '@id'
                                                              }
                                                            }
                                                          }),
       deletemail: $resource('/api/deletemail/:id', {}, {
                                                           delete: {
                                                                method: 'DELETE',
                                                                params: {}
                                                              },
                                                           }),


   //Mail page end here

   //Mail details page start here


   addmaildetails: $resource('/api/maildetails/:id', {
                                                            headers: {
                                                                  'Content-Type': 'application/json'
                                                                 }
                                                            }, {
                                                            add: {
                                                                 method: 'POST',
                                                                      params: {}
                                                                  }
                                                           }),
   displaymaildetails: $resource('/api/getmaildetails/:id', {
                                                             headers: {
                                                                   'Content-Type': 'application/json'
                                                                  }
                                                             }, {
                                                             query: {
                                                                 method: 'GET',
                                                                 params: {},
                                                                 isArray: true
                                                                }
                                                             }),
   updatemaildetails: $resource('/api/updatemaildetails/:id', {
                                                               headers: {
                                                                    'Content-Type': 'application/json'
                                                                  }
                                                               }, {
                                                               put: {
                                                                 method: 'PUT',
                                                                 params: {
                                                                      id: '@id'
                                                                   }
                                                                 }
                                                               }),
   deletemaildetails: $resource('/api/deletemaildetails/:id', {}, {
                                                                delete: {
                                                                     method: 'DELETE',
                                                                     params: {}
                                                                   },
                                                                }),



   //Mail details page end here


// Gallery page start here

addgallery: $resource('/api/gallery/:id', {
                                                   headers: {
                                                         'Content-Type': 'application/json'
                                                        }
                                                   }, {
                                                   add: {
                                                        method: 'POST',
                                                             params: {}
                                                         }
                                                  }),
 displaygallery: $resource('/api/getgallery/:id', {
                                                    headers: {
                                                          'Content-Type': 'application/json'
                                                         }
                                                    }, {
                                                    query: {
                                                        method: 'GET',
                                                        params: {},
                                                        isArray: true
                                                       }
                                                    }),
 updategallery: $resource('/api/updategallery/:id', {
                                                      headers: {
                                                           'Content-Type': 'application/json'
                                                         }
                                                      }, {
                                                      put: {
                                                        method: 'PUT',
                                                        params: {
                                                             id: '@id'
                                                          }
                                                        }
                                                      }),
 deletegallery: $resource('/api/deletegallery/:id', {}, {
                                                       delete: {
                                                            method: 'DELETE',
                                                            params: {}
                                                          },
                                                       }),


//Gallaery page end here


// Gallery details page start here

addgallerydetails: $resource('/api/gallerydetails/:id', {
                                                               headers: {
                                                                     'Content-Type': 'application/json'
                                                                    }
                                                               }, {
                                                               add: {
                                                                    method: 'POST',
                                                                         params: {}
                                                                     }
                                                              }),
 displaygallerydetails: $resource('/api/getgallerydetails/:id', {
                                                                headers: {
                                                                      'Content-Type': 'application/json'
                                                                     }
                                                                }, {
                                                                query: {
                                                                    method: 'GET',
                                                                    params: {},
                                                                    isArray: true
                                                                   }
                                                                }),
 updategallerydetails: $resource('/api/updategallerydetails/:id', {
                                                                  headers: {
                                                                       'Content-Type': 'application/json'
                                                                     }
                                                                  }, {
                                                                  put: {
                                                                    method: 'PUT',
                                                                    params: {
                                                                         id: '@id'
                                                                      }
                                                                    }
                                                                  }),
 deletegallerydetails: $resource('/api/deletegallerydetails/:id', {}, {
                                                                   delete: {
                                                                        method: 'DELETE',
                                                                        params: {}
                                                                      },
                                                                   }),



//Gallery details page end here


//Gotra page strat here


addgotra: $resource('/api/gotra/:id', {
                                                             headers: {
                                                                   'Content-Type': 'application/json'
                                                                  }
                                                             }, {
                                                             add: {
                                                                  method: 'POST',
                                                                       params: {}
                                                                   }
                                                            }),

displaygotra: $resource('/api/getgotra/:id', {
                                                                  headers: {
                                                                     'Content-Type': 'application/json'
                                                                         }
                                                                     }, {
                                                                      query: {
                                                                       method: 'GET',
                                                                       params: {},
                                                                       isArray: true
                                                                         }
                                                                   }),

updategotra: $resource('/api/updategotra/:id', {
                                                              headers: {
                                                                   'Content-Type': 'application/json'
                                                                 }
                                                              }, {
                                                              put: {
                                                                method: 'PUT',
                                                                params: {
                                                                     id: '@id'
                                                                  }
                                                                }
                                                              }),
 deletegotra: $resource('/api/deletegotra/:id', {}, {
                                                               delete: {
                                                                    method: 'DELETE',
                                                                    params: {}
                                                                  },
                                                               }),



//Gotra page end here

//User page start here

adduser: $resource('/api/users/:id', {
                                                             headers: {
                                                                   'Content-Type': 'application/json'
                                                                  }
                                                             }, {
                                                             add: {
                                                                  method: 'POST',
                                                                       params: {}
                                                                   }
                                                            }),

displayuser: $resource('/api/getuser/:id', {
                                                                  headers: {
                                                                     'Content-Type': 'application/json'
                                                                         }
                                                                     }, {
                                                                      query: {
                                                                       method: 'GET',
                                                                       params: {},
                                                                       isArray: true
                                                                         }
                                                                   }),


updateuser: $resource('/api/updateuser/:id', {
                                                               headers: {
                                                                    'Content-Type': 'application/json'
                                                                  }
                                                               }, {
                                                               put: {
                                                                 method: 'PUT',
                                                                 params: {
                                                                      id: '@id'
                                                                   }
                                                                 }
                                                               }),
deleteuser: $resource('/api/deleteuser/:id', {}, {
                                                                delete: {
                                                                     method: 'DELETE',
                                                                     params: {}
                                                                   },
                                                                }),


/*Edit for Testing*/
displaydetails: $resource('/api/displaydetails/:name/:gotra/:city/:state/:country/:profession/:sonmaritalstatus/:daughtermaritalstatus/:brothermaritalstatus', {
                                                                  headers: {
                                                                     'Content-Type': 'application/json'
                                                                         }
                                                                     }, {
                                                                      query: {
                                                                       method: 'GET',
                                                                       params: {},
                                                                       isArray: true
                                                                         }
                                                                   }),



//User page end here


// Home page start here


addhome: $resource('/api/home/:id', {
                                                   headers: {
                                                         'Content-Type': 'application/json'
                                                        }
                                                   }, {
                                                   add: {
                                                        method: 'POST',
                                                             params: {}
                                                         }
                                                  }),
displayhome: $resource('/api/gethome/:id', {
                                                    headers: {
                                                          'Content-Type': 'application/json'
                                                         }
                                                    }, {
                                                    query: {
                                                        method: 'GET',
                                                        params: {},
                                                        isArray: true
                                                       }
                                                    }),
updatehome: $resource('/api/updatehome/:id', {
                                                      headers: {
                                                           'Content-Type': 'application/json'
                                                         }
                                                      }, {
                                                      put: {
                                                        method: 'PUT',
                                                        params: {
                                                             id: '@id'
                                                          }
                                                        }
                                                      }),
 deletehome: $resource('/api/deletehome/:id', {}, {
                                                       delete: {
                                                            method: 'DELETE',
                                                            params: {}
                                                          },
                                                       }),


//Home page end here





   };
    }];
    var module = angular.module("myApp");
    module.factory("webservice", webservice);

}());

