'use strict';
var app = angular.module('myApp', ['ui.bootstrap','ngRoute', 'ngAnimate','colorpicker.module', 'ngResource', 'ngMessages','angularjs-dropdown-multiselect', 'textAngular', 'angularFileUpload']);

app.config(['$provide', '$httpProvider', function($provide, $httpProvider) {

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $provide.decorator('taOptions', ['$delegate', function(taOptions) {
        taOptions.forceTextAngularSanitize = true;
        taOptions.keyMappings = [];
        taOptions.toolbar = [
            ['p', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'], /**/
            ['bold', 'italics', 'underline', 'ul', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ];
        taOptions.classes = {
            focussed: 'focussed',
            toolbar: 'btn-toolbar',
            toolbarGroup: 'btn-group',
            toolbarButton: 'btn btn-default',
            toolbarButtonActive: 'active',
            disabled: 'disabled',
            textEditor: 'form-control',
            htmlEditor: 'form-control'
        };
        return taOptions;
    }]);
    $provide.decorator('taTools', ['$delegate', function(taTools) {
        delete taTools.quote.iconclass;
        taTools.quote.buttontext = 'quote';
        return taTools;
    }]);
}]);
app.config(['$routeProvider', function($routeProvider) {

    $routeProvider




     .when('/', {
                            templateUrl: 'assets/partials/screen_saver.html',
                            controller: 'screen_saver'
                        })


      .when('/registration', {
                      templateUrl: 'assets/partials/registration.html',
                      controller: 'registration'
                  })

     .when('/announcement', {
                          templateUrl: 'assets/partials/announcement.html',
                          controller: 'announcement'
                      })
     .when('/organization', {
                               templateUrl: 'assets/partials/organization.html',
                               controller: 'organization'
                           })

     .when('/organizationDetails', {
                                     templateUrl: 'assets/partials/organizationDetails.html',
                                     controller: 'organizationDetails'
                                 })

      .when('/formTitle', {
                                        templateUrl: 'assets/partials/formTitle.html',
                                        controller: 'formTitle'
                                    })


     .when('/form', {
                                   templateUrl: 'assets/partials/form.html',
                                   controller: 'form'
                               })
     .when('/goldSilver', {
                                      templateUrl: 'assets/partials/goldSilver.html',
                                      controller: 'goldSilver'
                                  })

     .when('/suggestion', {
                                        templateUrl: 'assets/partials/suggestion.html',
                                        controller: 'suggestion'
                                    })
     .when('/admitCard', {
                                          templateUrl: 'assets/partials/admitCard.html',
                                          controller: 'admitCard'
                                      })

     .when('/chat', {
                                              templateUrl: 'assets/partials/chat.html',
                                              controller: 'chat'
                                          })

      .when('/achievment', {
                                                 templateUrl: 'assets/partials/achievment.html',
                                                 controller: 'achievment'
                                             })
      .when('/achievmentLocation', {
                                                  templateUrl: 'assets/partials/achievmentLocation.html',
                                                  controller: 'achievmentLocation'
                                              })
      .when('/achievmentDetails', {
                                                   templateUrl: 'assets/partials/achievmentDetails.html',
                                                   controller: 'achievmentDetails'
                                               })
     .when('/facebook', {
                                                        templateUrl: 'assets/partials/facebook.html',
                                                        controller: 'facebook'
                                                    })

     .when('/facebookDetails', {
                                                             templateUrl: 'assets/partials/facebookDetails.html',
                                                             controller: 'facebookDetails'
                                                         })

    .when('/mail', {
                                                                 templateUrl: 'assets/partials/mail.html',
                                                                 controller: 'mail'
                                                             })
   .when('/mailDetails', {
                                                                    templateUrl: 'assets/partials/mailDetails.html',
                                                                    controller: 'mailDetails'
                                                                })
   .when('/gallery', {
                                                                       templateUrl: 'assets/partials/gallery.html',
                                                                       controller: 'gallery'
                                                                   })
    .when('/galleryDetails', {
                                                                           templateUrl: 'assets/partials/galleryDetails.html',
                                                                           controller: 'galleryDetails'
                                                                       })
   .when('/gotra', {
                                                                              templateUrl: 'assets/partials/gotra.html',
                                                                              controller: 'gotra'
                                                                          })
   .when('/user', {
                                                                                 templateUrl: 'assets/partials/user.html',
                                                                                 controller: 'user'
                                                                             })

   .when('/home', {
                                                                                    templateUrl: 'assets/partials/home.html',
                                                                                    controller: 'home'
                                                                                })
   .when('/adds', {
                                                                                    templateUrl: 'assets/partials/adds.html',
                                                                                    controller: 'adds'
                                                                                })


      $routeProvider.otherwise({
        redirectTo: '/'
    });

}])

app.controller('indexCtrl', ['$rootScope', '$scope', '$location', 'webservice', function($rootScope, $scope, $location, webservice) {

    console.log("index--");

}]);