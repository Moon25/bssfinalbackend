package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Facebook extends UAC {

    @Id
    public Long id;

    public String name;

    public String thumbnail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "facebook")
    List<FacebookDetails> facebookDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<FacebookDetails> getFacebookDetails() {
        return facebookDetails;
    }

    public void setFacebookDetails(List<FacebookDetails> facebookDetails) {
        this.facebookDetails = facebookDetails;
    }
}
