package models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Gotra extends UAC {

    @Id
    public Long id;

    public String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

/*
 ALTER TABLE `bssdb`.`announcement`
CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL ;

*
* */


}
