package models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GoldSliver extends UAC {
    @Id
    public Long id;

    public String name;

    public String price;

    public String file;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
