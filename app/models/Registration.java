package models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Registration extends UAC {

    @Id
    public Long id;

    public String name;

    public String email;

    public String password;

    //public String thumbnail;

    /*@OneToMany(mappedBy = "category")
    List<SubCategory> subCategories;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }*/
}
