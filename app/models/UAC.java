package models;

import io.ebean.Model;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;


/**
 *
 * Created by PlusTV Technologies on 25/5/17.
 * For access control
 * @Inheritance(strategy = InheritanceType.JOINED)
 */

@MappedSuperclass
public abstract class UAC extends Model {

    @ManyToOne
    @Transient
    Company company;

    public Company getCompany() {
        return this.company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}