package models;

import play.data.format.Formats;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class Announcement extends UAC {

    @Id
    public Long id;

    public String heading;

    //public String date;

    public Long date;

    public String description;
/*
 ALTER TABLE `bssdb`.`announcement`
CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL ;

*
* */

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
