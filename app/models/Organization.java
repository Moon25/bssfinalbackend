package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Organization extends UAC {

    @Id
    public Long id;

    public String name;

    public String thumbnail;

    @OneToMany(mappedBy = "organization")
    List<OrganizationDetails> organizationDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<OrganizationDetails> getOrganizationDetails() {
        return organizationDetails;
    }

    public void setOrganizationDetails(List<OrganizationDetails> organizationDetails) {
        this.organizationDetails = organizationDetails;
    }
}
