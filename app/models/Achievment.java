package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Achievment extends UAC {

    @Id
    public Long id;

    public String name;

    public String thumbnail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "achievment")
    List<AchievmentLocation> achievmentLocations;

    public List<AchievmentLocation> getAchievmentLocations() {
        return achievmentLocations;
    }

    public void setAchievmentLocations(List<AchievmentLocation> achievmentLocations) {
        this.achievmentLocations = achievmentLocations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
