package models;


import io.ebean.Ebean;

import javax.persistence.*;

/**
 * Created by medroid-3 on 12/7/17.
 */
@Entity
public class User extends UAC {

    @Id
    public Long id;

    public String name;
    public  String yob;
    public  String gotra;
    public  String address;
    public  String city;
    public  String state;
    public  String country;
    public  String mobile;

    public  String profilePic; // image name

    @Column(unique = true)
    public String email;

    public String password;
    public  String profession;
    public  String fathername;
    public  String mothername;
    public  String mothergotra;
    public  String wifename;
    public  String wifegotra;
    public  String sonname;
    public String sonmaritalstatus;
    public  String sonnameone;
    public String sonmaritalstatusone;
    public  String sonnametwo;
    public String sonmaritalstatustwo;
    public  String sonnamethree;
    public String sonmaritalstatusthree;
    public  String sonnamefour;
    public String sonmaritalstatusfour;
    public  String sonnamefive;
    public String sonmaritalstatusfive;

    public  String daughtername;
    public  String daughtermaritalstatus;
    public  String daughternameone;
    public  String daughtermaritalstatusone;
    public  String daughternametwo;
    public  String daughtermaritalstatustwo;
    public  String daughternamethree;
    public  String daughtermaritalstatusthree;
    public  String daughternamefour;
    public  String daughtermaritalstatusfour;
    public  String daughternamefive;
    public  String daughtermaritalstatusfive;
    public String brothername;
    public   String brothermaritalstatus;
    public String brothernameone;
    public   String brothermaritalstatusone;
    public String brothernametwo;
    public   String brothermaritalstatustwo;
    public String brothernamethree;
    public   String brothermaritalstatusthree;
    public String brothernamefour;
    public   String brothermaritalstatusfour;
    public String brothernamefive;
    public   String brothermaritalstatusfive;
    public  String sistername;
    public  String sistermaritalstatus;
    public  String sisternameone;
    public  String sistermaritalstatusone;
    public  String sisternametwo;
    public  String sistermaritalstatustwo;
    public  String sisternamethree;
    public  String sistermaritalstatusthree;
    public  String sisternamefour;
    public  String sistermaritalstatusfour;

    /**
     * @see constants.UserType
     */
    public int usertype; //1 Admin,2 Users

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String accessToken;


   //public



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYob() {
        return yob;
    }

    public void setYob(String yob) {
        this.yob = yob;
    }

    public String getGotra() {
        return gotra;
    }

    public void setGotra(String gotra) {
        this.gotra = gotra;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getMothername() {
        return mothername;
    }

    public void setMothername(String mothername) {
        this.mothername = mothername;
    }

    public String getMothergotra() {
        return mothergotra;
    }

    public void setMothergotra(String mothergotra) {
        this.mothergotra = mothergotra;
    }

    public String getWifename() {
        return wifename;
    }

    public void setWifename(String wifename) {
        this.wifename = wifename;
    }

    public String getWifegotra() {
        return wifegotra;
    }

    public void setWifegotra(String wifegotra) {
        this.wifegotra = wifegotra;
    }

    public String getSonname() {
        return sonname;
    }

    public void setSonname(String sonname) {
        this.sonname = sonname;
    }

    public String getSonmaritalstatus() {
        return sonmaritalstatus;
    }

    public void setSonmaritalstatus(String sonmaritalstatus) {
        this.sonmaritalstatus = sonmaritalstatus;
    }

    public String getSonnameone() {
        return sonnameone;
    }

    public void setSonnameone(String sonnameone) {
        this.sonnameone = sonnameone;
    }

    public String getSonmaritalstatusone() {
        return sonmaritalstatusone;
    }

    public void setSonmaritalstatusone(String sonmaritalstatusone) {
        this.sonmaritalstatusone = sonmaritalstatusone;
    }

    public String getSonnametwo() {
        return sonnametwo;
    }

    public void setSonnametwo(String sonnametwo) {
        this.sonnametwo = sonnametwo;
    }

    public String getSonmaritalstatustwo() {
        return sonmaritalstatustwo;
    }

    public void setSonmaritalstatustwo(String sonmaritalstatustwo) {
        this.sonmaritalstatustwo = sonmaritalstatustwo;
    }

    public String getSonnamethree() {
        return sonnamethree;
    }

    public void setSonnamethree(String sonnamethree) {
        this.sonnamethree = sonnamethree;
    }

    public String getSonmaritalstatusthree() {
        return sonmaritalstatusthree;
    }

    public void setSonmaritalstatusthree(String sonmaritalstatusthree) {
        this.sonmaritalstatusthree = sonmaritalstatusthree;
    }

    public String getSonnamefour() {
        return sonnamefour;
    }

    public void setSonnamefour(String sonnamefour) {
        this.sonnamefour = sonnamefour;
    }

    public String getSonmaritalstatusfour() {
        return sonmaritalstatusfour;
    }

    public void setSonmaritalstatusfour(String sonmaritalstatusfour) {
        this.sonmaritalstatusfour = sonmaritalstatusfour;
    }

    public String getSonnamefive() {
        return sonnamefive;
    }

    public void setSonnamefive(String sonnamefive) {
        this.sonnamefive = sonnamefive;
    }

    public String getSonmaritalstatusfive() {
        return sonmaritalstatusfive;
    }

    public void setSonmaritalstatusfive(String sonmaritalstatusfive) {
        this.sonmaritalstatusfive = sonmaritalstatusfive;
    }

    public String getDaughtername() {
        return daughtername;
    }

    public void setDaughtername(String daughtername) {
        this.daughtername = daughtername;
    }

    public String getDaughtermaritalstatus() {
        return daughtermaritalstatus;
    }

    public void setDaughtermaritalstatus(String daughtermaritalstatus) {
        this.daughtermaritalstatus = daughtermaritalstatus;
    }

    public String getDaughternameone() {
        return daughternameone;
    }

    public void setDaughternameone(String daughternameone) {
        this.daughternameone = daughternameone;
    }

    public String getDaughtermaritalstatusone() {
        return daughtermaritalstatusone;
    }

    public void setDaughtermaritalstatusone(String daughtermaritalstatusone) {
        this.daughtermaritalstatusone = daughtermaritalstatusone;
    }

    public String getDaughternametwo() {
        return daughternametwo;
    }

    public void setDaughternametwo(String daughternametwo) {
        this.daughternametwo = daughternametwo;
    }

    public String getDaughtermaritalstatustwo() {
        return daughtermaritalstatustwo;
    }

    public void setDaughtermaritalstatustwo(String daughtermaritalstatustwo) {
        this.daughtermaritalstatustwo = daughtermaritalstatustwo;
    }

    public String getDaughternamethree() {
        return daughternamethree;
    }

    public void setDaughternamethree(String daughternamethree) {
        this.daughternamethree = daughternamethree;
    }

    public String getDaughtermaritalstatusthree() {
        return daughtermaritalstatusthree;
    }

    public void setDaughtermaritalstatusthree(String daughtermaritalstatusthree) {
        this.daughtermaritalstatusthree = daughtermaritalstatusthree;
    }

    public String getDaughternamefour() {
        return daughternamefour;
    }

    public void setDaughternamefour(String daughternamefour) {
        this.daughternamefour = daughternamefour;
    }

    public String getDaughtermaritalstatusfour() {
        return daughtermaritalstatusfour;
    }

    public void setDaughtermaritalstatusfour(String daughtermaritalstatusfour) {
        this.daughtermaritalstatusfour = daughtermaritalstatusfour;
    }

    public String getDaughternamefive() {
        return daughternamefive;
    }

    public void setDaughternamefive(String daughternamefive) {
        this.daughternamefive = daughternamefive;
    }

    public String getDaughtermaritalstatusfive() {
        return daughtermaritalstatusfive;
    }

    public void setDaughtermaritalstatusfive(String daughtermaritalstatusfive) {
        this.daughtermaritalstatusfive = daughtermaritalstatusfive;
    }

    public String getBrothername() {
        return brothername;
    }

    public void setBrothername(String brothername) {
        this.brothername = brothername;
    }

    public String getBrothermaritalstatus() {
        return brothermaritalstatus;
    }

    public void setBrothermaritalstatus(String brothermaritalstatus) {
        this.brothermaritalstatus = brothermaritalstatus;
    }

    public String getBrothernameone() {
        return brothernameone;
    }

    public void setBrothernameone(String brothernameone) {
        this.brothernameone = brothernameone;
    }

    public String getBrothermaritalstatusone() {
        return brothermaritalstatusone;
    }

    public void setBrothermaritalstatusone(String brothermaritalstatusone) {
        this.brothermaritalstatusone = brothermaritalstatusone;
    }

    public String getBrothernametwo() {
        return brothernametwo;
    }

    public void setBrothernametwo(String brothernametwo) {
        this.brothernametwo = brothernametwo;
    }

    public String getBrothermaritalstatustwo() {
        return brothermaritalstatustwo;
    }

    public void setBrothermaritalstatustwo(String brothermaritalstatustwo) {
        this.brothermaritalstatustwo = brothermaritalstatustwo;
    }

    public String getBrothernamethree() {
        return brothernamethree;
    }

    public void setBrothernamethree(String brothernamethree) {
        this.brothernamethree = brothernamethree;
    }

    public String getBrothermaritalstatusthree() {
        return brothermaritalstatusthree;
    }

    public void setBrothermaritalstatusthree(String brothermaritalstatusthree) {
        this.brothermaritalstatusthree = brothermaritalstatusthree;
    }

    public String getBrothernamefour() {
        return brothernamefour;
    }

    public void setBrothernamefour(String brothernamefour) {
        this.brothernamefour = brothernamefour;
    }

    public String getBrothermaritalstatusfour() {
        return brothermaritalstatusfour;
    }

    public void setBrothermaritalstatusfour(String brothermaritalstatusfour) {
        this.brothermaritalstatusfour = brothermaritalstatusfour;
    }

    public String getBrothernamefive() {
        return brothernamefive;
    }

    public void setBrothernamefive(String brothernamefive) {
        this.brothernamefive = brothernamefive;
    }

    public String getBrothermaritalstatusfive() {
        return brothermaritalstatusfive;
    }

    public void setBrothermaritalstatusfive(String brothermaritalstatusfive) {
        this.brothermaritalstatusfive = brothermaritalstatusfive;
    }

    public String getSistername() {
        return sistername;
    }

    public void setSistername(String sistername) {
        this.sistername = sistername;
    }

    public String getSistermaritalstatus() {
        return sistermaritalstatus;
    }

    public void setSistermaritalstatus(String sistermaritalstatus) {
        this.sistermaritalstatus = sistermaritalstatus;
    }

    public String getSisternameone() {
        return sisternameone;
    }

    public void setSisternameone(String sisternameone) {
        this.sisternameone = sisternameone;
    }

    public String getSistermaritalstatusone() {
        return sistermaritalstatusone;
    }

    public void setSistermaritalstatusone(String sistermaritalstatusone) {
        this.sistermaritalstatusone = sistermaritalstatusone;
    }

    public String getSisternametwo() {
        return sisternametwo;
    }

    public void setSisternametwo(String sisternametwo) {
        this.sisternametwo = sisternametwo;
    }

    public String getSistermaritalstatustwo() {
        return sistermaritalstatustwo;
    }

    public void setSistermaritalstatustwo(String sistermaritalstatustwo) {
        this.sistermaritalstatustwo = sistermaritalstatustwo;
    }

    public String getSisternamethree() {
        return sisternamethree;
    }

    public void setSisternamethree(String sisternamethree) {
        this.sisternamethree = sisternamethree;
    }

    public String getSistermaritalstatusthree() {
        return sistermaritalstatusthree;
    }

    public void setSistermaritalstatusthree(String sistermaritalstatusthree) {
        this.sistermaritalstatusthree = sistermaritalstatusthree;
    }

    public String getSisternamefour() {
        return sisternamefour;
    }

    public void setSisternamefour(String sisternamefour) {
        this.sisternamefour = sisternamefour;
    }

    public String getSistermaritalstatusfour() {
        return sistermaritalstatusfour;
    }

    public void setSistermaritalstatusfour(String sistermaritalstatusfour) {
        this.sistermaritalstatusfour = sistermaritalstatusfour;
    }

    public int getUsertype() {
        return usertype;
    }

    public void setUsertype(int usertype) {
        this.usertype = usertype;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public User() {
    }


    public User(Long id, String name, String yob,String gotra,String address,String city,String state,String country,String mobile,String email,String password,String profession,String fathername,String mothername,String mothergotra,String wifename,String wifegotra,String sonname,String songotra,String sonmaritalstatus,String daughtername,String daughtergotra,String daughtermaritalstatus,String brothername,String bothergotra,String brothermaritalstatus, int usertype, String accessToken,String profilePic) {
        this.id = id;
        this.name = name;
        this.yob = yob;
        this.gotra = gotra;
        this.address = address;
        this.city = city;
        this.state = state;
        this.country = country;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.profession = profession;
        this.fathername = fathername;
        this.mothername = mothername;
        this.mothergotra = mothergotra;
        this.wifename = wifename;
        this.wifegotra = wifegotra;
        this.sonname = sonname;
        this.sonmaritalstatus = sonmaritalstatus;
        this.daughtername = daughtername;
        this.daughtermaritalstatus = daughtermaritalstatus;
        this.usertype = usertype;
        this.accessToken = accessToken;
        this.profilePic =profilePic;
    }

}
