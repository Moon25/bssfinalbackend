package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class AchievmentLocation extends UAC {

    @Id
    public Long id;

    public String locationname;

    public String thumbnail;

    @ManyToOne
    public Achievment achievment;


    @OneToMany(mappedBy = "achievmentLocation")
    List<AchievmentDetails> achievmentDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocationname() {
        return locationname;
    }

    public void setLocationname(String locationname) {
        this.locationname = locationname;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Achievment getAchievment() {
        return achievment;
    }

    public void setAchievment(Achievment achievment) {
        this.achievment = achievment;
    }

    public List<AchievmentDetails> getAchievmentDetails() {
        return achievmentDetails;
    }

    public void setAchievmentDetails(List<AchievmentDetails> achievmentDetails) {
        this.achievmentDetails = achievmentDetails;
    }
}
