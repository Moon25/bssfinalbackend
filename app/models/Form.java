package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Form extends UAC {
    @Id
    public Long id;

    public String thumbnail;

    @ManyToOne
    public FormTitle formTitle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public FormTitle getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(FormTitle formTitle) {
        this.formTitle = formTitle;
    }
}
