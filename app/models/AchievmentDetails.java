package models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class AchievmentDetails extends UAC {
    @Id
    public Long id;
    @ManyToOne
    public Achievment achievment;

    public  String serialno;

    public String name;

    public String fathername;

    public String aadharno;

    public String mobile;

    public  String district;

    public  String state;

    @ManyToOne
    public AchievmentLocation achievmentLocation;

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Achievment getAchievment() {
        return achievment;
    }

    public void setAchievment(Achievment achievment) {
        this.achievment = achievment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getAadharno() {
        return aadharno;
    }

    public void setAadharno(String aadharno) {
        this.aadharno = aadharno;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public AchievmentLocation getAchievmentLocation() {
        return achievmentLocation;
    }

    public void setAchievmentLocation(AchievmentLocation achievmentLocation) {
        this.achievmentLocation = achievmentLocation;
    }
}
