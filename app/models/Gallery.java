package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Gallery extends UAC {

    @Id
    public Long id;

    public String name;

    public  Long date;

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    @OneToMany(mappedBy = "gallery")
    List<GalleryDetails> galleryDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<GalleryDetails> getGalleryDetails() {
        return galleryDetails;
    }

    public void setGalleryDetails(List<GalleryDetails> galleryDetails) {
        this.galleryDetails = galleryDetails;
    }
}
