package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Achievment;
import models.AchievmentLocation;
import models.Organization;
import models.OrganizationDetails;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class OrganizationDetailsController extends BaseController {

    @Inject
    public OrganizationDetailsController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }
    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        OrganizationDetails organizationDetails =new OrganizationDetails();

        Long organization=jsonNode.findPath("organization").asLong();
        Organization organization1=ebeanServer.find(Organization.class).setId(organization).findOne();
        String thumbnail=jsonNode.findPath("thumbnail").asText();


        String membername=jsonNode.findPath("membername").asText();
        String designation=jsonNode.findPath("designation").asText();
        String phone=jsonNode.findPath("phone").asText();
        String email=jsonNode.findPath("email").asText();
        organizationDetails.organization=organization1;
        organizationDetails.membername=membername;
        organizationDetails.designation = designation;
        organizationDetails.phone = phone;
        organizationDetails.email = email;
        organizationDetails.save();
        return ok();
    }

    public Result getall() {
        List<OrganizationDetails> organizationDetails = ebeanServer.find(OrganizationDetails.class).select("").fetch("organization").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(organizationDetails));
    }

    /*public Result getorganizationDetails(Long orgId) {
        Organization organization=ebeanServer.find(Organization.class).setId(orgId).findUnique();
        List<OrganizationDetails> organizationDetails = ebeanServer.find(OrganizationDetails.class)
                .select("")
                .where().eq("organization",organization).findList();

        return ok(Json.toJson(organizationDetails));
    }*/

    public Result getorganizationDetails(Long orgId){
        Organization organization=ebeanServer.find(Organization.class).setId(orgId).findUnique();
        List<OrganizationDetails> organizationDetails = ebeanServer.find(OrganizationDetails.class).select("").where().eq("organization",organization).findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(organizationDetails));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        System.out.println(jsonNode);
        OrganizationDetails organizationDetails= ebeanServer.find(OrganizationDetails.class).setId(id).findUnique();
        Long organization=jsonNode.findPath("organization").asLong();
        Organization organization1=ebeanServer.find(Organization.class).setId(organization).findOne();
        String membername=jsonNode.findPath("membername").asText();
        String designation=jsonNode.findPath("designation").asText();
        String phone=jsonNode.findPath("phone").asText();
        String email=jsonNode.findPath("email").asText();
        organizationDetails.setMembername(membername);
        organizationDetails.setDesignation(designation);
        organizationDetails.setPhone(phone);
        organizationDetails.setEmail(email);
        organizationDetails.setOrganization(organization1);
        organizationDetails.update();
        return ok();
    }

    public Result delete(Long id) {
        OrganizationDetails organizationDetails = ebeanServer.find(OrganizationDetails.class).setId(id).findUnique();
        organizationDetails.delete();
        return ok();
    }

}
