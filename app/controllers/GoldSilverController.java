package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Form;
import models.GoldSliver;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class GoldSilverController extends BaseController {

    @Inject
    public GoldSilverController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        GoldSliver goldSliver =new GoldSliver();
        String name=jsonNode.findPath("name").asText();
        String price = jsonNode.findPath("price").asText();
        String file = jsonNode.findPath("file").asText();
        goldSliver.name = name;
        goldSliver.price = price;
        goldSliver.file = file;
        goldSliver.save();
        return ok();
    }

    public Result getall() {
        //Form form = ebeanServer.find(Form.class).select("file").findUnique();
        List<GoldSliver> goldSlivers = ebeanServer.find(GoldSliver.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(goldSlivers));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        GoldSliver goldSliver= ebeanServer.find(GoldSliver.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        String price = jsonNode.findPath("price").asText();
        String file = jsonNode.findPath("file").asText();
        goldSliver.setName(name);
        goldSliver.setPrice(price);
        goldSliver.setFile(file);
        goldSliver.update();
        return ok();
    }

    public Result delete(Long id) {
        GoldSliver goldSliver = ebeanServer.find(GoldSliver.class).setId(id).findUnique();
        goldSliver.delete();
        return ok();
    }



}




