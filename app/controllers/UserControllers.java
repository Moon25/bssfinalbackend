package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import constants.UserType;
import io.ebean.*;
import io.ebean.text.json.JsonContext;
import jdk.nashorn.internal.ir.Expression;
import models.Registration;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import javax.inject.Inject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;


public class UserControllers extends BaseController {


    private static final int USERNOT_FOUND=-1;
    private static final int USER_PASSWORD_WRONG=0;
    private static final int USER_OK=1;

    private FormFactory formFactory;

    @Inject
    public UserControllers(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext,FormFactory formFactory) {
        super(ebeanConfig, executionContext);
        this.formFactory= formFactory;
    }



    /* @BodyParser.Of(BodyParser.Json.class)
    public Result login() {
        System.out.print("login");
        JsonNode jsonNode = Controller.request().body().asJson();
        String email = jsonNode.findPath("username").asText();
        String password = jsonNode.findPath("password").asText();
       // User user =User.find.where().eq("email",email).findUnique();
        User user =ebeanServer.find(User.class).where().eq("email",email).findUnique();



        switch (checkAuthentication(email,password,user)){

            case USERNOT_FOUND : return notFound(Json.toJson("User Not found"));

            case USER_PASSWORD_WRONG :return badRequest(Json.toJson("Either username or passsword  is wrong"));

            case USER_OK :
                      return ok(getUserFromSession());

            default: return ok(Json.toJson("Bad request"));

        }


    }
*/
    public Result login(){
        Form<User> loginForm = formFactory.form(User.class).bindFromRequest();

        User user =ebeanServer.find(User.class).where().eq("email",loginForm.get().getEmail()).findUnique();
        switch (checkAuthentication(loginForm,user)){

            case USERNOT_FOUND:  flash().put("failure","User not found");
                return Results.redirect("/");


            case USER_PASSWORD_WRONG :  flash().put("failure","Either email-id or password  is wrong.");
                return Results.redirect("/");


            case USER_OK : return Results.redirect("/home" );

            default:
                return Results.redirect("/");
        }


    }

    public Result loginAPI(){
        Form<User> loginForm = formFactory.form(User.class).bindFromRequest();

        User user =ebeanServer.find(User.class).where().eq("email",loginForm.get().getEmail()).findUnique();



        switch (checkAuthentication(loginForm,user)){

            case USERNOT_FOUND : return notFound(Json.toJson(new Response("User not found",404,null)));


            case USER_PASSWORD_WRONG :return badRequest(Json.toJson(new Response("Either email-id or password  is wrong.",400,null)));

            case USER_OK :
                return ok(Json.toJson(new Response("success",200,user)));

            default: return ok(Json.toJson(new Response("Bad request",400,null)));

        }

    }

    public int checkAuthentication( Form<User>  login, User user){

        if(user==null)
            return USERNOT_FOUND;

        if(user.getEmail().equals(login.get().getEmail())&& user.password.equals(login.get().getPassword())){
            setUserToSession(user);
            return USER_OK;
        }
        return USER_PASSWORD_WRONG;
    }


    public static class Response{

        public String message;
        public  Integer statusCode;
        public User user;


        public Response(String message, Integer statusCode, User user) {
            this.message = message;
            this.statusCode = statusCode;
            this.user = user;
        }




    }



    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = request().body().asJson();
        User newUser=new User();
        String name=jsonNode.findPath("name").asText();
        String yob=jsonNode.findPath("yob").asText();
        String gotra=jsonNode.findPath("gotra").asText();
        String address=jsonNode.findPath("address").asText();
        String city=jsonNode.findPath("city").asText();
        String state=jsonNode.findPath("state").asText();
        String country=jsonNode.findPath("country").asText();
        String mobile=jsonNode.findPath("mobile").asText();
        String email=jsonNode.findPath("email").asText();
        String password=jsonNode.findPath("password").asText();
        String profession=jsonNode.findPath("profession").asText();
        String fathername=jsonNode.findPath("fathername").asText();
        String mothername=jsonNode.findPath("mothername").asText();
        String mothergotra=jsonNode.findPath("mothergotra").asText();
        String wifename=jsonNode.findPath("wifename").asText();
        String wifegotra=jsonNode.findPath("wifegotra").asText();
        String sonname=jsonNode.findPath("sonname").asText();
        String sonmaritalstatus=jsonNode.findPath("sonmaritalstatus").asText();
        String sonnameone=jsonNode.findPath("sonnameone").asText();
        String sonmaritalstatusone=jsonNode.findPath("sonmaritalstatusone").asText();
        String sonnametwo=jsonNode.findPath("sonnametwo").asText();
        String sonmaritalstatustwo=jsonNode.findPath("sonmaritalstatustwo").asText();
        String sonnamethree=jsonNode.findPath("sonnamethree").asText();
        String sonmaritalstatusthree=jsonNode.findPath("sonmaritalstatusthree").asText();
        String sonnamefour=jsonNode.findPath("sonnamefour").asText();
        String sonmaritalstatusfour=jsonNode.findPath("sonmaritalstatusfour").asText();
        String sonnamefive=jsonNode.findPath("sonnamefive").asText();
        String sonmaritalstatusfive=jsonNode.findPath("sonmaritalstatusfive").asText();
        String daughtername=jsonNode.findPath("daughtername").asText();
        String daughtermaritalstatus=jsonNode.findPath("daughtermaritalstatus").asText();
        String daughternameone=jsonNode.findPath("daughternameone").asText();
        String daughtermaritalstatustone=jsonNode.findPath("daughtermaritalstatusone").asText();
        String daughternametwo=jsonNode.findPath("daughternametwo").asText();
        String daughtermaritalstatustwo=jsonNode.findPath("daughtermaritalstatustwo").asText();
        String daughternamethree=jsonNode.findPath("daughternamethree").asText();
        String daughtermaritalstatusthree=jsonNode.findPath("daughtermaritalstatusthree").asText();
        String daughternamefour=jsonNode.findPath("daughternamefour").asText();
        String daughtermaritalstatusfour=jsonNode.findPath("daughtermaritalstatusfour").asText();
        String daughternamefive=jsonNode.findPath("daughternamefive").asText();
        String daughtermaritalstatusfive=jsonNode.findPath("daughtermaritalstatusfive").asText();
        String brothername=jsonNode.findPath("brothername").asText();
        String brothermaritalstatus=jsonNode.findPath("brothermaritalstatus").asText();
        String brothernameone=jsonNode.findPath("brothernameone").asText();
        String brothermaritalstatusone=jsonNode.findPath("brothermaritalstatusone").asText();
        String brothernametwo=jsonNode.findPath("brothernametwo").asText();
        String brothermaritalstatustwo=jsonNode.findPath("brothermaritalstatustwo").asText();
        String brothernamethree=jsonNode.findPath("brothernamethree").asText();
        String brothermaritalstatusthree=jsonNode.findPath("brothermaritalstatusthree").asText();
        String brothernamefour=jsonNode.findPath("brothernamefour").asText();
        String brothermaritalstatusfour=jsonNode.findPath("brothermaritalstatusfour").asText();
        String brothernamefive=jsonNode.findPath("brothernamefive").asText();
        String brothermaritalstatusfive=jsonNode.findPath("brothermaritalstatusfive").asText();
        String sistername=jsonNode.findPath("sistername").asText();
        String sistermaritalstatus=jsonNode.findPath("sistermaritalstatus").asText();
        String sisternameone=jsonNode.findPath("sisternameone").asText();
        String sistermaritalstatusone=jsonNode.findPath("sistermaritalstatusone").asText();
        String sisternametwo=jsonNode.findPath("sisternametwo").asText();
        String sistermaritalstatustwo=jsonNode.findPath("sistermaritalstatustwo").asText();
        String sisternamethree=jsonNode.findPath("sisternamethree").asText();
        String sistermaritalstatusthree=jsonNode.findPath("sistermaritalstatusthree").asText();
        String sisternamefour=jsonNode.findPath("sisternamefour").asText();
        String sistermaritalstatusfour=jsonNode.findPath("sistermaritalstatusfour").asText();
        String profilePic=jsonNode.findPath("profilePic").asText();

        newUser.name=name;

        newUser.yob=yob;
        newUser.gotra=gotra;
        newUser.address=address;
        newUser.city=city;
        newUser.state=state;
        newUser.country=country;
        newUser.mobile=mobile;
        newUser.email=email;
        newUser.password=password;
        newUser.profession=profession;
        newUser.fathername=fathername;
        newUser.mothername=mothername;
        newUser.mothergotra=mothergotra;
        newUser.wifename=wifename;
        newUser.wifegotra=wifegotra;
        newUser.sonname=sonname;
        newUser.sonmaritalstatus=sonmaritalstatus;
        newUser.sonnameone=sonnameone;
        newUser.sonmaritalstatusone=sonmaritalstatusone;
        newUser.sonnametwo=sonnametwo;
        newUser.sonmaritalstatustwo=sonmaritalstatustwo;
        newUser.sonnamethree=sonnamethree;
        newUser.sonmaritalstatusthree=sonmaritalstatusthree;
        newUser.sonnamefour=sonnamefour;
        newUser.sonmaritalstatusfour=sonmaritalstatusfour;
        newUser.sonnamefive=sonnamefive;
        newUser.sonmaritalstatusfive=sonmaritalstatusfive;
        newUser.profilePic=profilePic;

        newUser.daughtername=daughtername;
        newUser.daughtermaritalstatus=daughtermaritalstatus;
        newUser.daughternameone=daughternameone;
        newUser.daughtermaritalstatusone=daughtermaritalstatustone;
        newUser.daughternametwo=daughternametwo;
        newUser.daughtermaritalstatustwo=daughtermaritalstatustwo;
        newUser.daughternamethree=daughternamethree;
        newUser.daughtermaritalstatusthree=daughtermaritalstatusthree;
        newUser.daughternamefour=daughternamefour;
        newUser.daughtermaritalstatusfour=daughtermaritalstatusfour;
        newUser.daughternamefive=daughternamefive;
        newUser.daughtermaritalstatusfive=daughtermaritalstatusfive;

        newUser.brothername=brothername;
        newUser.brothermaritalstatus=brothermaritalstatus;
        newUser.brothernameone=brothernameone;
        newUser.brothermaritalstatusone=brothermaritalstatusone;
        newUser.brothernametwo=brothernametwo;
        newUser.brothermaritalstatustwo=brothermaritalstatustwo;
        newUser.brothernamethree=brothernamethree;
        newUser.brothermaritalstatusthree=brothermaritalstatusthree;
        newUser.brothernamefour=brothernamefour;
        newUser.brothermaritalstatusfour=brothermaritalstatusfour;
        newUser.brothernamefive=brothernamefive;
        newUser.brothermaritalstatusfive=brothermaritalstatusfive;
        newUser.usertype = UserType.NORMAL.ordinal();

        newUser.sistername=sistername;
        newUser.sistermaritalstatus=sistermaritalstatus;
        newUser.sisternameone=sisternameone;
        newUser.sistermaritalstatusone=sistermaritalstatusone;
        newUser.sisternametwo=sisternametwo;
        newUser.sistermaritalstatustwo=sistermaritalstatustwo;
        newUser.sisternamethree=sisternamethree;
        newUser.sistermaritalstatusthree=sistermaritalstatusthree;
        newUser.sisternamefour=sisternamefour;
        newUser.sistermaritalstatusfour=sistermaritalstatusfour;

        newUser.save();

        return ok(Json.toJson(new Response("success",200,newUser)));
    }

    public Result getall() {

        List<User> users = ebeanServer.find(User.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(users));
    }

    public Result get(long userId) {
        User user = ebeanServer.find(User.class).select("*").where().eq("id",userId).findUnique();
        return ok(Json.toJson(user));
    }


    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        User user= ebeanServer.find(User.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        String yob=jsonNode.findPath("yob").asText();
        String gotra=jsonNode.findPath("gotra").asText();
        String address=jsonNode.findPath("address").asText();
        String city=jsonNode.findPath("city").asText();
        String state=jsonNode.findPath("state").asText();
        String country=jsonNode.findPath("country").asText();
        String mobile=jsonNode.findPath("mobile").asText();
        String email=jsonNode.findPath("email").asText();
        String password=jsonNode.findPath("password").asText();
        String profession=jsonNode.findPath("profession").asText();
        String fathername=jsonNode.findPath("fathername").asText();
        String mothername=jsonNode.findPath("mothername").asText();
        String mothergotra=jsonNode.findPath("mothergotra").asText();
        String wifename=jsonNode.findPath("wifename").asText();
        String wifegotra=jsonNode.findPath("wifegotra").asText();
        String sonname=jsonNode.findPath("sonname").asText();
        String sonmaritalstatus=jsonNode.findPath("sonmaritalstatus").asText();
        String sonnameone=jsonNode.findPath("sonnameone").asText();
        String sonmaritalstatusone=jsonNode.findPath("sonmaritalstatusone").asText();
        String sonnametwo=jsonNode.findPath("sonnametwo").asText();
        String sonmaritalstatustwo=jsonNode.findPath("sonmaritalstatustwo").asText();
        String sonnamethree=jsonNode.findPath("sonnamethree").asText();
        String sonmaritalstatusthree=jsonNode.findPath("sonmaritalstatusthree").asText();
        String sonnamefour=jsonNode.findPath("sonnamefour").asText();
        String sonmaritalstatusfour=jsonNode.findPath("sonmaritalstatusfour").asText();
        String sonnamefive=jsonNode.findPath("sonnamefive").asText();
        String sonmaritalstatusfive=jsonNode.findPath("sonmaritalstatusfive").asText();
        String daughtername=jsonNode.findPath("daughtername").asText();
        String daughtermaritalstatus=jsonNode.findPath("daughtermaritalstatus").asText();
        String daughternameone=jsonNode.findPath("daughternameone").asText();
        String daughtermaritalstatustone=jsonNode.findPath("daughtermaritalstatusone").asText();
        String daughternametwo=jsonNode.findPath("daughternametwo").asText();
        String daughtermaritalstatustwo=jsonNode.findPath("daughtermaritalstatustwo").asText();
        String daughternamethree=jsonNode.findPath("daughternamethree").asText();
        String daughtermaritalstatusthree=jsonNode.findPath("daughtermaritalstatusthree").asText();
        String daughternamefour=jsonNode.findPath("daughternamefour").asText();
        String daughtermaritalstatusfour=jsonNode.findPath("daughtermaritalstatusfour").asText();
        String daughternamefive=jsonNode.findPath("daughternamefive").asText();
        String daughtermaritalstatusfive=jsonNode.findPath("daughtermaritalstatusfive").asText();
        String brothername=jsonNode.findPath("brothername").asText();
        String brothermaritalstatus=jsonNode.findPath("brothermaritalstatus").asText();
        String brothernameone=jsonNode.findPath("brothernameone").asText();
        String brothermaritalstatusone=jsonNode.findPath("brothermaritalstatusone").asText();
        String brothernametwo=jsonNode.findPath("brothernametwo").asText();
        String brothermaritalstatustwo=jsonNode.findPath("brothermaritalstatustwo").asText();
        String brothernamethree=jsonNode.findPath("brothernamethree").asText();
        String brothermaritalstatusthree=jsonNode.findPath("brothermaritalstatusthree").asText();
        String brothernamefour=jsonNode.findPath("brothernamefour").asText();
        String brothermaritalstatusfour=jsonNode.findPath("brothermaritalstatusfour").asText();
        String brothernamefive=jsonNode.findPath("brothernamefive").asText();
        String brothermaritalstatusfive=jsonNode.findPath("brothermaritalstatusfive").asText();
        String sistername=jsonNode.findPath("sistername").asText();
        String sistermaritalstatus=jsonNode.findPath("sistermaritalstatus").asText();
        String sisternameone=jsonNode.findPath("sisternameone").asText();
        String sistermaritalstatusone=jsonNode.findPath("sistermaritalstatusone").asText();
        String sisternametwo=jsonNode.findPath("sisternametwo").asText();
        String sistermaritalstatustwo=jsonNode.findPath("sistermaritalstatustwo").asText();
        String sisternamethree=jsonNode.findPath("sisternamethree").asText();
        String sistermaritalstatusthree=jsonNode.findPath("sistermaritalstatusthree").asText();
        String sisternamefour=jsonNode.findPath("sisternamefour").asText();
        String sistermaritalstatusfour=jsonNode.findPath("sistermaritalstatusfour").asText();
        String profilePic=jsonNode.findPath("profilePic").asText();

        user.setName(name);
        user.setYob(yob);
        user.setGotra(gotra);
        user.setAddress(address);
        user.setCity(city);
        user.setState(state);
        user.setCountry(country);
        user.setMobile(mobile);
        user.setEmail(email);
        user.setProfilePic(profilePic);
        user.setPassword(password);
        user.setProfession(profession);
        user.setFathername(fathername);
        user.setMothername(mothername);
        user.setMothergotra(mothergotra);
        user.setWifename(wifename);
        user.setWifegotra(wifegotra);
        user.setSonname(sonname);
        user.setSonmaritalstatus(sonmaritalstatus);
        user.setSonnameone(sonnameone);
        user.setSonmaritalstatusone(sonmaritalstatusone);
        user.setSonnametwo(sonnametwo);
        user.setSonmaritalstatustwo(sonmaritalstatustwo);
        user.setSonnamethree(sonnamethree);
        user.setSonmaritalstatusthree(sonmaritalstatusthree);
        user.setSonnamefour(sonnamefour);
        user.setSonmaritalstatusfour(sonmaritalstatusfour);
        user.setSonnamefive(sonnamefive);
        user.setSonmaritalstatusfive(sonmaritalstatusfive);
        user.setDaughtername(daughtername);
        user.setDaughtermaritalstatus(daughtermaritalstatus);
        user.setDaughternameone(daughternameone);
        user.setDaughtermaritalstatusone(daughtermaritalstatustone);
        user.setDaughternametwo(daughternametwo);
        user.setDaughtermaritalstatustwo(daughtermaritalstatustwo);
        user.setDaughternamethree(daughternamethree);
        user.setDaughtermaritalstatusthree(daughtermaritalstatusthree);
        user.setDaughternamefour(daughternamefour);
        user.setDaughtermaritalstatusfour(daughtermaritalstatusfour);
        user.setDaughternamefive(daughternamefive);
        user.setDaughtermaritalstatusfive(daughtermaritalstatusfive);
        user.setBrothername(brothername);
        user.setBrothermaritalstatus(brothermaritalstatus);
        user.setBrothernameone(brothernameone);
        user.setBrothermaritalstatusone(brothermaritalstatusone);
        user.setBrothernametwo(brothernametwo);
        user.setBrothermaritalstatustwo(brothermaritalstatustwo);
        user.setBrothernamethree(brothernamethree);
        user.setBrothermaritalstatusthree(brothermaritalstatusthree);
        user.setBrothernamefour(brothernamefour);
        user.setBrothermaritalstatusfour(brothermaritalstatusfour);
        user.setBrothernamefive(brothernamefive);
        user.setBrothermaritalstatusfive(brothermaritalstatusfive);
        user.setSistername(sistername);
        user.setSistermaritalstatus(sistermaritalstatus);
        user.setSisternameone(sisternameone);
        user.setSistermaritalstatusone(sistermaritalstatusone);
        user.setSisternametwo(sisternametwo);
        user.setSistermaritalstatustwo(sistermaritalstatustwo);
        user.setSisternamethree(sisternamethree);
        user.setSistermaritalstatusthree(sistermaritalstatusthree);
        user.setSisternamefour(sisternamefour);
        user.setSistermaritalstatusfour(sistermaritalstatusfour);
        user.update();
        return ok(Json.toJson(new Response("success",200,user)));

    }

    public Result delete(Long id) {
        User user = ebeanServer.find(User.class).setId(id).findUnique();
        user.delete();
        return ok();
    }


    public Result findMembers(String name ,String gotra ,String city,String state,
                              String country,String profession,String sonmaritalstatus ,
                              String daughtermaritalstatus ,String brothermaritalstatus ) {
        System.out.println("NAme----------"+name );
        System.out.println( " " + gotra + " " + city + " " + state );
        System.out.println(country + " "+profession+" "+sonmaritalstatus );
        System.out.println(daughtermaritalstatus+ " "+brothermaritalstatus);

        List<User> users =ebeanServer.find(User.class)
                           .where()
                           .eq("country",country)
                           .eq("state",state)
                           .and(
                                   Expr.ilike("name", "%"+name+"%"),
                                   Expr.ilike("gotra","%"+gotra+"%")

                           )
                           .or()
                           .ilike("city", "%"+city+"%")
                           .or()
                           .ilike("profession","%" +profession+"%")
                           .or()
                           .eq("sonmaritalstatus",sonmaritalstatus)
                           .or()
                           .eq("daughtermaritalstatus",daughtermaritalstatus)
                           .or()
                           .eq("brothermaritalstatus",brothermaritalstatus)
                           .findList();
        System.out.println("dads"+users);
      //   List<User> users = null ;//=ebeanServer.find(User.class).select("name,country,state")
//                         .where()
//                         .or(
//                                 Expr.eq("country",country),
//                                 Expr.eq("state",state)
//                          )
//                         .findList();*/

        // JsonContext jc = Ebean.json();
        return ok(Json.toJson(users));
    }


}
/*
*
*  .or()
                           .eq("sonmaritalstatus",sonmaritalstatus)
                           .or()
                           .eq("daughtermaritalstatus",daughtermaritalstatus)
                           .or()
                           .eq("brothermaritalstatus",brothermaritalstatus)
                           .or()
                           .ilike("city", "%" +city+"%")

                           .or()
                           .ilike("gotra","%"+gotra+"%")*/