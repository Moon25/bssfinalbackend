package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Gallery;
import models.GalleryDetails;
import models.Organization;
import models.OrganizationDetails;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class GalleryDetailsController extends BaseController {

    @Inject
    public GalleryDetailsController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }
    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        GalleryDetails galleryDetails =new GalleryDetails();

        Long gallery=jsonNode.findPath("gallery").asLong();
        Gallery gallery1=ebeanServer.find(Gallery.class).setId(gallery).findOne();



        String thumbnail=jsonNode.findPath("thumbnail").asText();

        galleryDetails.gallery=gallery1;
        galleryDetails.thumbnail=thumbnail;

        galleryDetails.save();
        return ok();
    }

    public Result getall() {
        List<GalleryDetails> galleryDetails = ebeanServer.find(GalleryDetails.class).select("").fetch("gallery").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(galleryDetails));
    }

    /*public Result getgalleryDetails(Long galleryId) {
        Gallery gallery=ebeanServer.find(Gallery.class).setId(galleryId).findUnique();
        List<GalleryDetails> galleryDetails = ebeanServer.find(GalleryDetails.class)
                .select("*")
                .where().eq("gallery",gallery).findList();

        return ok(Json.toJson(galleryDetails));
    }*/


    public Result getgallerydetails(Long galleryId){
        Gallery gallery=ebeanServer.find(Gallery.class).setId(galleryId).findUnique();
        List<GalleryDetails> galleryDetails = ebeanServer.find(GalleryDetails.class).select("").where().eq("gallery",gallery).findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(galleryDetails));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        GalleryDetails galleryDetails= ebeanServer.find(GalleryDetails.class).setId(id).findUnique();
        Long gallery=jsonNode.findPath("gallery").asLong();
        Gallery gallery1=ebeanServer.find(Gallery.class).setId(gallery).findOne();
        String thumbnail=jsonNode.findPath("thumbnail").asText();

        galleryDetails.setThumbnail(thumbnail);

        galleryDetails.setGallery(gallery1);
        galleryDetails.update();
        return ok();
    }

    public Result delete(Long id) {
        GalleryDetails galleryDetails = ebeanServer.find(GalleryDetails.class).setId(id).findUnique();
        galleryDetails.delete();
        return ok();
    }

}
