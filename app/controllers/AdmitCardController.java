package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.AdmitCard;
import models.Suggestion;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class AdmitCardController extends  BaseController {

    @Inject
    public AdmitCardController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        AdmitCard admitCard =new AdmitCard();
        String city = jsonNode.findPath("city").asText();
        String name=jsonNode.findPath("name").asText();
        String mobile = jsonNode.findPath("mobile").asText();

        admitCard.city = city;
        admitCard.name=name;
        admitCard.mobile=mobile;
        admitCard.save();
        return ok();
    }

    public Result getall() {

        List<AdmitCard> admitCard = ebeanServer.find(AdmitCard.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(admitCard));
    }



    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        AdmitCard admitCard= ebeanServer.find(AdmitCard.class).setId(id).findUnique();
        String city = jsonNode.findPath("city").asText();
        String name=jsonNode.findPath("name").asText();
        String mobile=jsonNode.findPath("mobile").asText();
        admitCard.setCity(city);
        admitCard.setName(name);
        admitCard.setMobile(mobile);
        admitCard.update();
        return ok();

    }

    public Result delete(Long id) {
        AdmitCard admitCard = ebeanServer.find(AdmitCard.class).setId(id).findUnique();
        admitCard.delete();
        return ok();
    }


}
