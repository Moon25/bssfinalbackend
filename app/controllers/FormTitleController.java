package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Form;
import models.FormTitle;
import models.Organization;
import models.OrganizationDetails;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class FormTitleController extends  BaseController {

    @Inject
    public FormTitleController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        FormTitle formTitle =new FormTitle();
        String name=jsonNode.findPath("name").asText();
        formTitle.name=name;
        formTitle.save();
        return ok();
    }

    public Result getall() {

        List<FormTitle> formTitles = ebeanServer.find(FormTitle.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(formTitles));
    }

    public Result getSideList(){
        List<FormTitle> formTitles =ebeanServer.find(FormTitle.class).select("").findList();
        JsonArray jsonArray =new JsonArray();
        JsonContext jc = Ebean.json();
        jsonArray.add(jc.toJson(formTitles));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            FormTitle formTitle =ebeanServer.find(FormTitle.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            List<Form> forms =ebeanServer.find(Form.class).select("").where()
                    .eq("formTitle",formTitle).findList();



        }


        return ok(jsonArray.toString());

    }


    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        FormTitle formTitle= ebeanServer.find(FormTitle.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        formTitle.setName(name);
        formTitle.update();
        return ok();

    }

    public Result delete(Long id) {
        FormTitle formTitle = ebeanServer.find(FormTitle.class).setId(id).findUnique();
        formTitle.delete();
        return ok();
    }


}
