package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Announcement;
import models.Registration;
import models.Suggestion;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class SuggestionController extends  BaseController {

    @Inject
    public SuggestionController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Suggestion suggestion =new Suggestion();
        String name=jsonNode.findPath("name").asText();
        String email = jsonNode.findPath("email").asText();
        String mobile = jsonNode.findPath("mobile").asText();
        String description=jsonNode.findPath("description").asText();

        suggestion.name=name;
        suggestion.email=email;
        suggestion.mobile=mobile;
        suggestion.description = description;
        suggestion.save();
        return ok();
    }

    public Result getall() {

        List<Suggestion> suggestion = ebeanServer.find(Suggestion.class).select("").orderBy("id desc").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(suggestion));
    }



    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Suggestion suggestion= ebeanServer.find(Suggestion.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        String email=jsonNode.findPath("email").asText();
        String mobile=jsonNode.findPath("mobile").asText();
        String description = jsonNode.findPath("description").asText();
        suggestion.setName(name);
        suggestion.setEmail(email);
        suggestion.setMobile(mobile);
        suggestion.setDescription(description);
        suggestion.update();
        return ok();

    }

    public Result delete(Long id) {
        Suggestion suggestion = ebeanServer.find(Suggestion.class).setId(id).findUnique();
        suggestion.delete();
        return ok();
    }


}
