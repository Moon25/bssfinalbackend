package controllers;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import models.Company;
import models.User;
import play.db.ebean.EbeanConfig;
import play.mvc.Controller;

import static constants.Constants.SESSION_USERID;
import static constants.Constants.SESSION_USERNAME;

/**
 * Base controller
 */
public abstract class BaseController extends Controller {

   protected  EbeanServer ebeanServer;
   protected   DatabaseExecutionContext executionContext;

    public BaseController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    protected  Company getCompanyOfCurentUser()  {
        User user=null;
        user = getUserFromSession( );
        return ebeanServer.find(Company.class).setId(user.getCompany().getId()).findUnique();
    }


    protected User getUserFromSession() throws NullPointerException{
        long userId;



        try{
            userId = Long.parseLong(session(SESSION_USERID));
        }catch (Exception e){
            return null;
        }


        return ebeanServer.find(User.class).where().eq("id",userId)
                .findUnique();


    }

   protected void setToSession(String key,String value){
       session(key, value);
   }

    protected void clearFromSession(String key){
       session().remove(key);
    }

    protected void setUserToSession(User user){
        setToSession(SESSION_USERID, String.valueOf(user.getId()));
        setToSession(SESSION_USERNAME, user.getName());
    }

    protected void clearUserSession(){
        clearFromSession(SESSION_USERNAME);
        clearFromSession(SESSION_USERID);
    }



}
