package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Facebook;
import models.FacebookDetails;
import models.Mail;
import models.MailDetails;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class MailDetailsController extends BaseController {

    @Inject
    public MailDetailsController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }
    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        MailDetails mailDetails =new MailDetails();

        Long mail=jsonNode.findPath("mail").asLong();
        Mail mail1=ebeanServer.find(Mail.class).setId(mail).findOne();
        String url=jsonNode.findPath("url").asText();
        mailDetails.mail=mail1;
        mailDetails.url=url;
        mailDetails.save();
        return ok();
    }

    public Result getall() {
        List<MailDetails> mailDetails = ebeanServer.find(MailDetails.class).select("").fetch("mail").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(mailDetails));
    }

    public Result getmaildetails(Long mailid) {
        Mail mail=ebeanServer.find(Mail.class).setId(mailid).findUnique();
        List<MailDetails> mailDetails = ebeanServer.find(MailDetails.class)
                .select("")
                .where().eq("mail",mail).findList();

        return ok(Json.toJson(mailDetails));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        MailDetails mailDetails= ebeanServer.find(MailDetails.class).setId(id).findUnique();
        Long mail=jsonNode.findPath("mail").asLong();

        Mail mail1=ebeanServer.find(Mail.class).setId(mail).findOne();
        String url=jsonNode.findPath("url").asText();
        mailDetails.setUrl(url);

        mailDetails.setMail(mail1);
        mailDetails.update();
        return ok();
    }

    public Result delete(Long id) {
        MailDetails mailDetails = ebeanServer.find(MailDetails.class).setId(id).findUnique();
        mailDetails.delete();
        return ok();
    }

}
