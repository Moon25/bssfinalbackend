package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Organization;
import models.OrganizationDetails;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class OrganizationController extends  BaseController {

    @Inject
    public OrganizationController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Organization organization =new Organization();
        String name=jsonNode.findPath("name").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        organization.name=name;
        organization.thumbnail=thumbnail;
        organization.save();
        return ok();
    }

    public Result getall() {

        List<Organization> organizations = ebeanServer.find(Organization.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(organizations));
    }

     public Result getSideList(){
        List<Organization> organizations =ebeanServer.find(Organization.class).select("").findList();
         JsonArray jsonArray =new JsonArray();
         JsonContext jc = Ebean.json();
         jsonArray.add(jc.toJson(organizations));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            Organization organization =ebeanServer.find(Organization.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            List<OrganizationDetails> organizationDetails =ebeanServer.find(OrganizationDetails.class).select("").where()
                    .eq("organization",organization).findList();



        }


         return ok(jsonArray.toString());

     }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Organization organization= ebeanServer.find(Organization.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        organization.setName(name);
        organization.setThumbnail(thumbnail);
        organization.update();
        return ok();

    }

    public Result delete(Long id) {
        Organization organization = ebeanServer.find(Organization.class).setId(id).findUnique();
        organization.delete();
        return ok();
    }


}
