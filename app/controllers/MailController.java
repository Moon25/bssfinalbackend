package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Facebook;
import models.FacebookDetails;
import models.Mail;
import models.MailDetails;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class MailController extends  BaseController {

    @Inject
    public MailController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Mail mail =new Mail();
        String name=jsonNode.findPath("name").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        mail.name=name;
        mail.thumbnail=thumbnail;
        mail.save();
        return ok();
    }

    public Result getall() {

        List<Mail> mail = ebeanServer.find(Mail.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(mail));
    }

    public Result getSideList(){
        List<Mail> mail =ebeanServer.find(Mail.class).select("").findList();
        JsonArray jsonArray =new JsonArray();
        JsonContext jc = Ebean.json();
        jsonArray.add(jc.toJson(mail));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            Mail mail1 =ebeanServer.find(Mail.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            List<MailDetails> mailDetails =ebeanServer.find(MailDetails.class).select("").where()
                    .eq("mail",mail).findList();




        }


        return ok(jsonArray.toString());

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Mail mail= ebeanServer.find(Mail.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        mail.setName(name);
        mail.setThumbnail(thumbnail);
        mail.update();
        return ok();

    }

    public Result delete(Long id) {
        Mail mail = ebeanServer.find(Mail.class).setId(id).findUnique();
        mail.delete();
        return ok();
    }


}
