package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.*;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AchievmentDetailsController extends BaseController {

    @Inject
    public AchievmentDetailsController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }
    @BodyParser.Of(BodyParser.Json.class)
    public Result add() throws NullPointerException {
        JsonNode jsonNode = Controller.request().body().asJson();
        AchievmentDetails achievmentDetails=new AchievmentDetails();

        Long achievment=jsonNode.findPath("achievment").asLong();
        Achievment achievment1=ebeanServer.find(Achievment.class).setId(achievment).findUnique();


        Long achievmentLocation =jsonNode.findPath("achievmentLocation").asLong();
        AchievmentLocation achievmentLocation1=ebeanServer.find(AchievmentLocation.class).setId(achievmentLocation).findUnique();


        String serialno=jsonNode.findPath("serialno").asText();
        String name=jsonNode.findPath("name").asText();
        String fathername=jsonNode.findPath("fathername").asText();

        String aadharno=jsonNode.findPath("aadharno").asText();
        String mobile=jsonNode.findPath("mobile").asText();
        String district=jsonNode.findPath("district").asText();
        String state=jsonNode.findPath("state").asText();

        achievmentDetails.achievment = achievment1;
        achievmentDetails.achievmentLocation = achievmentLocation1;
        achievmentDetails.serialno = serialno ;
        achievmentDetails.name = name;
        achievmentDetails.fathername = fathername;
        achievmentDetails.aadharno = aadharno;
        achievmentDetails.mobile = mobile;
        achievmentDetails.district = district;
        achievmentDetails.state = state;
        achievmentDetails.save();
        return ok();
    }

    public Result getall() {
        List<AchievmentDetails> achievmentDetails = ebeanServer.find(AchievmentDetails.class).select("").fetch("achievment").fetch("achievmentLocation").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(achievmentDetails));
    }

    public Result getdata(Long achievmentdetailsid){
        AchievmentLocation achievmentLocation=ebeanServer.find(AchievmentLocation.class).setId(achievmentdetailsid).findUnique();
        List<AchievmentDetails>achievmentDetails= ebeanServer.find(AchievmentDetails.class).select("").where().eq("achievmentLocation",achievmentLocation).findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(achievmentDetails));
    }
    /*public Result getById(Long id){
        Category category1 =ebeanServer.find(Category.class).setId(id).findUnique();
        List<SubCategory> subCategory1s = ebeanServer.find(SubCategory.class)
                .select("")
                .where()
                .eq("category",category1)
                .findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(subCategory1s));
    }
*/


    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) throws IOException {
        AchievmentDetails achievmentDetails= ebeanServer.find(AchievmentDetails.class).setId(id).findUnique();
        JsonNode jsonNode = Controller.request().body().asJson();

        Long achievment=jsonNode.findPath("achievment").asLong();
        Achievment achievment1=ebeanServer.find(Achievment.class).setId(achievment).findUnique();


        Long  achievmentLocation=jsonNode.findPath("achievmentLocation").asLong();
        AchievmentLocation achievmentLocation1=ebeanServer.find(AchievmentLocation.class).setId(achievmentLocation).findUnique();


        String serialno=jsonNode.findPath("serialno").asText();
        String name=jsonNode.findPath("name").asText();
        String fathername=jsonNode.findPath("fathername").asText();
        String aadharno=jsonNode.findPath("aadharno").asText();
        String mobile=jsonNode.findPath("mobile").asText();
        String district=jsonNode.findPath("district").asText();
        String state=jsonNode.findPath("state").asText();

        achievmentDetails.setSerialno(serialno);
        achievmentDetails.setName(name);
        achievmentDetails.setFathername(fathername);
        achievmentDetails.setAadharno(aadharno);
        achievmentDetails.setMobile(mobile);
        achievmentDetails.setDistrict(district);
        achievmentDetails.setState(state);
        achievmentDetails.setAchievment(achievment1);
        achievmentDetails.setAchievmentLocation(achievmentLocation1);
        achievmentDetails.update();
        return ok();
    }

    public Result delete(Long id) {
        AchievmentDetails achievmentDetails = ebeanServer.find(AchievmentDetails.class).setId(id).findUnique();
        achievmentDetails.delete();
        return ok();
    }

}
