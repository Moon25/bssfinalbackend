package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Achievment;
import models.AchievmentLocation;
import models.Facebook;
import models.FacebookDetails;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class FacebookDetailsController extends BaseController {

    @Inject
    public FacebookDetailsController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }
    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        FacebookDetails facebookDetails =new FacebookDetails();

        Long facebook=jsonNode.findPath("facebook").asLong();
        Facebook facebook1=ebeanServer.find(Facebook.class).setId(facebook).findOne();
        String url=jsonNode.findPath("url").asText();
        facebookDetails.facebook=facebook1;
        facebookDetails.url=url;
        facebookDetails.save();
        return ok();
    }

    public Result getall() {
        List<FacebookDetails> facebookDetails = ebeanServer.find(FacebookDetails.class).select("").fetch("facebook").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(facebookDetails));
    }

    public Result getfacebookdetails(Long locid){
        Facebook facebook=ebeanServer.find(Facebook.class).setId(locid).findUnique();
        List<FacebookDetails> facebookDetails = ebeanServer.find(FacebookDetails.class).select("").where().eq("facebook",facebook).findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(facebookDetails));
    }





    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        FacebookDetails facebookDetails= ebeanServer.find(FacebookDetails.class).setId(id).findUnique();
        Long facebook=jsonNode.findPath("facebook").asLong();

        Facebook facebook1=ebeanServer.find(Facebook.class).setId(facebook).findOne();
        String url=jsonNode.findPath("url").asText();
        facebookDetails.setUrl(url);

        facebookDetails.setFacebook(facebook1);
        facebookDetails.update();
        return ok();
    }

    public Result delete(Long id) {
        FacebookDetails facebookDetails = ebeanServer.find(FacebookDetails.class).setId(id).findUnique();
        facebookDetails.delete();
        return ok();
    }

}
