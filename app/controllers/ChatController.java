package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Announcement;
import models.Chat;
import models.Media;
import models.Registration;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utils.VideoThumbnail;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class ChatController extends  BaseController {

    @Inject
    public ChatController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

   // @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Chat chat =new Chat();
        String msgType=jsonNode.findPath("msgType").asText();
        String message=jsonNode.findPath("message").asText();
        String mediaThumbnail=jsonNode.findPath("mediaThumbnail").asText();
        String media=jsonNode.findPath("media").asText();
        String senderName=jsonNode.findPath("senderName").asText();
        String senderId=jsonNode.findPath("senderId").asText();
        String time=jsonNode.findPath("time").asText();
        String oldId=jsonNode.findPath("id").asText();


        chat.oldId = oldId;
        chat.msgType=msgType;
        chat.message = message;
        chat.mediaThumbnail = mediaThumbnail;
        chat.media = media;
        chat.senderName = senderName;
        chat.senderId = senderId;
        chat.time = time;
        chat.save();
        System.out.println(Json.toJson(chat));
        return ok(Json.toJson(chat));
    }

    public Result getall() {

        List<Chat> chats = ebeanServer.find(Chat.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(chats));
    }

    public Result getMessages(int page) {

        List<Chat> chats = ebeanServer.find(Chat.class).select("")
                                                       .where().order("time desc")
                                                       .setMaxRows(10)
                                                       .setFirstRow(10*page).findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(chats));
    }

    public Result getLatestMessages(Long id){
        List<Chat> chats = ebeanServer.find(Chat.class).select("")
                .where().gt("id",id).order("time desc").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(chats));
    }


    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Chat chat= ebeanServer.find(Chat.class).setId(id).findUnique();
        String msgType=jsonNode.findPath("msgType").asText();
        String message=jsonNode.findPath("message").asText();
        String mediaThumbnail=jsonNode.findPath("mediaThumbnail").asText();
        String media=jsonNode.findPath("media").asText();
        String senderName=jsonNode.findPath("senderName").asText();
        String senderId=jsonNode.findPath("senderId").asText();
        String time=jsonNode.findPath("time").asText();

        chat.setMsgType(msgType);
        chat.setMessage(message);
        chat.setMediaThumbnail(mediaThumbnail);
        chat.setMedia(media);
        chat.setSenderName(senderName);
        chat.setSenderId(senderId);
        chat.setTime(time);

        chat.update();
        return ok();

    }

    public Result delete(Long id) {

        Chat chat = ebeanServer.find(Chat.class).setId(id).findUnique();
        //todo if chat is multimedia then delete its files too

        boolean isSuccessful = chat.delete();

        return ok("{\"status\":"+isSuccessful+"}");
    }


    public Result chatUpload() throws IOException {
        Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> picture = formData.getFile("file");

        Chat chat = new Chat();
        for(String key : formData.asFormUrlEncoded().keySet()) {
            System.out.println(key+"  "+formData.asFormUrlEncoded().get(key));
         }
//        chat.msgType=0;
//        chat.message = message;
//        chat.mediaThumbnail = mediaThumbnail;
//        chat.media = media;
//        chat.senderName = senderName;
//        chat.senderId = senderId;
//        chat.time = time;
//        chat.save();

        String fileName=null;
        Media media =new Media();
        if (picture != null) {
            fileName = picture.getFilename();
            media.name =fileName;
            media.type = "image";
            media.save();
            String contentType = picture.getContentType();
            File file = picture.getFile();
            file.renameTo(new File(System.getProperty("user.home")+File.separator+"bssAssets"+File.separator+"media",media.name));


            if(media.name.endsWith(".mp4")) {
                media.type = "video";
                media.update();
                try {
                    VideoThumbnail.transform(media.name, System.getProperty("user.home") + File.separator + "bssAssets" + File.separator + "media");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.print("got exception ");
                }
            }
        }
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(media));

    }

}
