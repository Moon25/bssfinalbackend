package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Achievment;
import models.AchievmentDetails;
import models.AchievmentLocation;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class AchievmentLocationController extends BaseController {

    @Inject
    public AchievmentLocationController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }
    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        AchievmentLocation achievmentLocation =new AchievmentLocation();

        Long achievment=jsonNode.findPath("achievment").asLong();
        Achievment achievment1=ebeanServer.find(Achievment.class).setId(achievment).findOne();
        String thumbnail=jsonNode.findPath("thumbnail").asText();


        String locationname=jsonNode.findPath("locationname").asText();
        achievmentLocation.achievment=achievment1;
        achievmentLocation.locationname=locationname;
        achievmentLocation.thumbnail=thumbnail;
        achievmentLocation.save();
        return ok();
    }

    public Result getall() {
        List<AchievmentLocation> achievmentLocations = ebeanServer.find(AchievmentLocation.class).select("").fetch("achievment").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(achievmentLocations));
    }
    public Result getdata(Long locid){
        Achievment achievment=ebeanServer.find(Achievment.class).setId(locid).findUnique();
        List<AchievmentLocation> achievmentLocations = ebeanServer.find(AchievmentLocation.class).select("").where().eq("achievment",achievment).findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(achievmentLocations));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        AchievmentLocation achievmentLocation= ebeanServer.find(AchievmentLocation.class).setId(id).findUnique();
        Long achievment=jsonNode.findPath("achievment").asLong();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        Achievment achievment1=ebeanServer.find(Achievment.class).setId(achievment).findOne();
        String locationname=jsonNode.findPath("locationname").asText();
        achievmentLocation.setLocationname(locationname);
        achievmentLocation.setThumbnail(thumbnail);
        achievmentLocation.setAchievment(achievment1);
        achievmentLocation.update();
        return ok();
    }

    public Result delete(Long id) {
        AchievmentLocation achievmentLocation = ebeanServer.find(AchievmentLocation.class).setId(id).findUnique();
        achievmentLocation.delete();
        return ok();
    }

}
