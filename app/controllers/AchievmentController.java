package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Achievment;
import models.AchievmentLocation;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class AchievmentController extends  BaseController {

    @Inject
    public AchievmentController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Achievment achievment =new Achievment();
        String name=jsonNode.findPath("name").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        achievment.name=name;
        achievment.thumbnail=thumbnail;
        achievment.save();
        return ok();
    }

    public Result getall() {

        List<Achievment> achievments = ebeanServer.find(Achievment.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(achievments));
    }

     public Result getSideList(){
        List<Achievment> achievments =ebeanServer.find(Achievment.class).select("").findList();
         JsonArray jsonArray =new JsonArray();
         JsonContext jc = Ebean.json();
         jsonArray.add(jc.toJson(achievments));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            Achievment achievment =ebeanServer.find(Achievment.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            List<AchievmentLocation> achievmentDetails =ebeanServer.find(AchievmentLocation.class).select("").where()
                    .eq("achievment",achievments).findList();
        /*JsonArray jsonArray1=new JsonArray();
            jsonArray1.add(jc.toJson(subCategories));
        jsonArray.;*/



        }


         return ok(jsonArray.toString());

     }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Achievment achievment= ebeanServer.find(Achievment.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        achievment.setName(name);
        achievment.setThumbnail(thumbnail);
        achievment.update();
        return ok();

    }

    public Result delete(Long id) {
        Achievment achievment = ebeanServer.find(Achievment.class).setId(id).findUnique();
        achievment.delete();
        return ok();
    }


}
