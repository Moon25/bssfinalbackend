package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Announcement;
import models.Gotra;
import models.Registration;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class GotraController extends  BaseController {

    @Inject
    public GotraController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Gotra gotra =new Gotra();
        String name=jsonNode.findPath("name").asText();
        gotra.name=name;
        gotra.save();
        return ok();
    }

    public Result getall() {

        List<Gotra> gotras = ebeanServer.find(Gotra.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(gotras));
    }

     /*public Result getSideList(){
        List<Registration> registrations =ebeanServer.find(Registration.class).select("").findList();
         JsonArray jsonArray =new JsonArray();
         JsonContext jc = Ebean.json();
         jsonArray.add(jc.toJson(registrations));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            Registration registration1 =ebeanServer.find(Registration.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            *//*List<SubCategory> subCategories =ebeanServer.find(SubCategory.class).select("").where()
                    .eq("category",category1).findList();*//*
        *//*JsonArray jsonArray1=new JsonArray();
            jsonArray1.add(jc.toJson(subCategories));
        jsonArray.;*//*



        }


         return ok(jsonArray.toString());

     }
*/

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Gotra gotra= ebeanServer.find(Gotra.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        gotra.setName(name);
        gotra.update();
        return ok();

    }

    public Result delete(Long id) {
        Gotra gotra = ebeanServer.find(Gotra.class).setId(id).findUnique();
        gotra.delete();
        return ok();
    }


}
