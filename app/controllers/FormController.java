package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Form;
import models.FormTitle;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class FormController extends BaseController {

    @Inject
    public FormController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Form form =new Form();

        Long formTitle=jsonNode.findPath("formTitle").asLong();
        FormTitle formTitle1=ebeanServer.find(FormTitle.class).setId(formTitle).findOne();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        form.formTitle=formTitle1;
        form.thumbnail= thumbnail;
        form.save();
        return ok();
    }

    public Result getall() {
        //Form form = ebeanServer.find(Form.class).select("file").findUnique();
        List<Form> form = ebeanServer.find(Form.class).select("").fetch("formTitle").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(form));
    }

   /* public Result getform(Long formId) {
        FormTitle formTitle=ebeanServer.find(FormTitle.class).setId(formId).findUnique();
        List<Form> forms = ebeanServer.find(Form.class)
                .select("")
                .where().eq("formTitle",formTitle).findList();

        return ok(Json.toJson(forms));
    }*/


    public Result getform(Long formId){
        FormTitle formTitle=ebeanServer.find(FormTitle.class).setId(formId).findUnique();
        List<Form> forms = ebeanServer.find(Form.class).select("").where().eq("formTitle",formTitle).findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(forms));
    }


    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Form form= ebeanServer.find(Form.class).setId(id).findUnique();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        Long formTitle=jsonNode.findPath("formTitle").asLong();
        FormTitle formTitle1=ebeanServer.find(FormTitle.class).setId(formTitle).findOne();
        form.setFormTitle(formTitle1);
        form.setThumbnail(thumbnail);
        form.update();
        return ok();
    }

    public Result delete(Long id) {
        Form form = ebeanServer.find(Form.class).setId(id).findUnique();
        form.delete();
        return ok();
    }



}




