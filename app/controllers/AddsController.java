package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.*;
import io.ebean.text.json.JsonContext;
import models.*;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class AddsController extends  BaseController {

    @Inject
    public AddsController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Adds adds =new Adds();
        String name=jsonNode.findPath("name").asText();
        String thumbnail1=jsonNode.findPath("thumbnail").asText();
        System.out.println("the image" +thumbnail1);
        Boolean status=jsonNode.findPath("status").asBoolean();
        adds.name=name;
        adds.thumbnail=thumbnail1;

        adds.status=status;
        adds.save();
        return ok();
    }

    public Result getall() {

        List<Adds> adds = ebeanServer.find(Adds.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(adds));
    }

   /* public Result getSideList(){
        List<Gallery> galleries =ebeanServer.find(Gallery.class).select("").findList();
        JsonArray jsonArray =new JsonArray();
        JsonContext jc = Ebean.json();
        jsonArray.add(jc.toJson(galleries));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            Gallery gallery =ebeanServer.find(Gallery.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            List<GalleryDetails> galleryDetails =ebeanServer.find(GalleryDetails.class).select("").where()
                    .eq("gallery",gallery).findList();



        }


        return ok(jsonArray.toString());

    }*/

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {

        JsonNode jsonNode = Controller.request().body().asJson();
        Adds adds= ebeanServer.find(Adds.class).setId(id).findUnique();

        String sql = "UPDATE adds SET status = 0 WHERE status = 1";
        SqlUpdate update = Ebean.createSqlUpdate(sql);
        update.execute();


        Boolean status=jsonNode.findPath("status").asBoolean();
        adds.setStatus(status);
        adds.update();
        return ok();

    }

    public Result delete(Long id) {
        Adds adds = ebeanServer.find(Adds.class).setId(id).findUnique();
        adds.delete();
        return ok();
    }

    public Result getAds(){
        Adds adds = ebeanServer.find(Adds.class).select("name,thumbnail").where().eq("status","1").findOne();
        JsonContext jc = Ebean.json();
        if(adds!=null)
        return ok(jc.toJson(adds));
        else
        return ok("{none}");
    }

}
