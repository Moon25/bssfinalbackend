package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Announcement;
import models.Registration;
import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.security.Timestamp;
import java.util.List;

public class AnnouncementController extends  BaseController {

    @Inject
    public AnnouncementController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();

        Announcement announcement =new Announcement();
        String heading=jsonNode.findPath("heading").asText();
        Long date=jsonNode.findPath("date").asLong();
        String description=jsonNode.findPath("description").asText();
        System.out.println("jsonNode"+date);
        announcement.heading=heading;
        announcement.date=date;
        announcement.description=description;
        announcement.save();
        return ok();
    }

    public Result getall() {

        List<Announcement> announcement = ebeanServer.find(Announcement.class).select("").orderBy("date desc").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(announcement));
    }

     public Result getSideList(){
        List<Registration> registrations =ebeanServer.find(Registration.class).select("").findList();
         JsonArray jsonArray =new JsonArray();
         JsonContext jc = Ebean.json();
         jsonArray.add(jc.toJson(registrations));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            Registration registration1 =ebeanServer.find(Registration.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            /*List<SubCategory> subCategories =ebeanServer.find(SubCategory.class).select("").where()
                    .eq("category",category1).findList();*/
        /*JsonArray jsonArray1=new JsonArray();
            jsonArray1.add(jc.toJson(subCategories));
        jsonArray.;*/



        }


         return ok(jsonArray.toString());

     }


    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Announcement announcement= ebeanServer.find(Announcement.class).setId(id).findUnique();
        String heading=jsonNode.findPath("heading").asText();
        Long date=jsonNode.findPath("date").asLong();
        String description=jsonNode.findPath("description").asText();
        announcement.setHeading(heading);
        announcement.setDate(date);
        announcement.setDescription(description);
        announcement.update();
        return ok();

    }

    public Result delete(Long id) {
        Announcement announcement = ebeanServer.find(Announcement.class).setId(id).findUnique();
        announcement.delete();
        return ok();
    }


}
