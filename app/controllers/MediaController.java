package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.Ebean;
import io.ebean.EbeanServer;
import io.ebean.text.json.JsonContext;
import models.Media;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utils.VideoThumbnail;

import javax.inject.Inject;
import java.io.*;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;


public class MediaController extends BaseController {

    @Inject
    public MediaController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }


    public Result upload() throws IOException {
        Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> picture = formData.getFile("file");
        String fileName=null;
        Media media =new Media();
        if (picture != null) {
            fileName = picture.getFilename();
            media.name =fileName;
            media.type = "image";
            media.save();
            String contentType = picture.getContentType();
            File file = picture.getFile();
            file.renameTo(new File(System.getProperty("user.home")+File.separator+"bssAssets"+File.separator+"media",media.name));


            if(media.name.endsWith(".mp4")) {
                media.type = "video";
                media.update();
                try {
                    VideoThumbnail.transform(media.name, System.getProperty("user.home") + File.separator + "bssAssets" + File.separator + "media");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.print("got exception ");
                }
            }
        }
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(media));

    }

    public Result uploadmedia() throws IOException {
        Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> picture = formData.getFile("file");
        String fileName=null;
        if (picture != null) {
            fileName = picture.getFilename();
            System.out.println("fileName"+fileName);

            String contentType = picture.getContentType();
            File file = picture.getFile();
            file.renameTo(new File(System.getProperty("user.home")+File.separator+"bssAssets"+File.separator+"media",fileName));

        }
        JsonContext jc = Ebean.json();
        return ok(fileName);

    }

    public Result getmedia() {
        List<Media> media = ebeanServer.find(Media.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(media));
    }

    public Result delete(Long id) {
        Media media = ebeanServer.find(Media.class).setId(id).findUnique();
        new File(System.getProperty("user.home")+File.separator+"bssAssets"+File.separator+"media",media.name).delete();
        media.delete();
        return ok();
    }



    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    String randomStringID(){
        StringBuilder sb = new StringBuilder( 10);
        for( int i = 0; i < 10 ; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }




    public Result saveImage() {
        String name = randomStringID();
        File f = new File(System.getProperty("user.home")+File.separator+"bssAssets"+File.separator+"media",name);
        System.out.println(request().body().asText());
         byte data[];
        data = Base64.getDecoder().decode(request().body().asText());
        try (FileOutputStream fos = new FileOutputStream(f)) {
            fos.write(data);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return ok("{\"message\":\""+name+"\"}");
    }

    public Result what() {
        Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> picture = formData.getFile("file");
        String fileName=null;
        if (picture != null) {
            fileName = randomStringID();
            System.out.println(fileName);
            String contentType = picture.getContentType();
            File file = picture.getFile();
            file.renameTo(new File(System.getProperty("user.home")+File.separator+"bssAssets"+File.separator+"media",fileName+".jpg"));

        }
        return ok("{\"name\":\""+fileName+"\",\"statusCode\":200}");
    }


}