package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Achievment;
import models.AchievmentLocation;
import models.Facebook;
import models.FacebookDetails;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class FacebookController extends  BaseController {

    @Inject
    public FacebookController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Facebook facebook =new Facebook();
        String name=jsonNode.findPath("name").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        facebook.name=name;
        facebook.thumbnail=thumbnail;
        facebook.save();
        return ok();
    }

    public Result getall() {

        List<Facebook> facebooks = ebeanServer.find(Facebook.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(facebooks));
    }

     public Result getSideList(){
        List<Facebook> facebooks =ebeanServer.find(Facebook.class).select("").findList();
         JsonArray jsonArray =new JsonArray();
         JsonContext jc = Ebean.json();
         jsonArray.add(jc.toJson(facebooks));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            Facebook facebook =ebeanServer.find(Facebook.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            List<FacebookDetails> facebookDetails =ebeanServer.find(FacebookDetails.class).select("").where()
                    .eq("facebook",facebook).findList();




        }


         return ok(jsonArray.toString());

     }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Facebook facebook= ebeanServer.find(Facebook.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        facebook.setName(name);
        facebook.setThumbnail(thumbnail);
        facebook.update();
        return ok();

    }

    public Result delete(Long id) {
        Facebook facebook = ebeanServer.find(Facebook.class).setId(id).findUnique();
        facebook.delete();
        return ok();
    }


}
