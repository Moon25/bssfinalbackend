package controllers;

import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.db.ebean.EbeanConfig;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import java.io.File;

public class Application extends BaseController {

    private FormFactory formFactory;

    @Inject
    public Application(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext, FormFactory formFactory) {
        super(ebeanConfig, executionContext);
        this.formFactory = formFactory;
    }

    public Result login() {
        Form<User> loginForm = formFactory.form(User.class);
        createdirectory();
        return Results.ok(views.html.login.render(loginForm));
    }

    public Result index() {
         User user =getUserFromSession();
        return Results.ok(views.html.index.render(user));
    }

    public static void createdirectory(){
        new File(System.getProperty("user.home")+ File.separator+"bssAssets").mkdir();
        new File(System.getProperty("user.home")+ File.separator+"bssAssets" + File.separator+"media").mkdir();
    }

    public Result logOut(){
        clearUserSession();
        return Results.redirect("/");
    }


    public Result getFile(String name){
        return ok(new File(System.getProperty("user.home")+File.separator+"bssAssets"+File.separator+"media",name));
    }
    public Result getFileThumb(String name){
        return ok(new File(System.getProperty("user.home")+File.separator+"bssAssets"+File.separator+"media"+File.separator+"Thumbnail",name));
    }

    public Result options(String path){
        response().setHeader("Access-Control-Allow-Origin" ,  "http://plustvinc.com");
        response().setHeader("Access-Control-Allow-Headers" , "access-control-allow-headers,access-control-allow-methods,access-control-allow-origin");
        response().setHeader("Access-Control-Allow-Methods" , "GET, POST, PUT, DELETE, OPTIONS");
        response().setHeader("Access-Control-Allow-Credentials" , "true");
        response().setHeader("Access-Control-Max-Age" , String.valueOf(60 * 60 * 24));
      return  ok("");
    }


}
