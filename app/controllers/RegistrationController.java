package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;

import models.Registration;

import play.db.ebean.EbeanConfig;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class RegistrationController extends  BaseController {

    @Inject
    public RegistrationController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Registration registration =new Registration();
        String name=jsonNode.findPath("name").asText();
        String email=jsonNode.findPath("email").asText();
        String password=jsonNode.findPath("password").asText();

        registration.name=name;
        registration.email=email;
        registration.password=password;

        registration.save();
        return ok();
    }

    public Result getall() {

        List<Registration> registration = ebeanServer.find(Registration.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(registration));
    }





    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Registration registration= ebeanServer.find(Registration.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        //String thumbnail=jsonNode.findPath("thumbnail").asText();
        registration.setName(name);
        //category.setThumbnail(thumbnail);
        registration.update();
        return ok();

    }

    public Result delete(Long id) {
        Registration registration = ebeanServer.find(Registration.class).setId(id).findUnique();
        registration.delete();
        return ok();
    }



}
