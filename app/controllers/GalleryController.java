package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Gallery;
import models.GalleryDetails;
import models.Organization;
import models.OrganizationDetails;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class GalleryController extends  BaseController {

    @Inject
    public GalleryController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Gallery gallery =new Gallery();
        String name=jsonNode.findPath("name").asText();
        Long date=jsonNode.findPath("date").asLong();
        gallery.name=name;
        gallery.date=date;
        gallery.save();
        return ok();
    }

    public Result getall() {

        List<Gallery> galleries = ebeanServer.find(Gallery.class).select("").orderBy("date desc").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(galleries));
    }

     public Result getSideList(){
        List<Gallery> galleries =ebeanServer.find(Gallery.class).select("").findList();
         JsonArray jsonArray =new JsonArray();
         JsonContext jc = Ebean.json();
         jsonArray.add(jc.toJson(galleries));
        for(int i=0;i<jsonArray.size();i++){
            JsonElement js= jsonArray.get(i);
            Gallery gallery =ebeanServer.find(Gallery.class).setId(js.getAsJsonObject().get("id").getAsLong()).findOne();
            List<GalleryDetails> galleryDetails =ebeanServer.find(GalleryDetails.class).select("").where()
                    .eq("gallery",gallery).findList();



        }


         return ok(jsonArray.toString());

     }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Gallery gallery= ebeanServer.find(Gallery.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        Long date=jsonNode.findPath("date").asLong();
        gallery.setName(name);
        gallery.setDate(date);
        gallery.update();
        return ok();

    }

    public Result delete(Long id) {
        Gallery gallery = ebeanServer.find(Gallery.class).setId(id).findUnique();
        gallery.delete();
        return ok();
    }


}
