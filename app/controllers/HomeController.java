package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import io.ebean.Ebean;
import io.ebean.text.json.JsonContext;
import models.Home;
import models.Organization;
import models.OrganizationDetails;
import play.db.ebean.EbeanConfig;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

public class HomeController extends  BaseController {

    @Inject
    public HomeController(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        super(ebeanConfig, executionContext);

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result add() {
        JsonNode jsonNode = Controller.request().body().asJson();
        Home home =new Home();
        String name=jsonNode.findPath("name").asText();
        String mobile=jsonNode.findPath("mobile").asText();
        String email=jsonNode.findPath("email").asText();
        String description=jsonNode.findPath("description").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        home.name=name;
        home.mobile=mobile;
        home.email=email;
        home.description=description;
        home.thumbnail=thumbnail;
        home.save();
        return ok();
    }

    public Result getall() {

        List<Home> homes = ebeanServer.find(Home.class).select("").findList();
        JsonContext jc = Ebean.json();
        return ok(jc.toJson(homes));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id) {
        JsonNode jsonNode = Controller.request().body().asJson();
        Home home= ebeanServer.find(Home.class).setId(id).findUnique();
        String name=jsonNode.findPath("name").asText();
        String mobile=jsonNode.findPath("mobile").asText();
        String email=jsonNode.findPath("email").asText();
        String description=jsonNode.findPath("description").asText();
        String thumbnail=jsonNode.findPath("thumbnail").asText();
        home.setName(name);
        home.setMobile(mobile);
        home.setEmail(email);
        home.setDescription(description);
        home.setThumbnail(thumbnail);
        home.update();
        return ok();

    }

    public Result delete(Long id) {
        Home home = ebeanServer.find(Home.class).setId(id).findUnique();
        home.delete();
        return ok();
    }


}
