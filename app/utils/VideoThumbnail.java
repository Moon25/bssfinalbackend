package utils;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;

/**
 * Created by Dell on 30-01-2018.
 */

public class VideoThumbnail {

    /**
     *  its your duty to write what it does
     * @param file
     * @param path
     * @return
     * @throws java.lang.Exception
     */
    public static String transform(String file, String path) throws java.lang.Exception {
        String inputFilename = path + File.separator + file;
        String outputFilename = path+ File.separator+ "Thumbnail" + File.separator+file;

        String command[]={
                "C:\\Users\\Dell\\Desktop\\FFMPEG\\ffmpeg-3.3.3-win64-static\\bin\\ffmpeg",
                "-ss",
                "00:00:04",
                "-i",
                inputFilename ,/*Input File*/
                "-vframes" ,
                "1" ,
                "-q:v" ,
                "2" ,
                outputFilename+".png" /*Output File*/
        };

        try {
            Process pr = Runtime.getRuntime().exec(command);
            pr.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace( );
        }


        try {
            cropTumbnail(file+".png",path+ File.separator+ "Thumbnail");
        } catch (Exception e) {
            e.printStackTrace( );
        }

        return file+".png";
    }

    public static void cropTumbnail(String file,String path) throws Exception
    {
        File f = new File(file);
        String format =null;
        if(Files.probeContentType(f.toPath()).equals("image/jpeg"))
        {
            format= "JPG";
        }
        else
        {
            format="PNG";
        }

        Image image = javax.imageio.ImageIO.read(new File(path,file));
        int thumbWidth = 440;
        int thumbHeight =300;
        double thumbRatio = (double)thumbWidth / (double)thumbHeight;
        int imageWidth    = image.getWidth(null);
        int imageHeight   = image.getHeight(null);
        double imageRatio = (double)imageWidth / (double)imageHeight;
        if (thumbRatio < imageRatio)
        {
            thumbHeight = (int)(thumbWidth / imageRatio);
        }
        else
        {
            thumbWidth = (int)(thumbHeight * imageRatio);
        }

        if(imageWidth < thumbWidth && imageHeight < thumbHeight)
        {
            thumbWidth = imageWidth;
            thumbHeight = imageHeight;
        }
        else if(imageWidth < thumbWidth)
            thumbWidth = imageWidth;
        else if(imageHeight < thumbHeight)
            thumbHeight = imageHeight;

        BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = thumbImage.createGraphics();
        graphics2D.setBackground(Color.WHITE);
        graphics2D.setPaint(Color.WHITE);
        graphics2D.fillRect(0, 0, thumbWidth, thumbHeight);
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
        javax.imageio.ImageIO.write(thumbImage, format, new File(path ,file));

    }


    public static String convertStreamToString(InputStream inputStream) throws IOException {
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(inputStream, "UTF-8");
        for (; ; ) {
            int rsz = in.read(buffer, 0, buffer.length);
            if (rsz < 0)
                break;
            out.append(buffer, 0, rsz);
        }
        return out.toString();

    }
}
