package constants;

/**
 * Created by PlusTV Technologies on 13/6/17.
 * <p>
 * Platform can handle three types of subscriptions plans
 * ie.
 *      Monthly
 *      Annually
 *      Lifetime
 * </p>
 */
public class SubscriptionType {

    public static final int MONTHLY=30;

    public static final int ANNUALLY=365;

    /**
     * DEFAULT
     * One Time Purchase
     * Calculator
     */
    public static final int LIFETIME=Integer.MAX_VALUE;

}
