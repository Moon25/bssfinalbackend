package constants;

/**
 * Created by medroid-3 on 13/6/17.
 */
public class Constants {

    public final static String EMAIL_PREFIX="admin@";
    public final static String EMAIL_SUFFIX=".com";
    public final static String SESSION_USERNAME="userName";
    public final static String SESSION_USERID="userId";

    public static class MessageDigester {

        public final static int MD_CHAT = 0;
        public final static int MD_WORKFLOW = 1;
        public final static int MD_ASSIGNEDTO = 2;

    }

}

