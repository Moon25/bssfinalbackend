package constants;

/**
 * Created by PlusTV Technologies on 24/5/17.
 */
public enum UserType {
    ADMIN,
    NORMAL
}
