[<img src="https://img.shields.io/travis/playframework/play-java-ebean-example.svg"/>](https://travis-ci.org/playframework/play-java-ebean-example)

## Technology Used

 PlayFramework 2.6.7

 [https://playframework.com/documentation/latest/Home](https://playframework.com/documentation/latest/Home)

 [https://github.com/playframework/play-java-ebean-example](https://github.com/playframework/play-java-ebean-example)

 Ebean

 [https://ebean-orm.github.io/](https://ebean-orm.github.io/)

 Angular 1.6.6


 HTML
 CSS

